import {Node, Tree} from '../../../../../common/tree/tree';
import {Clock, DefaultClock} from '../../../../../common/clock';
import {
  MatchShellType,
  NormalizedCommand,
  NormalizedSuggestResult,
} from '../models';
import {RegexpHelper} from '../../../../../common/regexp';
import {Suggestion, SuggestionType, SuggestRequest} from '../../suggester';
import {Hash} from '../../../../../+shared/models/hash/hash';
import {CommandDirectoryEntity, DirectoryEntity} from '../../../../../+shared/services/commands/entities';


export interface NodeData extends NodeDataInternal {
  directories: string[];
}

interface NodeDataInternal {
  commandHash?: string;
  executionCount: number;
  selectionCount: number;
  lastExecutionDate?: Date;
  lastSelectionDate?: Date;
  isDeleted: boolean;
}

export class CommandTree {

  constructor(private clock: Clock = new DefaultClock()) {
  }

  private tree = new Tree<string, NodeDataInternal>();

  public saveExecution(...commands: NormalizedCommand[]): NodeData[] {
    const result: NodeData[] = [];
    const dirsFromPreviousNavigation = [];
    for (const command of commands) {
      if (dirsFromPreviousNavigation.length > 0) {
        command.directories = dirsFromPreviousNavigation;
      }
      const now = this.clock.now();
      const keys = [...command.directories];
      if (!!command.commandHash) {
        keys.push(command.commandHash);
      }
      const currentNode = this.tree.getNode(keys, {
        createMissing: true,
        defaultData: {executionCount: 0, selectionCount: 0, lastExecutionDate: now, lastSelectionDate: undefined, isDeleted: false}
      });
      currentNode.data.executionCount++;
      currentNode.data.lastExecutionDate = now;
      currentNode.data.commandHash = command.commandHash;
      result.push({...currentNode.data, directories: command.directories});
      if (!command.commandHash) {
        dirsFromPreviousNavigation.push(...currentNode.pathUntil(null).map(n => n.key));
      }
    }
    return result;
  }

  public saveSelection(suggestion: Suggestion): NodeData {
    const now = this.clock.now();
    const keys = [...suggestion.directories, Hash.create(suggestion.label)];
    const currentNode = this.tree.getNode(keys, {
      createMissing: true,
      defaultData: {executionCount: 0, selectionCount: 0, lastSelectionDate: now, isDeleted: false}
    });
    currentNode.data.selectionCount++;
    currentNode.data.lastSelectionDate = now;
    currentNode.data.commandHash = Hash.create(suggestion.label);
    return {...currentNode.data, directories: suggestion.directories};
  }

  public restoreDirectory(directory: DirectoryEntity) {
    this.restore(
      directory.directories,
      directory.executionCount,
      directory.lastExecutionDate,
      directory.selectionCount,
      directory.lastSelectionDate);
  }

  public restoreCommand(directories: string[], command: CommandDirectoryEntity) {
    if(!command){ return; }
    const keys = [...directories || [], command.commandId];
    const currentNode = this.restore(keys, command.executionCount, command.lastExecutionDate, command.selectionCount, command.lastSelectionDate);
    currentNode.data.commandHash = command.commandId;
  }

  private restore(keys: string[], executionCount: number, executionDate: Date, selectionCount: number, selectionDate: Date): Node<string, NodeDataInternal> {
    const currentNode = this.tree.getNode(keys, {
      createMissing: true,
      defaultData: {executionCount: 0, selectionCount: 0, lastExecutionDate: executionDate, lastSelectionDate: selectionDate, isDeleted: false}
    });
    currentNode.data.executionCount = executionCount;
    currentNode.data.lastExecutionDate = executionDate;
    currentNode.data.selectionCount = selectionCount;
    currentNode.data.lastSelectionDate = selectionDate;
    return currentNode;
  }

  public apply(suggestRequest: SuggestRequest, searchResult: NormalizedSuggestResult) {
    const currentNode = this.tree.getNode(suggestRequest.directories);
    if (!currentNode) {
      return;
    }
    let minExecutionDate = new Date(8640000000000000);
    let minSelectionDate = new Date(8640000000000000);
    let maxExecutionDate = new Date(0);
    let maxSelectionDate = new Date(0);
    if (suggestRequest.isNavigationSearch) {
      const searchString = suggestRequest.input.replace('cd', '').trim();
      const regexp = this.buildSearchRegexp(searchString);
      const searchNode = searchString.trim().length > 0 ? this.tree : currentNode;
      const nodes = searchNode.find(childNode => this.matchInPath(childNode, currentNode, regexp));
      searchResult.suggestions = nodes.map(n => {
        if (minExecutionDate > n.data.lastExecutionDate) {
          minExecutionDate = n.data.lastExecutionDate;
        }
        if (maxExecutionDate < n.data.lastExecutionDate) {
          maxExecutionDate = n.data.lastExecutionDate;
        }
        if (minSelectionDate > n.data.lastSelectionDate) {
          minSelectionDate = n.data.lastSelectionDate;
        }
        if (maxSelectionDate < n.data.lastSelectionDate) {
          maxSelectionDate = n.data.lastSelectionDate;
        }
        const relativeNavigation = '..';
        const nodesToTargetDir = currentNode.pathTo(n, new Node<string, NodeData>(relativeNavigation, undefined));
        return {
          suggestionType: SuggestionType.Navigation,
          matchShellType: MatchShellType.Yes,
          globalExecutionDate: n.data.lastExecutionDate,
          globalSelectionDate: n.data.lastSelectionDate,
          executionDate: n.data.lastExecutionDate,
          executionCount: n.data.executionCount,
          selectionDate: n.data.lastSelectionDate,
          selectionCount: n.data.selectionCount,
          globalExecutionCount: n.data.executionCount,
          globalSelectionCount: n.data.selectionCount,
          pathToDirectory: nodesToTargetDir.map(node => node.key),
          isPathToDirectoryFromRoot: nodesToTargetDir[0].parent?.isRoot && nodesToTargetDir[0].key !== relativeNavigation
        };
      });
    } else {
      for (const s of searchResult.suggestions) {
        const node = currentNode
          .first(n => this.matchHash(n, s.commandHash) && !n.data.isDeleted);
        if (!node || !node.data) {
          s.suggestionType = SuggestionType.GlobalCommand;
          continue;
        }
        if (minExecutionDate > node.data.lastExecutionDate) {
          minExecutionDate = node.data.lastExecutionDate;
        }
        if (maxExecutionDate < node.data.lastExecutionDate) {
          maxExecutionDate = node.data.lastExecutionDate;
        }
        if (minSelectionDate > node.data.lastSelectionDate) {
          minSelectionDate = node.data.lastSelectionDate;
        }
        if (maxSelectionDate < node.data.lastSelectionDate) {
          maxSelectionDate = node.data.lastSelectionDate;
        }
        s.executionCount = node.data.executionCount;
        s.selectionCount = node.data.selectionCount;
        s.executionDate = node.data.lastExecutionDate;
        s.selectionDate = node.data.lastSelectionDate;
        s.pathToDirectory = node.parent.pathUntil(currentNode).map(n => n.key);
        s.suggestionType = s.pathToDirectory.length === 0 ? SuggestionType.LocalCommand : SuggestionType.SemiLocalCommand;
        s.isPathToDirectoryFromRoot = false;
      }
    }
    searchResult.scoringValues.minExecutionDate = minExecutionDate;
    searchResult.scoringValues.maxExecutionDate = maxExecutionDate;
    searchResult.scoringValues.minSelectionDate = minSelectionDate;
    searchResult.scoringValues.maxSelectionDate = maxSelectionDate;
  }

  private matchInPath(start: Node<string, NodeDataInternal>, until: Node<string, NodeDataInternal>, regexp: string): boolean {
    if (start.data && !!start.data.commandHash || start.data.isDeleted) {
      return false;
    }
    const nodePath = start.pathUntil(until);
    const nodePathAsString = nodePath.reduce((a, v) => a + ' ' + v.key, '');
    const result = new RegExp(regexp, 'gi').test(nodePathAsString);
    return result;
  }

  private matchHash(n: Node<string, NodeDataInternal>, commandHash: string): boolean {
    if (!n.data || !n.data.commandHash) {
      return false;
    }
    return n.key === commandHash;
  }

  private buildSearchRegexp(search: string): string {
    const regexp = RegexpHelper.escapeWeekRegExp(search).split(/\s/g).reduce((v1, v2) => {
      if (!v2) {
        return v1;
      }
      if (v1.length > 0) {
        v1 += '|';
      }
      return v1 + `(${v2})`;
    }, '');
    return regexp;
  }
}
