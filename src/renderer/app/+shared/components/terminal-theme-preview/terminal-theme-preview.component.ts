import {Component, Input, OnInit} from '@angular/core';
import {Theme} from '../../../../../shared/models/settings';
import {Color} from '../../../../../shared/color';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../icon/icon.component';
import {TerminalThemePromptComponent} from './terminal-theme-prompt/terminal-theme-prompt.component';

@Component({
    selector: 'app-terminal-theme-preview',
    templateUrl: './terminal-theme-preview.component.html',
    styleUrls: ['./terminal-theme-preview.component.scss'],
    imports: [
        CommonModule,
        IconComponent,
        TerminalThemePromptComponent
    ]
})
export class TerminalThemePreviewComponent implements OnInit {

  public _theme: Subject<Theme> = new BehaviorSubject<Theme>(null);
  public _backgroundImage: Subject<string> = new BehaviorSubject<string>(null);
  public _backgroundColor: Subject<string> = new BehaviorSubject<string>(null);
  public _imageBlur: Subject<string> = new BehaviorSubject<string>('');

  @Input()
  public set theme(theme: Theme) {
    this._theme.next(theme);
    let color = theme.colors.background;
    if (theme.image && theme.image.length > 0) {
      color += Color.getHexOpacity(theme.imageOpacity);
    }
    if (theme.imageBlur) {
      this._imageBlur.next(`blur(${theme.imageBlur}px`);
    } else {
      this._imageBlur.next('');
    }
    if (theme.image) {
      if (theme.image.startsWith('http')) {
        this._backgroundImage.next(theme.image);
      } else {
        this._backgroundImage.next(`file:///${theme.image}`);
      }
    }
    this._backgroundColor.next(color);
  }

  @Input()
  public promptColors: Observable<{foreground?: string, background?: string}[]>;

  public footerHeaderColor: Observable<string>;

  constructor() { }

  ngOnInit(): void {
    this.footerHeaderColor = this._theme.pipe(map(t => {
      if (t?.colors?.background) {
        const isLightTheme = Color.isLight(t.colors.background);
        const factore = isLightTheme ? -1 : 1;
        return Color.lightenDarkenColor(t.colors.background, 20 * factore);
      }
      return '';
    }));

    if (!this.promptColors) {
      this.promptColors = this._theme.pipe(map(t => {
        return t.colors.promptColors;
      }));
    }
  }
}
