import {ShortcutsParser} from './shortcuts.parser';
import {DEFAULT_SETTINGS} from '../default-settings';
import {Shortcuts} from '../models/shortcuts';

describe('shortcuts-validator', () => {

  let shortcuts: Shortcuts;

  beforeEach(() => {
    shortcuts = {...DEFAULT_SETTINGS.shortcuts};
  });

  it('should return valid false if string is undefined', () => {
    expect(ShortcutsParser.parseShortcuts(undefined).isValid).toBeFalsy();
  });

  it('should return valid false if string is null', () => {
    expect(ShortcutsParser.parseShortcuts(null).isValid).toBeFalsy();
  });

  it('should return valid false if string is empty', () => {
    expect(ShortcutsParser.parseShortcuts('').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty object', () => {
    expect(ShortcutsParser.parseShortcuts('{}').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty array', () => {
    expect(ShortcutsParser.parseShortcuts('[]').isValid).toBeFalsy();
  });

  it('should return valid false if shortcuts contains invalid accelerators', () => {
    shortcuts.bringToFront = 'abc';
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeFalsy();
  });

  it('should return valid false if shortcuts contains invalid aliases - missing shortcut', () => {
    shortcuts.aliases = [
      // @ts-ignore
      {command: 'cd ..'}
    ];
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeFalsy();
  });

  it('should return valid false if shortcuts contains invalid aliases - missing command', () => {
    shortcuts.aliases = [
      // @ts-ignore
      {shortcut: 'CommandOrControl+G'}
    ];
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeFalsy();
  });

  it('should return valid false if shortcuts contains invalid aliases - invalid shortcut', () => {
    shortcuts.aliases = [
      // @ts-ignore
      {shortcut: 'CommandOntrol+G', command: 'cd ..'}
    ];
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeFalsy();
  });

  it('should return valid true if shortcuts contains aliases with umlaut', () => {
    shortcuts.aliases = [
      // @ts-ignore
      {shortcut: 'CommandOrControl+Ö', command: 'cd ..'},
      {shortcut: 'CommandOrControl+Ä', command: 'cd ..'},
      {shortcut: 'CommandOrControl+Ü', command: 'cd ..'}
    ];
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeTruthy();
  });

  it('should return valid true if shortcuts contains µ', () => {
    shortcuts.aliases = [
      // @ts-ignore
      {shortcut: 'CommandOrControl+Alt+µ', command: 'cd ..'}
    ];
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeTruthy();
  });

  it('should return valid true if string is valid shortcuts', () => {
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeTruthy();
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).hasWarnings).toBeFalsy();
  });

  it('should return valid true if shortcut is undefined, null or empty', () => {
    shortcuts.bringToFront = null;
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeTruthy();
    shortcuts.bringToFront = undefined;
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeTruthy();
    shortcuts.bringToFront = '';
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeTruthy();
  });

  it('should return valid true but warning if we have a duplicate shortcut', () => {
    shortcuts.bringToFront = 'T';
    shortcuts.find = 'T';
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).isValid).toBeTruthy();
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).hasWarnings).toBeTruthy();
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(shortcuts)).warnings.length).toBe(1);
  });

  it('should remove not existing shortcuts', () => {
    const s = {...shortcuts};
    s['InvalidShortcut'] = 'CommandOrControl+I'
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(s)).isValid).toBeTruthy();
    expect(ShortcutsParser.parseShortcuts(JSON.stringify(s)).shortcuts['InvalidShortcut']).toBeFalsy();
  });
});
