export namespace Key {
  export const Escape = 'Escape';
  export const ArrowDown = 'ArrowDown';
  export const ArrowUp = 'ArrowUp';
  export const ArrowLeft = 'ArrowLeft';
  export const ArrowRight = 'ArrowRight';
  export const Enter = 'Enter';
  export const Tab = 'Tab';
  export const Backspace = 'Backspace';
  export const Delete = 'Delete';
  export const Home = 'Home';
  export const End = 'End';
  export const Shift = 'Shift';
  export const Meta = 'Meta';
  export const Control = 'Control';
  export const Alt = 'Alt';
  export const PageUp = 'PageUp';
  export const PageDown = 'PageDown';

  export function isModifierKeyPressed(event) {
    return event.ctrlKey || event.shiftKey || event.altKey || event.metaKey;
  }

  export function isKeyDownEvent(e: KeyboardEvent) {
    return e.type.toLowerCase() === 'keydown';
  }
}
