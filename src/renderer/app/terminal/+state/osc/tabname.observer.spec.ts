import {clear} from '../../../../../test/factory';
import {Terminal} from '@xterm/xterm';
import { TabnameObserver } from './tabname.observer';

describe('TabnameObserver', () => {
  let tabnameObserver: TabnameObserver;
  let terminal;

  beforeEach(() => {
    terminal = new Terminal();
    terminal.parser.registerOscHandler = jest.fn();
    tabnameObserver = new TabnameObserver();
  });

  afterEach(() => {
    clear();
  });

  test('bind() should bind to the terminal and register OSC handler', () => {
    tabnameObserver.bind(terminal);

    expect(terminal.parser.registerOscHandler).toHaveBeenCalledWith(
      tabnameObserver.OSC_CODE,
      expect.any(Function)
    );
  });

  test('destroy() should dispose all registered disposables', () => {
    const disposeMock = jest.fn();
    terminal.parser.registerOscHandler.mockReturnValue({ dispose: disposeMock });

    tabnameObserver.bind(terminal);
    tabnameObserver.destroy();

    expect(disposeMock).toHaveBeenCalled();
  });

  test('handleOsc() should decode base64 data and emit it through the copy subject', (done) => {
    const expected = 'hello world';

    tabnameObserver.tabname$.subscribe(s => {
      expect(s).toEqual(expected);
      done();
    })

    tabnameObserver['handleOsc'](expected);
  });

  it('handleOsc() should return true after handling data', () => {
    const testData = 'c;aGVsbG8gd29ybGQ=';

    const result = tabnameObserver['handleOsc'](testData);

    expect(result).toBe(true);
  });
});
