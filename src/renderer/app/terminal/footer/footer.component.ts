import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TerminalService} from '../+state/terminal.service';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../../+shared/components/icon/icon.component';
import {ShellConfig} from '../../../../shared/models/settings';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
    imports: [
        CommonModule,
        IconComponent
    ]
})
export class FooterComponent implements OnInit {

  public machine$: Observable<string>;
  public user$: Observable<string>;
  public areShortcutsEnabled$: Observable<boolean>;
  public isAdvancedFeaturesEnabled$: Observable<boolean>;
  public isPossibleRemoteShell$: Observable<boolean>;
  public shellConfig: Observable<ShellConfig>;

  constructor(private service: TerminalService) {
  }

  ngOnInit(): void {
    this.machine$ = this.service.select(s => s?.machine);
    this.user$ = this.service.select(s => s?.user);
    this.isAdvancedFeaturesEnabled$ = this.service.select(s => s?.isAdvancedEnabled);
    this.isPossibleRemoteShell$ = this.service.select(s => s?.isRemoteShell);
    this.areShortcutsEnabled$ = this.service.select(s => s?.areShortcutsEnabled);
  }

  injectAdvancedFeatures() {
    this.service.injectAdvancedFeatures();
  }

  toggleShortcutsEnabled() {
    this.service.toggleShortcutsEnabled();
  }
}
