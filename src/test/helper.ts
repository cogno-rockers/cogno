import {ColorName, Settings, ShellConfig, Theme} from '../shared/models/settings';
import {ShellType} from '../shared/models/models';
import {ParseResult} from '../shared/parser/settings.parser';
import {DEFAULT_SETTINGS} from '../shared/default-settings';
import {Clock} from '../renderer/app/common/clock';
import {ShellLocation} from '../renderer/app/common/shellLocation';
import {PluginConfig} from '../renderer/app/+shared/models/plugin-config';
import {AutocompleteRequest} from '../renderer/app/autocomplete/+state/autocomplete.service';


export namespace TestHelper {

  export function createFonts(): string[] {
    return ['fontA', 'fontB'];
  }

  export function createPlugin(): PluginConfig {
    return {
      type: 'Autocomplete',
      description: '',
      email: '',
      homepage: '',
      icon: '',
      installDir: '',
      installedVersion: '',
      isInstalled: false,
      isUpdateAvailable: false,
      label: 'Testplugin',
      regexp: '',
      repository: '',
      tarballUrl: '',
      username: '',
      workerPath: '',
      name: 'Testplugin',
      availableVersion: '1.0.0'
    };
  }

  export function createShellConfig(): ShellConfig {
    return {
      id: 'bash',
      name: 'Bash',
      default: true,
      isSelected: true,
      path: 'C:/bash.exe',
      type: ShellType.Bash,
      workingDir: 'c:/',
      injectionType: 'Auto'
    };
  }

  export function createSettingsResult(): ParseResult {
    return {settings: createSettings(), isValid: true, hasWarnings: false, errors: undefined};
  }

  export function createSettings(): Settings {
    const shell1: ShellConfig = {
      id: 'bash',
      name: 'Bash',
      default: true,
      isSelected: true,
      path: 'C:/bash.exe',
      type: ShellType.Bash,
      workingDir: 'c:/',
      injectionType: 'Auto'
    };
    const shell2: ShellConfig = {
      id: 'gitbash',
      name: 'GitBash',
      path: 'C:/gitbash.exe',
      type: ShellType.GitBash,
      workingDir: 'c:/',
      isSelected: true,
      injectionType: 'Auto'
    };
    const shell3: ShellConfig = {
      id: 'powershell',
      name: 'Powershell',
      path: 'C:/powershell.exe',
      type: ShellType.Powershell,
      workingDir: 'c:/',
      isSelected: true,
      injectionType: 'Auto'
    };
    return {shells: [shell1, shell2, shell3], themes: [createTheme()], shortcuts: {...DEFAULT_SETTINGS.shortcuts}, general: {openTabInSameDirectory: false, enableTelemetry: true, enablePasteOnRightClick: false, enableCopyOnSelect: false, scrollbackLines: 100000}, autocomplete: {ignore: [], position: 'cursor', mode: 'always'}};
  }

  export function buildAutocompleteRequest(directory: string[], input: string, shellType: ShellType): AutocompleteRequest {
    return {
      cursorPosition: undefined,
      location: new ShellLocation('test', shellType),
      input: input,
      directory: directory,
    };
  }

  export function createTheme(name = 'light'): Theme {
    return {
      appFontsize: 12,
      appFontFamily: 'Roboto',
      name: name,
      padding: '3px',
      paddingAsArray: [3, 3, 3, 3],
      fontsize: 12,
      fontFamily: 'monospace',
      fontWeight: '200',
      cursorWidth: 3,
      cursorStyle: 'bar',
      cursorBlink: true,
      isDefault: true,
      image: 'background.jpg',
      prompt: '$',
      promptVersion: 1,
      colors: {
        foreground: '#7B7B7B',
        background: '#FFFFFF',
        highlight: '#45D298',
        cursor: '#45D298',
        // "Terminal ANSI colors"
        black: '#3B4251',
        red: '#DF5869',
        green: '#45D298',
        yellow: '#F1CE0C',
        blue: '#81A1C1',
        magenta: '#B48DAE',
        cyan: '#88C0D0',
        white: '#E5E9F0',
        brightBlack: '#3B4251',
        brightRed: '#DF5869',
        brightGreen: '#45D298',
        brightYellow: '#F1CE0C',
        brightBlue: '#81A1C1',
        brightMagenta: '#B48DAE',
        brightCyan: '#88C0D0',
        brightWhite: '#E5E9F0',
        promptColors: [{foreground: ColorName.black, background: ColorName.blue}, {foreground: ColorName.black, background: ColorName.blue}]
      }
    };
  }

  export function createKeyboardEvent(event: Partial<KeyboardEvent> = {}): KeyboardEvent {
    return {...event, preventDefault: () => {}, stopPropagation: () => {}} as KeyboardEvent;
  }

  export function createEvent(event: Partial<Event> = {}): Event {
    return {...event, preventDefault: () => {}, stopPropagation: () => {}} as Event;
  }
}

export class TestClock implements Clock {

  private date = new Date();

  set clock(date: Date) {
    this.date = date;
  }

  now(offset?: number): Date {
    return new Date(this.date.getDate() + offset || 0);
  }
}
