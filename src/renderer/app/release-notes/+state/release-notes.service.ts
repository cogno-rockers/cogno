import {Injectable, OnDestroy} from '@angular/core';
import {TabService} from '../../+shared/abstract-components/tab/+state/tab.service';
import {GridService} from '../../global/grid/+state/grid.service';
import {ElectronService} from '../../+shared/services/electron/electron.service';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';


@Injectable({
  providedIn: 'root'
})
export class ReleaseNotesService extends TabService<{}> implements OnDestroy {
  constructor( protected gridService: GridService, private electron: ElectronService, private KeyboardService: KeyboardService) {
    super(gridService, {});
    this.KeyboardService.activateGlobalShortcuts();
  }

  openCogno(): void {
    this.electron.openExternal('http://www.cogno.rocks');
  }

  openWebgl(): void {
    this.electron.openExternal('https://github.com/xtermjs/xterm.js/releases/tag/4.3.0');
  }

  openRemoteDocumentation() {
    this.electron.openExternal('https://gitlab.com/cogno-rockers/cogno/-/wikis/Remote-Connections-(Experimental-Feature)');
  }

openPluginDocumentation() {
  this.electron.openExternal('https://gitlab.com/cogno-rockers/cogno/-/wikis/Autocompleter-Plugins');
}

  ngOnDestroy(): void {
    this.unsubscribe();
    this.KeyboardService.deactivateGlobalShortcuts();
  }

  openLoremPicsum(): void {
    this.electron.openExternal('https://picsum.photos');
  }
}
