import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs';
import {ShellConfig} from '../../../../shared/models/settings';
import {ShellType} from '../../../../shared/models/models';
import {GeneralSettingsService} from './+state/general.settings.service';
import {SettingsTabService} from '../+state/settings-tab.service';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {EditShellDialogComponent} from './edit-shell-dialog/edit-shell-dialog.component';
import {ShellIconComponent} from '../../+shared/components/shell-icon/shell-icon.component';
import {IconComponent} from '../../+shared/components/icon/icon.component';
import {CopyEditDeleteComponent} from '../../+shared/components/copy-edit-delete/copy-edit-delete.component';

@Component({
    selector: 'app-general',
    templateUrl: './general.component.html',
    styleUrls: ['./general.component.scss'],
    providers: [GeneralSettingsService],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        FormsModule,
        IconComponent,
        ShellIconComponent,
        EditShellDialogComponent,
        CopyEditDeleteComponent
    ]
})
export class GeneralComponent implements OnInit, OnDestroy {

  public enableTelemetry: Observable<boolean>;
  public enablePasteOnRightClick: Observable<boolean>;
  public enableCopyOnSelect: Observable<boolean>;
  public openTabInSameDirectory: Observable<boolean>;
  public scrollbackLines: Observable<number>;
  public shells: Observable<ShellConfig[]>;
  public isEditShellDialogVisible: Observable<boolean>;
  public isLastShell: Observable<boolean>;
  public ShellType = ShellType;

  constructor(private settingsService: SettingsTabService, private service: GeneralSettingsService) {
  }

  ngOnInit(): void {
    this.service.init(this.settingsService.getTabId() + 'generalsettings');
    this.service.loadShells();
    this.enableTelemetry = this.service.selectEnableTelemetry();
    this.enablePasteOnRightClick = this.service.selectEnablePasteOnRightClick();
    this.openTabInSameDirectory = this.service.selectOpenTabInSameDirectory();
    this.isEditShellDialogVisible = this.service.selectIsEditShellDialogVisible();
    this.enableCopyOnSelect = this.service.selectEnableCopyOnSelect();
    this.scrollbackLines = this.service.selectScrollbackLines();
    this.isLastShell = this.service.selectIsLastShell();
    this.shells = this.service.selectShells();
  }

  ngOnDestroy(): void {
    this.service.destroy();
  }

  toggleEnableTelemetry() {
    this.service.toggleEnableTelemetry();
  }

  showAddShellDialog() {
    this.service.showEditShellDialog();
  }

  removeShell(shell: ShellConfig) {
    this.service.removeShell(shell.id);
  }

  editShell(shell: ShellConfig) {
    this.service.updateShellToEdit(shell.id);
    this.service.showEditShellDialog();
  }

  toggleEnablePasteOnRightClick() {
    this.service.toggleEnablePasteOnRightClick();
  }

  toggleEnableCopyOnSelect() {
    this.service.toggleEnableCopyOnSelect();
  }

  toggleOpenTabInSameDirectory() {
    this.service.toggleOpenTabInSameDirectory();
  }

  editDelete($event: 'edit' | 'delete', shell: ShellConfig) {
    switch ($event){
      case 'delete': this.removeShell(shell); break;
      case 'edit': this.editShell(shell); break;
    }
  }

  setScrollbackLines(event: Event) {
    this.service.setScrollbackLines((event.target as any).value);
  }
}
