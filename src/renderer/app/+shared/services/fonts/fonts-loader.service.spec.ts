import {ElectronService} from '../electron/electron.service';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {of} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {FontsLoaderService} from './fonts-loader.service';


describe('FontsLoaderService', () => {

  let electronService: ElectronService;
  let fontsLoaderService: FontsLoaderService;

  beforeEach(() => {
    // @ts-ignore
    electronService = new ElectronService(null);
    fontsLoaderService = new FontsLoaderService(electronService);
  });

  it('should work', (done) => {
    jest.spyOn(electronService, 'invoke').mockImplementation((channel, listener) => {
      return of(['fontA']).pipe(debounceTime(500));
    });
    fontsLoaderService.loadFonts().subscribe(r => {
      expect(electronService.invoke).toBeCalledWith(IpcChannel.GetAvailableFonts);
      expect(r).toEqual(['fontA']);
      done();
    });
  });
});
