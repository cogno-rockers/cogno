import {ITerminalAddon, Terminal} from '@xterm/xterm';
import {IDisposable} from 'node-pty';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, tap} from 'rxjs/operators';
import {Viewport} from '../models';

export class ViewportAddon implements ITerminalAddon {
  private _terminal: Terminal | undefined;
  private _viewportElement: HTMLElement | undefined;
  private _disposables: IDisposable[] = [];
  private _viewportSubject = new BehaviorSubject<Viewport>(undefined);
  private _baseLineText: string | undefined;
  private _lastBufferOverflow: number | undefined;

  activate(terminal: Terminal): void {
    this._terminal = terminal;
    this._disposables.push(this._terminal.onWriteParsed(() => {
      this.calcViewport();
    }));
    setTimeout(() => {
      this._viewportElement = this._terminal.element.querySelector('.xterm-viewport');
      this._viewportElement.addEventListener('scroll', this.onSroll);
    }, 1000);
  }

  private onSroll = () => {
    const that = this;
    that.calcViewport();
  };

  private calcViewport() {
      const baseLineText = this._terminal.buffer.active.getLine(0)?.translateToString();
      if(this._baseLineText !== baseLineText) {
        // we have a bufferoverflow
        this._baseLineText = baseLineText;
        this._lastBufferOverflow = Date.now();
      }
      const visibleStartLine = this._terminal.buffer.active.viewportY;
      const totalRows = this._terminal.buffer.active.baseY + this._terminal.rows;
      const visibleEndLine = Math.min(visibleStartLine + this._terminal.rows, totalRows);
      let lastNonEmptyLine = visibleStartLine;
      for(let i = visibleStartLine; i < visibleEndLine; i++) {
        const line = this._terminal.buffer.active.getLine(i);
        if (line) {
          const lineContent = line.translateToString(true);
          if (lineContent.length > 0) {
            lastNonEmptyLine = i;
          }
        }
      }
      const lineOfCursor = this._terminal.buffer.active.cursorY + this._terminal.buffer.active.baseY;
      const newViewport: Viewport = {startLine: visibleStartLine, endLine: lastNonEmptyLine, lineOfCursor, lastBufferOverflow: this._lastBufferOverflow};
      this._viewportSubject.next(newViewport);
  }

  dispose(): void {
    this._disposables.forEach(d => d.dispose());
    this._viewportElement.removeEventListener('scroll', this.onSroll);
    this._viewportElement = undefined;
  }

  onViewportChanged(): Observable<Viewport> {
    return this._viewportSubject.pipe(
      filter(v => !!v),
      distinctUntilChanged((a, b) => a.startLine === b.startLine && a.endLine === b.endLine && a.lineOfCursor === b.lineOfCursor && a.lastBufferOverflow === b.lastBufferOverflow)
    );
  }

  getCurrentViewport(): Viewport | undefined {
    return this._viewportSubject.value;
  }
}
