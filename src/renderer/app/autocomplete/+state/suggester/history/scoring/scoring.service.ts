import {MatchShellType, ScoringValues, SuggestionCalculation} from '../models';
import {SuggestionType} from '../../suggester';

export class ScoringService {

  private static calcDateScore(date: Date, minDate: Date, maxDate: Date): number {
    if (!date || !minDate || !maxDate) {
      return 0;
    }
    if(date < minDate || date > maxDate) {
      date = maxDate;
    }
    let range = maxDate.getTime() - minDate.getTime();
    range = range === 0 ? 1 : range;
    return (date.getTime() - minDate.getTime()) / range;
  }

  private static calcBoost(calculation: SuggestionCalculation): number {
    let boost = 1;
    switch (calculation?.suggestionType) {
      case SuggestionType.LocalCommand:
        boost *= 1;
        break;
      case SuggestionType.SemiLocalCommand:
        boost *= .8;
        break;
      default:
        boost *= 0.1;
    }
    switch (calculation?.matchShellType) {
      case MatchShellType.Yes:
        boost *= 1;
        break;
      case MatchShellType.Partly:
        boost *= 0.5;
        break;
      default:
        boost *= 0;
    }
    return boost;
  }

  public apply(calculation: SuggestionCalculation, scoringValues: ScoringValues) {
    const matchesScore = calculation.labelMatches >= scoringValues.tokenCount ? 1 : 0;
    const matchesStartOfWordScore = (calculation.labelMatchesStartOfWord > scoringValues.tokenCount ? scoringValues.tokenCount : calculation.labelMatchesStartOfWord) / scoringValues.tokenCount < 0 ? scoringValues.tokenCount : 1;
    const directoryScore = calculation.pathToDirectory ? 1 / (calculation.pathToDirectory.length + 1) : 1;
    const executionCountScore = calculation.executionCount > 0 ? 1 - 1 / calculation.executionCount : 0;
    const selectionCountScore = calculation.selectionCount > 0 ? 1 - 1 / calculation.selectionCount : 0;
    const globalExecutionCountScore = calculation.globalExecutionCount > 0 ? 1 - 1 / calculation.globalExecutionCount : 0;
    const globalSelectionCountScore = calculation.globalSelectionCount > 0 ? 1 - 1 / calculation.globalSelectionCount : 0;
    const executionDateScore = ScoringService.calcDateScore(calculation.executionDate, scoringValues.minExecutionDate, scoringValues.maxExecutionDate);
    const selectionDateScore = ScoringService.calcDateScore(calculation.selectionDate, scoringValues.minSelectionDate, scoringValues.maxSelectionDate);
    const globalExecutionDateScore = ScoringService.calcDateScore(calculation.globalExecutionDate, scoringValues.minGlobalExecutionDate, scoringValues.maxGlobalExecutionDate);
    const globalSelectionDateScore = ScoringService.calcDateScore(calculation.globalSelectionDate, scoringValues.minGlobalSelectionDate, scoringValues.maxGlobalSelectionDate);
    const boost = ScoringService.calcBoost(calculation);
    const sum = 0.35 * matchesScore +
      0.25 * matchesStartOfWordScore +
      0.06 * selectionCountScore +
      0.06 * executionCountScore +
      0.06 * executionDateScore +
      0.06 * selectionDateScore +
      0.03 * globalExecutionCountScore +
      0.03 * globalSelectionCountScore +
      0.03 * globalExecutionDateScore +
      0.03 * globalSelectionDateScore +
      0.04 * directoryScore;
    calculation.score = sum * boost;
    calculation.scoreDetails = {
      globalExecutionCount: globalExecutionCountScore,
      globalSelectionCount: globalSelectionCountScore,
      globalExecutionDate: globalExecutionDateScore,
      globalSelectionDate: globalSelectionDateScore,
      executionCount: executionCountScore,
      selectionCount: selectionCountScore,
      executionDate: executionDateScore,
      selectionDate: selectionDateScore,
      matches: matchesScore,
      directory: directoryScore,
    };
  }
}
