import {Component, Input, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';

@Component({
    selector: 'app-emoji',
    templateUrl: './emoji.component.html',
    styleUrls: ['./emoji.component.scss'],
    imports: [
        CommonModule
    ]
})
export class EmojiComponent implements OnInit {

  @Input()
  public emoji: number;

  constructor() { }

  ngOnInit(): void {
  }
}
