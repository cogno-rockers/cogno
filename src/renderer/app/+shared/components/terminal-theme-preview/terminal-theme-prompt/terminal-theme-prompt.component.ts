import {Component, Input, OnInit} from '@angular/core';
import {Chars} from '../../../../../../shared/chars';
import {CommonModule} from '@angular/common';
import {Observable, of} from 'rxjs';
import {Theme} from '../../../../../../shared/models/settings';

@Component({
    selector: 'app-terminal-theme-prompt',
    templateUrl: './terminal-theme-prompt.component.html',
    styleUrls: ['./terminal-theme-prompt.component.scss'],
    imports: [
        CommonModule
    ]
})
export class TerminalThemePromptComponent implements OnInit {

  @Input() theme: Observable<Theme>;
  @Input() promptColors: Observable<{foreground?: string, background?: string}[]>;
  @Input() command: string;
  @Input() showCursor: boolean;

  public Chars = Chars;

  constructor() { }

  ngOnInit(): void {
  }

  toColor(theme: Theme, colorName: string): string {
    return theme.colors[colorName];
  }
}
