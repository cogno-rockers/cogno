import {ShellType} from '../../../shared/models/models';
import {Hash} from '../+shared/models/hash/hash';

export class ShellLocation {

    constructor(
        public machine: string,
        public shellType: ShellType
    ) { }

    get isValid(): boolean {
        return  !!this.machine && this.machine.length > 0 && !!this.shellType && this.shellType.length > 0;
    }

    get hash(): string {
      return Hash.create(this.machine + this.shellType);
    }

    toString(): string {
        return `${this.machine}, ${this.shellType}`;
    }
}
