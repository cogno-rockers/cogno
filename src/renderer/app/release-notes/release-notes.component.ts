import {Component} from '@angular/core';
import {TabComponent} from '../+shared/abstract-components/tab/tab.component';
import {ReleaseNotesService} from './+state/release-notes.service';
import {IconComponent} from '../+shared/components/icon/icon.component';

@Component({
    selector: 'app-release-notes',
    templateUrl: './release-notes.component.html',
    styleUrls: ['./release-notes.component.scss'],
    providers: [ReleaseNotesService],
    imports: [
        IconComponent
    ]
})
export class ReleaseNotesComponent extends TabComponent<{}> {

  constructor(private service: ReleaseNotesService) {
    super(service);
  }

  openCogno(): void {
    this.service.openCogno();
  }

  openWebgl(): void {
    this.service.openWebgl();
  }

  openRemoteDocumentation() {
    this.service.openRemoteDocumentation();
  }

  openLoremPicsum() {
    this.service.openLoremPicsum();
  }

  openPluginDocumentation() {
    this.service.openPluginDocumentation();
  }
}
