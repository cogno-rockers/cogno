const fs = require('fs');
const path = require('path');

const pathToPatchFile = path.join('node_modules', 'node-pty', 'lib', 'windowsConoutConnection.js');

fs.readFile(pathToPatchFile, 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  var result = data.replace(/var FLUSH_DATA_INTERVAL = 1000;/g, 'var FLUSH_DATA_INTERVAL = 0;');

  fs.writeFile(pathToPatchFile, result, 'utf8', function (err) {
    if (err) return console.log(err);
  });
});
