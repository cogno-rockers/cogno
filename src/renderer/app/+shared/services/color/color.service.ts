import { Injectable } from '@angular/core';
import {SettingsService} from '../settings/settings.service';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  constructor(private settingsService: SettingsService) {
  }

  getColorByName(name: string, opacity: string = 'FF' ): string {
    const activeTheme = this.settingsService.getActiveTheme();
    const charCode = [...name].reduce((prev, curr) => prev + curr.charCodeAt(0), 0);
    const index = charCode % 12;
    switch (index) {
      case 0: return activeTheme.colors.white + opacity;
      case 1: return activeTheme.colors.brightWhite + opacity;
      case 2: return activeTheme.colors.green + opacity;
      case 3: return activeTheme.colors.brightGreen + opacity;
      case 4: return activeTheme.colors.blue + opacity;
      case 5: return activeTheme.colors.brightBlue + opacity;
      case 6: return activeTheme.colors.cyan + opacity;
      case 7: return activeTheme.colors.brightCyan + opacity;
      case 8: return activeTheme.colors.magenta + opacity;
      case 9: return activeTheme.colors.brightMagenta + opacity;
      case 10: return activeTheme.colors.yellow + opacity;
      case 11: return activeTheme.colors.brightYellow + opacity;
    }
  }
}
