import {Injectable} from '@angular/core';
import {createStore, Store} from '../../../common/store/store';
import {Observable, Subscription} from 'rxjs';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {SettingsService} from '../../../+shared/services/settings/settings.service';
import {SettingsTabService} from '../../+state/settings-tab.service';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {map} from 'rxjs/operators';
import {
  getShortcutKeys,
  Shortcut,
  shortcutsArrayToShortcuts,
  toAliasKey,
  toShortcutString
} from '../../../../../shared/models/shortcuts';

export interface ShortcutsState {
  shortcuts: Shortcut[];
  showEditShortcut: boolean;
  sameShortcutExists: boolean;
  duplicatedShortcuts: Shortcut[];
  activeShortcut: Shortcut | null;
}

export const initialState: ShortcutsState = {
    shortcuts: [],
    duplicatedShortcuts: [],
    showEditShortcut: false,
    sameShortcutExists: false,
    activeShortcut: null
};

@Injectable()
export class ShortcutsService {

  private store: Store<ShortcutsState>;
  private subscriptions: Subscription[] = [];

  constructor(private electron: ElectronService,
              private settingsService: SettingsService,
              private tabService: SettingsTabService) {
  }

  public init(id: string) {
    this.store = createStore(id,  initialState);
    this.subscriptions.push(
      this.settingsService.selectShortcutsAsArray().subscribe(shortcuts => {
        this.tabService.updateIsLoading(false);

        const map = new Map<string, Shortcut>();
        const duplicatedShortcuts: Shortcut[] = [];
        for (const shortcut of shortcuts) {
          if(shortcut.key === 'abortTask') {continue;}
          if(map.has(shortcut.shortcut)) {
            duplicatedShortcuts.push(shortcut);
            duplicatedShortcuts.push(map.get(shortcut.shortcut));
          } else {
            map.set(shortcut.shortcut, shortcut);
          }
        }
        this.store.update({
          shortcuts: shortcuts,
          duplicatedShortcuts: duplicatedShortcuts
        });
      })
    );
  }

  selectShortcuts(): Observable<Shortcut[]> {
    return this.store.select(s => s.shortcuts);
  }

  selectDuplicatedShortcuts(): Observable<Shortcut[]> {
    return this.store.select(s => s.duplicatedShortcuts);
  }

  selectActiveShortcut() {
    return this.store.select(s => s.activeShortcut);
  }

  selectShowEditShortcut(): Observable<boolean> {
    return this.store.select(s => s.showEditShortcut);
  }

  showEditShortcut(shortcut: Shortcut) {
    return this.store.update({ activeShortcut: {...shortcut}, showEditShortcut: true});
  }

  deleteShortcut(shortcut: Shortcut) {
    const currentShortcuts = [...this.store.get(s => s.shortcuts)];
    const index = currentShortcuts.findIndex(s => s.key === shortcut.key);
    const shortcutToDelete = currentShortcuts[index];
    if(shortcutToDelete.command) {
      currentShortcuts.splice(index, 1);
    } else {
      shortcutToDelete.shortcutKeys = [];
      shortcutToDelete.shortcut = '';
    }
    return this.store.update({ activeShortcut: null, shortcuts: currentShortcuts});
  }

  saveShortcuts() {
    const shortcuts = this.store.get(s => s.shortcuts);
    this.tabService.updateIsLoading(true);
    this.electron.send(IpcChannel.SaveShortcutConfig, shortcutsArrayToShortcuts(shortcuts));
  }

  destroy() {
    this.store.destroy();
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  selectActiveShortcutKeys(): Observable<string[]> {
    return this.store.select(s => s.activeShortcut).pipe(map(s => s?.shortcutKeys));
  }

  selectSameShortcutExists() {
    return this.store.select(s => s.sameShortcutExists);
  }

  closeEditShortcutDialog() {
    const state = this.store.get(s => s);
    if (
      !state?.activeShortcut ||
      state?.activeShortcut?.command?.trim().length === 0 ||
      state?.activeShortcut?.shortcutKeys?.length === 0) {
      this.store.update({activeShortcut: null, showEditShortcut: false, sameShortcutExists: false});
      return;
    }
    const currentShortcuts = [...state.shortcuts];
    const activeShortcut = state.activeShortcut;
    if (!state.activeShortcut.key) { // new Alias
      activeShortcut.label = activeShortcut.command;
      activeShortcut.key = toAliasKey(activeShortcut.command);
      currentShortcuts.push(activeShortcut);
    } else {
      const index = currentShortcuts.findIndex(s => s.key === activeShortcut.key);
      currentShortcuts.splice(index, 1, activeShortcut);
    }
    this.store.update({shortcuts: currentShortcuts, activeShortcut: null, showEditShortcut: false, sameShortcutExists: false});
  }

  updateShortcut(event: KeyboardEvent) {
    event.preventDefault();
    event.stopPropagation();
    const shortcutString = toShortcutString(event);
    const keys = getShortcutKeys(shortcutString);
    const sameShortcutExists = !!this.store.get(s => s.shortcuts).find(s => s.shortcut === shortcutString);
    this.store.update(s => ({sameShortcutExists: sameShortcutExists,  activeShortcut: {
        ...s.activeShortcut,
        shortcut: shortcutString,
        shortcutKeys: keys
      }}));
  }

  updateCommand(value: string) {
    this.store.update(s => ({activeShortcut: {...s.activeShortcut, command: value}}));
  }

  searchForLabel(label: string) {

  }
}
