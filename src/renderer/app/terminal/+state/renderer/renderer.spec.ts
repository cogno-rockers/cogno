import {Renderer} from './renderer';
import {AnsiEscSequence} from '../../../common/ansi-escape';
import {KeyboardService} from '../../../+shared/services/keyboard/keyboard.service';
import {SettingsService} from '../../../+shared/services/settings/settings.service';
import {AutocompleteService} from '../../../autocomplete/+state/autocomplete.service';
import {
  clear,
  getAutocompleterService,
  getRenderer,
  getSettingsService,
  getKeyboardService
} from '../../../../../test/factory';
import {TestHelper} from '../../../../../test/helper';

describe('XTermAdapter', () => {

  let adapter: Renderer;
  let shortcutsService: KeyboardService;
  let settingsService: SettingsService;
  let autocompleterService: AutocompleteService;

  beforeEach(() => {
    clear();
    settingsService = getSettingsService();
    shortcutsService = getKeyboardService();
    autocompleterService = getAutocompleterService();
    adapter = getRenderer();
  });

  it('link regexp should work "http://test.com"', () => {
     expect(adapter.linkRegexp.exec('http://test.com')[0]).toEqual('http://test.com');
   });

  it('link regexp should work "https://test.com"', () => {
    expect(adapter.linkRegexp.exec('https://test.com')[0]).toEqual('https://test.com');
  });

  it('link regexp should work "http://localhost:4200"', () => {
    expect(adapter.linkRegexp.exec('http://localhost:4200')[0]).toEqual('http://localhost:4200');
  });

  it('link regexp should work "ftp://test.com"', () => {
    expect(adapter.linkRegexp.exec('ftp://test.com')[0]).toEqual('ftp://test.com');
  });

  it('link regexp should work "something before http://test.com something after"', () => {
    expect(adapter.linkRegexp.exec('something before http://test.com something after')[0]).toEqual('http://test.com');
  });

  it('link regexp should work "something before https://de.test.com/index.html?title=Seitenname&action=Aktionsname something after"', () => {
    expect(adapter.linkRegexp.exec('something before https://de.test.com/index.html?title=Seitenname&action=Aktionsname something after')[0]).toEqual('https://de.test.com/index.html?title=Seitenname&action=Aktionsname');
  });

  it('path regexp should work "/test/tes" - find only files', () => {
    expect(adapter.pathRegexp.test('/test/tes')).toBeFalsy();
  });

  it('path regexp should work "test/test/test.txt"  - find only files', () => {
    expect(adapter.pathRegexp.test('test/test/test.txt')).toBeTruthy();
    expect(adapter.pathRegexp.exec('test/test/test.txt')[0]).toEqual('test/test/test.txt');
  });

  it('path regexp should work "/test/test.txt"', () => {
    expect(adapter.pathRegexp.test('/test/test.txt')).toBeTruthy();
    expect(adapter.pathRegexp.exec('/test/test.txt')[0]).toEqual('/test/test.txt');
  });

  it('path regexp should work "\\test\\test.txt"', () => {
    expect(adapter.pathRegexp.test('\\test\\test.txt')).toBeTruthy();
    expect(adapter.pathRegexp.exec('\\test\\test.txt')[0]).toEqual('\\test\\test.txt');
  });

  it('path regexp should work "c:\\test\\test.txt"', () => {
    expect(adapter.pathRegexp.test('c:\\test\\test.txt')).toBeTruthy();
    expect(adapter.pathRegexp.exec('c:\\test\\test.txt')[0]).toEqual('c:\\test\\test.txt');
  });

  it('path regexp should work "test/test.ts"', () => {
    expect(adapter.pathRegexp.test('test/test.ts')).toBeTruthy();
    expect(adapter.pathRegexp.exec('test/test.ts')[0]).toEqual('test/test.ts');
  });

  it('path regexp should work "test/test.ts"', () => {
    expect(adapter.pathRegexp.test('test/test.ts')).toBeTruthy();
    expect(adapter.pathRegexp.exec('test/test.ts')[0]).toEqual('test/test.ts');
  });

  it('path regexp should not match "[dispo].[We"', () => {
    expect(adapter.pathRegexp.test('[dispo].[We')).toBeFalsy();
  });
});
