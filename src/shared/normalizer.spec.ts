import {Normalizer} from './normalizer';
import {ShellType} from './models/models';
import {ShellConfig} from './models/settings';
import {ShellLocation} from "../renderer/app/common/shellLocation";

describe('Normalizer', () => {

  it('split connected commands bash', () => {
    const normalizer = new Normalizer(new ShellLocation('', ShellType.Bash));
    expect(normalizer.split('test1 test2')).toEqual(['test1 test2']);
    expect(normalizer.split('test1 && test2')).toEqual(['test1', 'test2']);
    expect(normalizer.split('test1 || test2')).toEqual(['test1', 'test2']);
    expect(normalizer.split('test1 & test2')).toEqual(['test1', 'test2']);
    expect(normalizer.split('test1; test2')).toEqual(['test1', 'test2']);
  });

  it('split connected commands powershell', () => {
    const normalizer = new Normalizer(new ShellLocation('', ShellType.Powershell));
    expect(normalizer.split('test1 test2')).toEqual(['test1 test2']);
    expect(normalizer.split('test1; test2')).toEqual(['test1', 'test2']);
  });

  it('combine commands', () => {
    let normalizer = new Normalizer(new ShellLocation('', ShellType.Bash));
    expect(normalizer.combine(['test', 'test2'])).toEqual('(test && test2)');
    expect(normalizer.combine(['test', 'test2'], true)).toEqual('test && test2 &&');
    expect(normalizer.combine(['test'])).toEqual('test');
    expect(normalizer.combine(['test', ''])).toEqual('test');
    normalizer = new Normalizer(new ShellLocation('', ShellType.Powershell));
    expect(normalizer.combine(['test', 'test2'])).toEqual('test ; test2');
    expect(normalizer.combine(['test', 'test2'], true)).toEqual('test ; test2 ;');
    expect(normalizer.combine(['test'])).toEqual('test');
    expect(normalizer.combine(['test', ''])).toEqual('test');
  });

  it('to path commands', () => {
    let normalizer = new Normalizer(new ShellLocation('', ShellType.Bash));
    expect(normalizer.toPath(['c', 'test2'], true)).toEqual('/c/test2');
    expect(normalizer.toPath(['temp', 'test2'], false)).toEqual('temp/test2');
    expect(normalizer.toPath(['c', 'users', 'micro'], true)).toEqual('/c/users/micro');
    normalizer = new Normalizer(new ShellLocation('', ShellType.Powershell));
    expect(normalizer.toPath(['c', 'test2'], true)).toEqual('c:\\test2');
    expect(normalizer.toPath(['c', 'users', 'micro'], true)).toEqual('c:\\users\\micro');
    expect(normalizer.toPath(['temp', 'test2'], false)).toEqual('temp\\test2');
  });

  it('diff', () => {
    expect(Normalizer.diff('', '')).toEqual('');
    expect(Normalizer.diff('a', 'a')).toEqual('');
    expect(Normalizer.diff('a b c', 'a b')).toEqual(' c');
    expect(Normalizer.diff('a b c d', 'a b')).toEqual(' c d');
    expect(Normalizer.diff('a b c d', 'a b d')).toEqual('c ');
  });
});

