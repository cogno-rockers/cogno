import {HighlightType, Suggestion} from './+state/suggester/suggester';

  export function calcHtml(suggestion: Suggestion): string {
    if (!suggestion.labelHighlight || suggestion.labelHighlight.length === 0) {
      return suggestion.label || '';
    }
    let result = '';
    for (let i = 0; i < suggestion.labelHighlight.length; i++) {
      const highlightLabel = suggestion.labelHighlight[i];
      const text = highlightLabel.text?.replace(/\s/g, '&nbsp;') || '';
      switch (highlightLabel.type) {
        case HighlightType.Argument:
          result += `<span class="lowlight">${text}</span>`;
          break;
        case HighlightType.Search:
          result += `<span class="highlight">${text}</span>`;
          break;
        case HighlightType.OpenSpecial:
          result += '<span class="highlight-special">';
          break;
        case HighlightType.CloseSpecial:
          result += '</span>';
          break;
        case HighlightType.None:
        default:
          result += `${text}`;
          break;
      }
    }
    return result;
  }
