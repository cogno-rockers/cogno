import { Component } from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SettingsService} from '../+shared/services/settings/settings.service';
import {ElectronService} from '../+shared/services/electron/electron.service';
import {CommonModule} from '@angular/common';
import {CloseWindowButtonComponent} from '../+shared/components/close-window-button/close-window-button.component';

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.scss'],
    imports: [
        CommonModule,
        CloseWindowButtonComponent
    ]
})
export class ErrorComponent {

  readonly MAX_ERRORS = 10;

  public settingsErrors: Observable<string[]>;

  constructor(private service: SettingsService, private electron: ElectronService) {
    this.settingsErrors = service.selectErrors().pipe(
      map(errors => {
        if (errors && errors.length > this.MAX_ERRORS) {
          const e = errors.slice(0, this.MAX_ERRORS);
          e.push('...');
          return e;
        }
        return errors;
     })
    );
  }
  openSettings() {
    this.electron.openPath(this.electron.getSettingsPath());
  }

}
