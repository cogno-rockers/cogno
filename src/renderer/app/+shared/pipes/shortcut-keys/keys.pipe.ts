import { Pipe, PipeTransform } from '@angular/core';
import {ShortcutPipe} from '../shortcut/shortcut.pipe';

@Pipe({
  name: 'shortcutKeys',
  standalone: true
})
export class ShortcutKeysPipe implements PipeTransform {

  private shortcutPipe: ShortcutPipe;

  constructor() {
    this.shortcutPipe = new ShortcutPipe();
  }

  transform(keys: string[]): string {
    if (!keys) { return ''; }
    const result =  keys.join('+');
    return this.shortcutPipe.transform(result);
  }
}
