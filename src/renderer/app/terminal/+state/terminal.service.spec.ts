import {TerminalService} from './terminal.service';
import {clear, getGridService, getPasteService, getShell, getTerminalService} from '../../../../test/factory';
import {PasteService} from '../../global/paste-history-menu/+state/paste.service';
import {Shell} from './shell/shell';
import {GridService} from '../../global/grid/+state/grid.service';
import {TabType} from '../../../../shared/models/models';
import {TestHelper} from '../../../../test/helper';

describe('TerminalService', () => {
  let service: TerminalService;
  let pasteService: PasteService;
  let gridService: GridService;
  let shell: Shell;

  beforeEach(async () => {
    clear();
    gridService = getGridService();
    pasteService = getPasteService();
    shell = getShell();
    service = getTerminalService();
    await pasteService.loadHistory();
  });

  it('should be able to create a TerminalService', () => {
    expect(service).toBeTruthy();
  });

  it('should handle paste', () => {
    jest.spyOn(shell, 'paste').mockImplementation((a, b, c) => {});
    gridService.addNewTabOnActivePane(TabType.Terminal, TestHelper.createShellConfig());
    service.init(gridService.getActiveTabId());
    pasteService.add('test');
    pasteService.pasteEntryAtIndex(0);
    expect(shell.paste).toHaveBeenCalledWith('test', null, {start: undefined, end: undefined, hasSelection: false, text: ''});
  })
})
