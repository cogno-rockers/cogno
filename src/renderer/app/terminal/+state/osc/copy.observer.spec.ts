import {CopyObserver} from './copy.observer';
import {clear} from '../../../../../test/factory';
import {Terminal} from '@xterm/xterm';

describe('CopyObserver', () => {
  let copyObserver: CopyObserver;
  let terminal;

  beforeEach(() => {
    terminal = new Terminal();
    terminal.parser.registerOscHandler = jest.fn();
    copyObserver = new CopyObserver();
  });

  afterEach(() => {
    clear();
  });

  test('bind() should bind to the terminal and register OSC handler', () => {
    copyObserver.bind(terminal);

      expect(terminal.parser.registerOscHandler).toHaveBeenCalledWith(
        copyObserver.OSC_CODE,
        expect.any(Function)
      );
    });

    test('destroy() should dispose all registered disposables', () => {
      const disposeMock = jest.fn();
      terminal.parser.registerOscHandler.mockReturnValue({ dispose: disposeMock });

      copyObserver.bind(terminal);
      copyObserver.destroy();

      expect(disposeMock).toHaveBeenCalled();
    });

    test('handleOsc() should decode base64 data and emit it through the copy subject', (done) => {
      const testData = 'c;aGVsbG8gd29ybGQ='; // "hello world" in base64 with prefix "c;"
      const expectedDecoded = 'hello world';

      copyObserver.copy$.subscribe(s => {
        expect(s).toEqual(expectedDecoded);
        done();
      })

      copyObserver['handleOsc'](testData);
    });

    it('handleOsc() should return true after handling data', () => {
      const testData = 'c;aGVsbG8gd29ybGQ=';

      const result = copyObserver['handleOsc'](testData);

      expect(result).toBe(true);
    });
});
