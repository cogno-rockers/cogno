import {Injectable, OnDestroy} from '@angular/core';
import {TerminalService} from './terminal.service';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';
import {Subscription} from 'rxjs';
import {GlobalMenuService} from '../../+shared/abstract-components/menu/+state/global-menu.service';
import {KeytipService} from '../../+shared/services/keyboard/keytip.service';
import {AutocompleteService} from '../../autocomplete/+state/autocomplete.service';
import {platform} from 'os';

@Injectable()
export class TerminalKeyboardService implements OnDestroy {

  private subscriptions: Subscription[] = [];

  constructor(private terminalService: TerminalService,
              private keyboardService: KeyboardService,
              private keytipService: KeytipService,
              private autocompleteService: AutocompleteService,
              private menuService: GlobalMenuService) {

  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  init() {
    const that = this;
    this.terminalService.attachCustomKeyEventHandler((e: KeyboardEvent) => {
      if (!e) {
        return true;
      }

      if (that.autocompleteService.handle(e)) {
        e.preventDefault();
        e.stopPropagation();
        return false;
      }

      if (that.keytipService.handleKeytip(e)) {
        e.preventDefault();
        e.stopPropagation();
        return false;
      }

      if (that.keyboardService.handleShortcut(e, that.terminalService.areShortcutsEnabled())) {
        e.preventDefault();
        e.stopPropagation();
        return false;
      }

      if (that.terminalService.hasSelection() && that.keyboardService.handleKeyPressed(e)) {
        e.preventDefault();
        e.stopPropagation();
        return false;
      }

      // option key does not work on mac, I'm not sure why. this is fixing the issue.
      // For example if I press 'option' + '9', xtermjs does not print '}' it prints '9'.
      // TODO: Find the reason and fix it
      const isDarwin = platform() === 'darwin';
      const isAltPressed = e.altKey;
      const isKeydown = e.type === 'keydown';
      const isAllowedKey =
        e.code.startsWith('Digit') ||
        e.code.startsWith('Key') ||
        e.code.startsWith('Equal');
      if (isDarwin && isAltPressed && isKeydown && isAllowedKey) {
        e.preventDefault();
        e.stopPropagation();
        // Skip writing if the key code starts with 'KeyN'
        if (!e.code.startsWith('KeyN')) {
          that.terminalService.write(e.key);
        }
        return false;
      }
      return true;
    });
    this.subscriptions.push(this.keyboardService.onNoneMetaKey().subscribe(event => {
      if (!this.terminalService.hasSelection()) {
        return;
      }
      if (event.isPrintableChar) {
        this.terminalService.replaceSelection(event);
      } else {
        this.terminalService.clearSelection();
      }
    }));
    this.subscriptions.push(this.keyboardService.onAlias().subscribe(alias => {
      const isActiveTab = this.terminalService.isActive();
      if (isActiveTab) {
        this.terminalService.execute(alias.command);
      }
    }));
    this.subscriptions.push(this.keyboardService.onShortcut(
      'copy',
      'paste',
      'cut',
      'clearBuffer',
      'clearLine',
      'abortTask',
      'abortAllTasks',
      'clearLineToEnd',
      'clearLineToStart',
      'nextArgument',
      'previousArgument',
      'deleteNextWord',
      'deletePreviousWord',
      'goToNextWord',
      'goToPreviousWord',
      'scrollToNextCommand',
      'scrollToPreviousCommand',
      'scrollToNextBookmark',
      'scrollToPreviousBookmark',
      'showAutocompletion',
      'selectTextLeft',
      'selectTextRight',
      'selectWordLeft',
      'selectWordRight',
      'selectTextToStartOfLine',
      'selectTextToEndOfLine'
    ).subscribe(shortcut => {
      const isActive = this.terminalService.isActive();
      if (isActive && !this.menuService.isAnyOpened()) {
        switch (shortcut.key) {
          case 'copy':
            this.terminalService.copyOrAbort();
            break;
          case 'cut':
            this.terminalService.cut();
            break;
          case 'paste':
            this.terminalService.paste();
            break;
          case 'clearBuffer':
            this.terminalService.clearTerminal();
            break;
          case 'clearLine':
            this.terminalService.clearLine();
            break;
          case 'clearLineToStart':
            this.terminalService.clearLineToStart();
            break;
          case 'clearLineToEnd':
            this.terminalService.clearLineToEnd();
            break;
          case 'abortTask':
            this.terminalService.copyOrAbort();
            break;
          case 'goToNextWord':
            this.terminalService.goToNextWord();
            break;
          case 'deleteNextWord':
            this.terminalService.deleteNextWord();
            break;
          case 'goToPreviousWord':
            this.terminalService.goToPreviousWord();
            break;
          case 'deletePreviousWord':
            this.terminalService.deletePreviousWord();
            break;
          case 'scrollToNextCommand':
            this.terminalService.scrollToNextCommand();
            break;
          case 'scrollToPreviousCommand':
            this.terminalService.scrollToPreviousCommand();
            break;
          case 'scrollToNextBookmark':
            this.terminalService.scrollToNextBookmark();
            break;
          case 'scrollToPreviousBookmark':
            this.terminalService.scrollToPreviousBookmark();
            break;
          case 'showAutocompletion':
            this.terminalService.showAutocompletion();
            break;
          case 'selectTextLeft':
            this.terminalService.selectTextLeft();
            break;
          case 'selectTextRight':
            this.terminalService.selectTextRight();
            break;
          case 'selectWordLeft':
            this.terminalService.selectWordLeft();
            break;
          case 'selectWordRight':
            this.terminalService.selectWordRight();
            break;
          case 'selectTextToStartOfLine':
            this.terminalService.selectTextToStartOfLine();
            break;
          case 'selectTextToEndOfLine':
            this.terminalService.selectTextToEndOfLine();
            break;
          case 'nextArgument':
            this.terminalService.nextArgument();
            break;
          case 'previousArgument':
            this.terminalService.previousArgument();
            break;
        }
      }

      switch (shortcut.key) {
        case 'abortAllTasks':
          if (this.terminalService.isCommandRunning()) {
            this.terminalService.abortTask();
          }
          break;
      }
    }));
  }
}
