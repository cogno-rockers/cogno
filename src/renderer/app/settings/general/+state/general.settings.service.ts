import {Injectable} from '@angular/core';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {GeneralConfig, ShellConfig} from '../../../../../shared/models/settings';
import {map} from 'rxjs/operators';
import {createStore, Store} from '../../../common/store/store';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {ShellsLoaderService} from '../../../+shared/services/shells/shells-loader.service';
import {Observable, Subscription} from 'rxjs';
import {SettingsService} from '../../../+shared/services/settings/settings.service';
import {Uuid} from '../../../common/uuid';
import {InjectionType, ShellType} from '../../../../../shared/models/models';
import {SettingsTabService} from '../../+state/settings-tab.service';

export interface GeneralSettingsState {
  config: GeneralConfig;
  shells: ShellConfig[];
  availableShells: ShellConfig[];
  selectedShell: ShellConfig;
  isEditShellDialogVisible: boolean;
  isLastShell: boolean;
  showRemoteShellDialog: boolean;
}

export const initialState: GeneralSettingsState = {
  config: {
    enableTelemetry: false,
    enablePasteOnRightClick: false,
    enableCopyOnSelect: false,
    openTabInSameDirectory: false,
    scrollbackLines: 10000,
  },
  shells: [],
  availableShells: [],
  selectedShell: null,
  isEditShellDialogVisible: false,
  isLastShell: false,
  showRemoteShellDialog: false
};

@Injectable()
export class GeneralSettingsService {

  private store: Store<GeneralSettingsState>;
  private subscriptions: Subscription[] = [];

  constructor(
    private electron: ElectronService,
    private settingsService: SettingsService,
    private shellsLoader: ShellsLoaderService,
    private tabService: SettingsTabService
  ) {
  }

  public init(id: string) {
    this.store = createStore(id, initialState);
    this.subscriptions.push(
      this.settingsService.selectSettings().subscribe(settings => {
        this.tabService.updateIsLoading(false);
        this.store.update({
          shells: settings.shells,
          isLastShell: settings.shells.length <= 1,
          config: settings.general
        });
      })
    );
  }

  destroy() {
    this.store.destroy();
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  loadShells(): void {
    this.shellsLoader.loadShells().subscribe(shells => this.store.update({availableShells: shells}));
  }

  toggleEnableTelemetry() {
    this.store.update(s => ({
      ...s,
      config: {...s.config, enableTelemetry: !s.config.enableTelemetry}
    }));
    this.saveConfig();
  }

  toggleEnablePasteOnRightClick() {
    this.store.update(s => ({
      ...s,
      config: {...s.config, enablePasteOnRightClick: !s.config.enablePasteOnRightClick}
    }));
    this.saveConfig();
  }

  toggleEnableCopyOnSelect() {
    this.store.update(s => ({
      ...s,
      config: {...s.config, enableCopyOnSelect: !s.config.enableCopyOnSelect}
    }));
    this.saveConfig();
  }

  toggleOpenTabInSameDirectory() {
    this.store.update(s => ({
      ...s,
      config: {...s.config, openTabInSameDirectory: !s.config.openTabInSameDirectory}
    }));
    this.saveConfig();
  }

  private saveConfig() {
    this.electron.send(IpcChannel.SaveGeneralConfig, this.store.get(s => s.config));
    this.tabService.updateIsLoading(true);
  }

  private saveShells(shells: ShellConfig[]) {
    this.fixShellConfig(shells);
    this.tabService.updateIsLoading(true);
    this.electron.send(IpcChannel.SaveShellsConfig, shells);
  }

  private fixShellConfig(shells: ShellConfig[]) {
    const hasDefaultShell = shells.findIndex(s => s.default) !== -1;
    if (!hasDefaultShell) {
      shells[0].default = true;
    }
    this.store.update({shells, isLastShell: shells.length <= 1});
  }

  toggleUseAsDefaultShellLocal() {
    const shell = this.store.get(s => s.selectedShell);
    shell.default = !shell.default;
    this.store.update({selectedShell: shell});
  }

  updateSelectedShellLocal(index: number) {
    const currentShell = this.store.get(s => s.selectedShell);
    const id = currentShell.id || Uuid.create();
    const selectedShell: ShellConfig = {
      ...this.store.get(s => s.availableShells[index]),
      id: id,
      injectionType: 'Auto',
    };
    if (!!currentShell.name && currentShell.name.length > 0) {
      selectedShell.name = currentShell.name;
    }
    this.store.update({selectedShell: selectedShell});
  }

  updateShellToEdit(id: string) {
    const selectedShell = {...this.store.get(s => s.shells.find(s => s.id === id))};
    this.store.update({selectedShell: selectedShell, showRemoteShellDialog: selectedShell.injectionType === 'Remote'});
  }


  setDirectory(workingDir: string) {
    if (!workingDir) {
      return;
    }
    const shell = this.store.get(s => s.selectedShell);
    shell.workingDir = workingDir;
    this.store.update({selectedShell: shell});
  }

  openFilePathSelectionLocal() {
    this.electron.openFileDialog('folder').pipe(
      map((path) => {
        if (path) {
          this.setDirectory(path);
        }
      })
    ).subscribe();
  }

  selectEnableTelemetry(): Observable<boolean> {
    return this.store.select(s => s.config.enableTelemetry);
  }

  selectEnablePasteOnRightClick(): Observable<boolean> {
    return this.store.select(s => s.config.enablePasteOnRightClick || false);
  }

  selectOpenTabInSameDirectory(): Observable<boolean> {
    return this.store.select(s => s.config.openTabInSameDirectory || false);
  }

  selectShells(): Observable<ShellConfig[]> {
    return this.store.select(s => s.shells);
  }

  selectShell(): Observable<ShellConfig> {
    return this.store.select(s => s.selectedShell);
  }

  selectAvailableShells(): Observable<ShellConfig[]> {
    return this.store.select(s => s.availableShells);
  }

  selectIsEditShellDialogVisible(): Observable<boolean> {
    return this.store.select(s => s.isEditShellDialogVisible);
  }

  showEditShellDialog() {
    const shell: ShellConfig = this.store.get(s => s.selectedShell) || {path: '', name: '', type: undefined, injectionType: 'Auto'};
    this.store.update({isEditShellDialogVisible: true, selectedShell: shell});
  }

  closeEditShellDialog() {
    this.store.update({isEditShellDialogVisible: false, selectedShell: null, showRemoteShellDialog: false});
  }

  removeShell(shellId: string) {
    const shells = [...this.store.get(s => s.shells)];
    const index = shells.findIndex(s => s.id === shellId);
    shells.splice(index, 1);
    if (!shells.find(s => s.default)) {
      shells[0].default = true;
    }
    this.saveShells(shells);

  }

  selectIsDialogValid(): Observable<boolean> {
    return this.store.select(s => s.selectedShell).pipe(
      map(s => !!s && !!s.path && s.path.length > 0 && !!s.name && s.name.length > 0)
    );
  }

  saveSelectedShell(): void {
    const shell = this.store.get(s => s.selectedShell);
    if (!shell.id) {
      shell.id = Uuid.create();
    }
    if (shell.injectionType !== 'Remote' && shell.remoteType) {
      shell.remoteType = undefined;
      shell.remoteCommand = undefined;
    }
    const shells = [...this.store.get(s => s.shells)];
    if (shell.default) {
      shells.forEach(s => {
        s.default = false;
      });
    }
    const indexOfShell = shells.findIndex(s => shell.id === s.id);
    if (indexOfShell === -1) {
      shells.push(shell);
    } else {
      shells.splice(indexOfShell, 1, shell);
    }
    this.saveShells(shells);
  };

  setName(value: string): void {
    const shell = this.store.get(s => s.selectedShell);
    shell.name = value;
    this.store.update({selectedShell: {...shell}});
  }

  setArguments(value: string): void {
    const shell = this.store.get(s => s.selectedShell);
    shell.args = value.split(',').map(s => s.trim());
    this.store.update({selectedShell: {...shell}});
  }

  setPromptTerminator(value: string | undefined): void {
    const shell = this.store.get(s => s.selectedShell);
    shell.promptTerminator = value && value.length > 0 ? value : undefined;
    this.store.update({selectedShell: {...shell}});
  }

  selectIsLastShell(): Observable<boolean> {
    return this.store.select(s => s.isLastShell);
  }

  updateHostShellRemote(index: number) {
    const currentShell: ShellConfig = this.store.get(s => s.selectedShell);
    const selectedShell: ShellConfig = {
      ...this.store.get(s => s.availableShells[index]),
      id: undefined,
      injectionType: 'Remote'
    };
    this.store.update({selectedShell: selectedShell});
    if (!!currentShell.name && currentShell.name.length > 0) {
      selectedShell.name = currentShell.name;
    }
    this.store.update({selectedShell: selectedShell});
  }

  updateTargetShellRemote(shellType: ShellType) {
    const shell = this.store.get(s => s.selectedShell);
    shell.remoteType = shellType;
    this.store.update({selectedShell: {...shell}});
  }

  selectShowRemoteShellDialog(): Observable<boolean> {
    return this.store.select(s => s.showRemoteShellDialog);
  }

  toggleShowRemoteShellDialog() {
    let show = this.store.get(s => s.showRemoteShellDialog);
    show = !show;
    const shell = this.store.get(s => s.selectedShell);
    if (show) {
      shell.default = false;
      shell.injectionType = 'Remote';
    } else {
      shell.injectionType = 'Auto';
    }
    this.store.update({showRemoteShellDialog: show, selectedShell: shell});
  }

  setConnectionCommandRemote(value: string) {
    const shell = this.store.get(s => s.selectedShell);
    shell.remoteCommand = value;
    this.store.update({selectedShell: {...shell}});
  }

  setScrollbackLines(value: number) {
    this.store.update(s => ({
      ...s,
      config: {...s.config, scrollbackLines: value}
    }));
    this.saveConfig();
  }

  openDocumentation(page: string) {
    this.electron.openExternal(`https://gitlab.com/cogno-rockers/cogno/-/wikis/${page}`);
  }

  selectShowRemoteShellSelection(): Observable<boolean> {
    return this.store.select(s => s.selectedShell).pipe(map(s => !s?.id));
  }

  selectEnableCopyOnSelect() {
    return this.store.select(s => s?.config?.enableCopyOnSelect || false);
  }

  getState(): GeneralSettingsState {
    return {...this.store.get(s => s)};
  }

  selectScrollbackLines(): Observable<number> {
    return this.store.select(s => s?.config?.scrollbackLines || 10000);
  }

  toggleDebugMode() {
    const shell = {...this.store.get(s => s.selectedShell)};
    shell.isDebugModeEnabled = !shell.isDebugModeEnabled;
    this.store.update({selectedShell: shell});
  }

  toggleFinalSpace() {
    const shell = {...this.store.get(s => s.selectedShell)};
    shell.usesFinalSpacePromptTerminator = !shell.usesFinalSpacePromptTerminator;
    this.store.update({selectedShell: shell});
  }

  setInjectionType(injectionType: InjectionType) {
    const shell = {...this.store.get(s => s.selectedShell)};
    shell.injectionType = injectionType;
    this.store.update({selectedShell: shell});
  }
}
