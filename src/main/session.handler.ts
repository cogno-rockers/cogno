import {ShellConfig} from '../shared/models/settings';
import {IpcChannel} from '../shared/ipc.chanels';
import {ipcMain} from 'electron';
import * as fs from 'fs';
import {Dimensions, SessionData, SessionDataType, ShellType} from '../shared/models/models';
import {WindowFactory} from './window.factory';
import {UTF8Splitter} from './utf8-splitter';
import {debounceTime} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {platform} from 'os';
import {IDisposable, IPty} from 'node-pty';
import psTree from 'ps-tree';

import * as nodePty from 'node-pty';
import {Logger} from './logger';

const sessions: Map<string, Session> = new Map<string, Session>();
let openHere: string;

export namespace SessionHandler {

  export function openNextHere(cwd: string) {
    openHere = cwd;
  }

  export function init(cwd: string) {
    openNextHere(cwd);
    Logger.debug('Init session handler');
    ipcMain.on(IpcChannel.Session, function (evt, data: SessionData) {
      const session = getOrCreateSession(evt.sender.id, data.id);
      switch (data.type) {
        case SessionDataType.New: {
          Logger.debug('New Session.', data.id);
          sessions.set(data.id, session);
          session.start(data.settings);
          break;
        }
        case SessionDataType.Exit: {
          Logger.debug('Sessions close event.', data.id);
          session.exit();
          removeDisposedSessions();
          break;
        }
        case SessionDataType.Resize: {
          Logger.debug('Resize session:', data.id, data.dimensions);
          session.resize(data.dimensions);
          break;
        }
        case SessionDataType.Input: {
          session.write(data.data);
          break;
        }
        case SessionDataType.Ack: {
          session.ack(data.ackLength);
          break;
        }
      }
    });
  }

  function getOrCreateSession(windowId: number, id: string): Session {
    if (sessions.has(id)) {
      return sessions.get(id);
    }
    return new Session(windowId, id);
  }

  export function removeDisposedSessions() {
    const sessionsToRemove = [];
    sessions.forEach((value, key) => {
      if (value.isDisposed) {
        sessionsToRemove.push(key);
      }
    });
    sessionsToRemove.forEach(key => sessions.delete(key));
    Logger.debug('Number of sessions:', sessions.size);
  }

  export function closeAll() {
    sessions.forEach((session, key) => {
        session.exit(true);
      }
    );
    removeDisposedSessions();
  }

  export function areAllClosed(): boolean {
    return sessions.size === 0;
  }
}

export class Session {

  private _ptyShell: IPty;
  private _isClosing = false;
  private _isDisposed = false;

  get isDisposed(): boolean {
    return this._isDisposed;
  }

  private _closeTimeout: any;

  private outputQueue: PTYDataQueue;

  private onExitEvent: IDisposable;
  private onDataEvent: IDisposable;

  constructor(private windowId, private id: string) {
  }

  start(settings: ShellConfig) {
    try {
      let workingDir = openHere ? openHere : this.getWorkingDir(settings);
      if (!fs.existsSync(workingDir)) {
        Logger.debug('set working dir to null');
        workingDir = null;
      }
      const args = settings.args || [];
      this._ptyShell = nodePty.spawn(settings.path, args.filter(arg => arg.trim().length > 0), {
        cwd: workingDir,
        env: this.createEnvironment(),
        useConpty: platform() === 'win32',
        // useConptyDll: true
      });
      openHere = undefined; // Use openHere only once
      if (!this._ptyShell) {
        this.sendError(`Could not start "${settings.path}".`);
        return;
      }
      this.onExitEvent = this._ptyShell.onExit((exitCode: { exitCode: number; signal?: number }) => {
        Logger.debug('onExit pty-shell!');
        if (!this._isClosing) {
          this._isClosing = true;
          Logger.debug('Send closed event to renderer.');
          WindowFactory.send(this.windowId, IpcChannel.Session, {
            id: this.id,
            type: SessionDataType.Closed
          });
        }
        this._isDisposed = true;
        this.onExitEvent.dispose();
        this.onDataEvent.dispose();
        this._ptyShell = null;
        this.onExitEvent = null;
        this.onDataEvent = null;
      });

      this.outputQueue = new PTYDataQueue(this._ptyShell, data => {
        setImmediate(() => {
          const d: SessionData = {
            id: this.id,
            type: SessionDataType.Output,
            data: data.toString(),
            ackLength: data.length
          };
          WindowFactory.send(this.windowId, IpcChannel.Session, d);
        });
      });

      this.onDataEvent = this._ptyShell.onData((data: string) => {
        this.outputQueue.push(Buffer.from(data));
      });
    } catch (e) {
      this.sendError(e.message);
    }
  }

  private getWorkingDir(shell: ShellConfig): string {
    if (!shell?.workingDir) {
      return '';
    }
    if (platform() === 'win32' && shell.type !== ShellType.Powershell) {
      return shell.workingDir
        .replace(/^\/c\//, 'c:/')
        .replace(/^c\//, 'c:/')
        .replace(/^.*\/mnt\/c\//, 'c:/');
    }
    return shell.workingDir;
  }

  private createEnvironment(): {} {
    const env = {
      ...process.env,
      wt: {
        WT_SESSION: 0,
      },
      cygwin: {
        TERM: 'cygwin',
      }
    };
    return env;
  }

  ack(length: number) {
    if (this._isClosing) {
      return;
    }
    this.outputQueue?.ack(length);
  }

  write(data: string) {
    try {
      if (this._isClosing || !(this._ptyShell as any)._writable) {
        return;
      }
      this._ptyShell.write(data);
    } catch (error) {
      Logger.error('Could not write to pty-shell', error);
    }
  }

  killProcesses(pid: number) {
    psTree(pid, (err, children) => {
      if (err) {
        Logger.error('Error when determining the child processes :', err);
        return;
      }
      // Alle Kindprozesse terminieren
      children.forEach(child => {
        Logger.debug(`Kill the child processes ${child.PID} ${child.COMMAND}`);
        try {
          process.kill(pid);
        } catch (e) {
          Logger.error(`Error when kill the processes ${pid}:`, e);
        }
      });
    });
  }

  exit(force: boolean = false) {
    if (this._isClosing) {
      return;
    }
    Logger.debug('Close pty-shell!');
    this._isClosing = true;
    if (this._closeTimeout) {
      clearTimeout(this._closeTimeout);
    }

    if (force) {
      try {
        // try to shutdown the process
        Logger.debug('Force Shutdown the process', this._ptyShell.pid);
        this.onDataEvent.dispose();
        this._ptyShell.kill();
      } catch (err) {
        this.killProcesses(this._ptyShell.pid);
        Logger.error('Exit error', err);
      }
    } else {
      this._closeTimeout = setTimeout(() => {
        this._closeTimeout = undefined;
        try {
          Logger.debug('Close pty-shell after timeout!');
          this.onDataEvent.dispose();
          this._ptyShell.kill();
          Logger.debug('Close pty-shell done! Send result to renderer.');
          WindowFactory.send(this.windowId, IpcChannel.Session, {
            id: this.id,
            type: SessionDataType.Closed
          });
          Logger.debug('Send result done.');
        } catch (err) {
          this.killProcesses(this._ptyShell.pid);
          Logger.error('Exit error', err);
        }
      }, 250);
    }
  }

  resize(dimensions: Dimensions) {
    try {
      if (this._isClosing) {
        return;
      }
      this._ptyShell.resize(dimensions.cols, dimensions.rows);
    } catch (e) {
      Logger.error('Could not resize pty-shell', e);
    }
  }

  private sendError(error: string) {
    WindowFactory.send(this.windowId, IpcChannel.Session, {
      id: this.id,
      type: SessionDataType.Error,
      error: error
    });
  }
}

/**
 * Based on Tabby's PTYDataQueue. Thanks Eugen!!!
 */
class PTYDataQueue {
  private buffers: Buffer[] = [];
  private delta = 0;
  private maxChunk = 1024 * 100;
  private maxDelta = this.maxChunk * 5;
  private flowPaused = false;
  private decoder = new UTF8Splitter();
  private output$ = new Subject<Buffer>();

  constructor(private pty: IPty, private onData: (data: Buffer) => void) {
    this.output$.pipe(debounceTime(100)).subscribe(() => {
      const remainder = this.decoder.flush();
      if (remainder.length) {
        this.onData(remainder);
      }
    });
  }

  push(data: Buffer) {
    this.buffers.push(data);
    this.maybeEmit();
  }

  ack(length: number) {
    this.delta -= length;
    this.maybeEmit();
  }

  private maybeEmit() {
    if (this.delta <= this.maxDelta && this.flowPaused) {
      Logger.info('Resume');
      this.resume();
      return;
    }
    if (this.buffers.length > 0) {
      if (this.delta > this.maxDelta && !this.flowPaused) {
        Logger.info('Pause');
        this.pause();
        return;
      }

      const buffersToSend = [];
      let totalLength = 0;
      while (totalLength < this.maxChunk && this.buffers.length) {
        totalLength += this.buffers[0].length;
        buffersToSend.push(this.buffers.shift());
      }

      if (buffersToSend.length === 0) {
        return;
      }

      let toSend = Buffer.concat(buffersToSend);
      if (toSend.length > this.maxChunk) {
        this.buffers.unshift(toSend.slice(this.maxChunk));
        toSend = toSend.slice(0, this.maxChunk);
      }
      this.emitData(toSend);
      this.delta += toSend.length;

      if (this.buffers.length) {
        setImmediate(() => this.maybeEmit());
      }
    }
  }

  private emitData(data: Buffer) {
    const validChunk = this.decoder.write(data);
    this.onData(validChunk);
    this.output$.next(validChunk);
  }

  private pause() {
    this.pty.pause();
    this.flowPaused = true;
  }

  private resume() {
    this.pty.resume();
    this.flowPaused = false;
    this.maybeEmit();
  }
}
