import {autoUpdater} from 'electron-updater';
import {app, ipcMain} from 'electron';
import {IpcChannel} from '../shared/ipc.chanels';
import {ElectronUpdateInfo} from '../shared/models/models';
import {WindowFactory} from './window.factory';
import {platform} from 'os';
import {Logger} from './logger';

export namespace UpdateHandler {

  let isInitialized = false;

  export function init() {
    if (!isInitialized && platform() !== 'linux') { //temporay disable on linux
      isInitialized = true;
      Logger.debug('Init updater');
      autoUpdater.channel = getChannel();
      // autoUpdater.logger = Logger.instance;
      autoUpdater.autoDownload = true;
      autoUpdater.autoInstallOnAppQuit = false;
      addListeners();
      ipcMain.on(IpcChannel.InstallUpdate, function (evt) {
        Logger.debug('Install update');
        quitAndInstall();
      });
    }
  }

  function getChannel() {
    const version = app.getVersion();
    if (version.includes('rpmnightly')) {
      return 'rpmnightly';
    }
    if (version.includes('x64nightly')) {
      return 'x64nightly';
    }
    if (version.includes('nightly')) {
      return 'nightly';
    }
    if (version.includes('x64')) {
      return 'x64';
    }
    return null;
  }

  function addListeners() {
    autoUpdater.on('checking-for-update', () => {
      sendUpdateInfo({
        isRunning: true,
        version: '',
        error: '',
        hasError: false,
        releaseDate: null,
        isUpdateAvailable: false
      });
    });
    autoUpdater.on('update-available', (ev) => {
      sendUpdateInfo({
          isRunning: true,
          version: ev.version,
          error: '',
          hasError: false,
          releaseDate: new Date(ev.releaseDate),
          isUpdateAvailable: true
        }
      );
    });
    autoUpdater.on('update-not-available', (ev) => {
      sendUpdateInfo({
        isRunning: false,
        version: '',
        error: '',
        hasError: false,
        releaseDate: null,
        isUpdateAvailable: false
      });
    });
    autoUpdater.on('error', (ev, err) => {
      sendUpdateInfo({
        isRunning: false,
        version: '',
        error: err.split('\n')[0],
        hasError: true,
        releaseDate: null,
        isUpdateAvailable: false
      });
    });
    autoUpdater.on('download-progress', (ev) => {
      sendUpdateInfo({
        isRunning: true,
        error: '',
        hasError: false,
        isUpdateAvailable: true
      });
    });
    autoUpdater.on('update-downloaded', (ev) => {
      sendUpdateInfo({
        isRunning: false,
        version: ev.version,
        error: '',
        hasError: false,
        releaseDate: new Date(ev.releaseDate),
        isUpdateAvailable: true
      });
    });
  }

  function sendUpdateInfo(progress: ElectronUpdateInfo) {
    setTimeout(() => {
      WindowFactory.sendMain(IpcChannel.UpdateInfo, progress);
    }, 7000);
  }

  export function checkForUpdates() {
    autoUpdater?.checkForUpdates().catch(err => {
      Logger.error('Error while checking for updates.', err);
    });

  }

  function quitAndInstall() {
    autoUpdater.quitAndInstall();
  }
}
