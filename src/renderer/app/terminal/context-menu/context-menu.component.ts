import {ChangeDetectionStrategy, Component, ElementRef, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Position} from '../../../../shared/models/models';
import {Shortcuts} from '../../../../shared/models/shortcuts';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';
import {MenuComponent} from '../../+shared/abstract-components/menu/menu.component';
import {ContextMenuService} from './+state/context-menu.service';
import {map} from 'rxjs/operators';
import {CommonModule} from '@angular/common';
import {ShortcutPipe} from '../../+shared/pipes/shortcut/shortcut.pipe';
import {KeytipService} from '../../+shared/services/keyboard/keytip.service';

@Component({
    selector: 'app-context-menu',
    templateUrl: './context-menu.component.html',
    styleUrls: ['./context-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        CommonModule,
        ShortcutPipe
    ]
})
export class ContextMenuComponent extends MenuComponent implements OnInit{

  isVisible: Observable<boolean>;
  positionStyle: Observable<any>;
  shortcuts: Observable<Shortcuts>;

  constructor(protected eRef: ElementRef, protected keyboardService: KeyboardService, public keytipService: KeytipService, protected menuService: ContextMenuService) {
    super(eRef, menuService, keyboardService, keytipService);
    this.shortcuts = keyboardService.selectShortcuts();
  }

  ngOnInit() {
    this.menuService.init();
    this.positionStyle = this.menuService.selectIsActive().pipe(map((isActive) => this.calculatePositionStyle(this.menuService.getClickPosition(), isActive)));
  }

  copy(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.copy();
  }

  paste(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.paste();
  }

  showPasteHistory(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.showPasteHistory();
  }

  find(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.find();
  }

  clearBuffer(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.clearBuffer();
  }

  private calculatePositionStyle(position: Position, isActive: boolean): any {
    if (!position || !isActive) {
      return {
        'top': '-100000px',
        'left': '-100000px'
      };
    }
    this.eRef.nativeElement.focus();
      const parentWidth = this.eRef.nativeElement.parentElement.offsetWidth;
      const parentHeight =  this.eRef.nativeElement.parentElement.offsetHeight;
      const height =  this.eRef.nativeElement.firstElementChild.getBoundingClientRect().height;
      const width =  this.eRef.nativeElement.firstElementChild.getBoundingClientRect().width;

      const correctPosition = {x: position.x, y: position.y};
      if (position.y + 40 + height > parentHeight) {
        correctPosition.y = parentHeight - height - 40;
      }
      if (position.x + 20 + width > parentWidth) {
        correctPosition.x = parentWidth - width - 20;
      }
      return {
        'top': correctPosition.y + 'px',
        'left': correctPosition.x + 'px'
      };

  }

  scrollToPreviousCommand(e: any) {
    e?.preventDefault();
    e?.stopPropagation();
    this.menuService.scrollToPreviousCommand();
  }

  scrollToNextCommand(e: any) {
    e?.preventDefault();
    e?.stopPropagation();
    this.menuService.scrollToNextCommand();
  }

  scrollToPreviousBookmark(e: any) {
    e?.preventDefault();
    e?.stopPropagation();
    this.menuService.scrollToPreviousBookmark();
  }

  scrollToNextBookmark(e: any) {
    e?.preventDefault();
    e?.stopPropagation();
    this.menuService.scrollToNextBookmark();
  }

  openFileManagerHere(e: any) {
    e?.preventDefault();
    e?.stopPropagation();
    this.menuService.openFileManagerHere();
  }
}
