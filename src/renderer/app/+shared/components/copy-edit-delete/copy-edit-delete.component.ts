import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {IconComponent} from '../icon/icon.component';
import {CommonModule} from '@angular/common';
import {Icon} from '../icon/icon';

@Component({
    selector: 'app-copy-edit-delete',
    styles: [`
    app-copy-edit-delete {
      display: inline-flex;
      align-items: center;
      justify-content: center;
      flex-direction: row;
      .icon-placeholder {
        background-color: rgba(0,0,0,0);
        opacity: 0;
      }
      .buttons {
        display: flex;
        flex-direction: row;
        &[hidden] {
          visibility: hidden;
          width: 0;
        }
      }
    }
  `],
    template: `
    <div class="buttons" [hidden]="isInDeleteMode">
      <button *ngIf="!enableEdit && enableDelete && !enableCopy" class="icon-placeholder"></button>
      <button title="Edit" *ngIf="enableEdit" (click)="onEvent.emit('edit'); $event.stopPropagation()"><app-icon name="mdiSquareEditOutline"></app-icon></button>
      <button title="Copy" *ngIf="enableCopy" (click)="onEvent.emit('copy'); $event.stopPropagation()"><app-icon name="mdiContentCopy"></app-icon></button>
      <button title="Delete" *ngIf="enableDelete" (click)="isInDeleteMode = true; $event.stopPropagation()"><app-icon name="mdiTrashCanOutline"></app-icon></button>
    </div>
    <div class="buttons" [hidden]="!isInDeleteMode" (mouseleave)="isInDeleteMode = false">
      <button *ngIf="enableEdit && enableDelete && enableCopy" class="icon-placeholder"></button>
      <button (click)="onEvent.emit('delete'); isInDeleteMode = false; $event.stopPropagation()"><app-icon name="mdiCheck"></app-icon></button>
      <button (click)="isInDeleteMode = false; $event.stopPropagation()"><app-icon name="mdiClose"></app-icon></button>
    </div>
  `,
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        IconComponent
    ]
})
export class CopyEditDeleteComponent {

  isInDeleteMode = false;
  @Input()
  enableDelete = true;
  @Input()
  enableEdit = true;
  @Input()
  enableCopy = false;

  @Output() onEvent: EventEmitter<'copy'|'edit'|'delete'> = new EventEmitter();
}
