import {Pane} from './pane';

export class Workspace {
  id: string;
  name: string;
  token?: string;
  colorOpacity?: string;
  color?: string;
  panes: WorkspaceTree;
  isAutosaveEnabled?: boolean;
}

export class WorkspaceTree {
  _root: WorkspaceNode;
}

export class WorkspaceNode {
  _data: Pane;
  _left: WorkspaceNode;
  _right: WorkspaceNode;
}
