import {Normalizer} from '../../../../../shared/normalizer';
import {HighlightType, Suggestion, SuggestionType} from './suggester';
import {RegexpHelper} from '../../../common/regexp';


export class Highlighter {

  constructor(private normalizer: Normalizer) {
  }


  public apply(suggestions: Suggestion[], tokens: string[]) {
    for (const suggestion of suggestions) {
      let highlights = this.splitVariableHighlights(suggestion.label);
      highlights = this.splitToSearchHighlights(highlights, tokens);
      highlights = this.splitConnections(highlights, suggestion.suggestionType);
      suggestion.labelHighlight = highlights;
      suggestion.icon = this.getIcon(suggestion);
    }
    return suggestions;
  }

  private splitConnections(searchHighlights: { text: string; type: HighlightType }[], suggestionType: SuggestionType): { text: string; type: HighlightType }[] {
    if (suggestionType === SuggestionType.Navigation) {
      return searchHighlights;
    }
    const hightlights: { text: string; type: HighlightType }[] = [];
    for (const searchHighlight of searchHighlights) {
      if (this.normalizer.isConnected(searchHighlight.text) && this.normalizer.isNavigation(searchHighlight.text)) {
        hightlights.splice(0, 0, {text: '', type: HighlightType.OpenSpecial});
        const splits = this.normalizer.split(searchHighlight.text);
        hightlights.push({text: this.normalizer.combine([splits[0].trim()], true), type: searchHighlight.type});
        hightlights.push({text: '', type: HighlightType.CloseSpecial});
        hightlights.push({text: splits.length === 2 ? splits[1] : ' ', type: searchHighlight.type});
      } else {
        hightlights.push(searchHighlight);
      }
    }
    return hightlights;
  }

  private splitVariableHighlights(label: string): { text: string; type: HighlightType }[] {
    const hightlights: { text: string; type: HighlightType }[] = [];
      const splits = label.split(new RegExp(RegexpHelper.RegexCommandPlaceholder, 'g'));
      const matches = label.match(new RegExp(RegexpHelper.RegexCommandPlaceholder, 'g'));
      for (let i = 0; i < splits.length; i++) {
        if (splits[i].length > 0) {
          hightlights.push({text: splits[i], type: HighlightType.None});
        }
        if(matches?.length > i) {
          hightlights.push({text: matches[i], type: HighlightType.Argument});
        }

    }
    return hightlights;
  }

  private splitToSearchHighlights(highlights: { text: string; type: HighlightType }[], tokens: string[]): {text: string; type: HighlightType}[] {
    const searchHighlights: { text: string; type: HighlightType }[] = [];
    for (const highlight of highlights) {
      if(highlight.type === HighlightType.Argument) {
        searchHighlights.push(highlight);
      } else {
        const label = highlight.text;
        const searchIndices = this.findSearchHightlightIndices(tokens, label);
        let lastIndex = 0;
        for (let i = 0; i < searchIndices.length; i++) {
          const highlight = searchIndices[i];
          if (highlight.start !== lastIndex) {
            searchHighlights.push({
              text: label.substring(lastIndex, highlight.start),
              type: HighlightType.None
            });
          }
          searchHighlights.push({
            text: label.substring(highlight.start, highlight.end),
            type: HighlightType.Search
          });
          lastIndex = highlight.end;
        }
        if (lastIndex < label.length) {
          searchHighlights.push({text: label.substring(lastIndex), type: HighlightType.None});
        }
      }
    }
    return searchHighlights;
  }

  private findSearchHightlightIndices(tokens: string[], label: string): { start: number; end: number }[] {
    const indexes = [];
    const foundAtPosition = new Array<boolean>(label.length);

    for (const token of tokens) {
      let index = -1;
      while ((index = label.toLowerCase().indexOf(token.toLowerCase(), index + 1)) >= 0) {
        for (let i = index; i < index + token.length; i++) {
          foundAtPosition[i] = true;
        }
      }
    }
    for (let i = 0; i < foundAtPosition.length; i++) {
      const startPosition = i;
      let endPosition = i;
      while (foundAtPosition[i]) {
        i++;
        endPosition = i;
      }
      if (startPosition !== endPosition) {
        indexes.push({start: startPosition, end: endPosition});
      }
    }
    return indexes;
  }

  private getIcon(suggestion: Suggestion) {
    switch (suggestion.suggestionType) {
      case SuggestionType.Navigation:
        return '📁';
      case SuggestionType.LocalCommand:
        return '⭐';
      case SuggestionType.SemiLocalCommand:
        return '⭐';
      case SuggestionType.GlobalCommand:
        return '🖥️';
      case SuggestionType.PluginCommand:
        return suggestion.icon ?? ''
    }
  }
}
