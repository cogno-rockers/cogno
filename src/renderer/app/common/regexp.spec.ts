import {RegexpHelper} from './regexp';

describe('RegexpHelper', () => {

  it('should replace week stuff', () => {
    expect(RegexpHelper.escapeWeekRegExp('a.*+?^${}()|[]\\/b')).toEqual('a.?.?.?.?.?.?.?.?.?.?.?.?.?.?.?b');
  });

  it('should replace strong stuff', () => {
    expect(RegexpHelper.escapeStrongRegExp('a.*+?^${}()|[]\\/b')).toEqual('a\\.\\*\\+\\?\\^\\$\\{\\}\\(\\)\\|\\[\\]\\\\\\/b');
  });

  it('should create valid regexp', () => {
    let regexp = RegexpHelper.escapeStrongRegExp('a.b');
    expect(new RegExp(regexp, 'g').test('a.b.c')).toEqual(true);
    regexp = RegexpHelper.escapeStrongRegExp('a\\b');
    expect(new RegExp(regexp, 'g').test('a\\b\\c')).toEqual(true);
  });
});
