import * as Path from 'path';
import {Injectable} from '@angular/core';
import {StringUtils} from '../../../common/string-utils';
import {Logger} from '../../../logger/logger';
import {DbBuilder} from './db.builder';
import {ElectronService} from '../electron/electron.service';
import Nedb from '@seald-io/nedb';


@Injectable({
  providedIn: 'root'
})
export class DbContext implements IDbContext {
  private _dbs: { [name: string]: Nedb } = {};
  private _testMode = global.__unittest__;

  constructor(private _electron: ElectronService) {
  }

  private existsDb(db: string) {
    return !!this._dbs[StringUtils.toFileName(db)];
  }

  public existsDbFileSync(db: string): boolean {
    const dbName = StringUtils.toFileName(db);
    const path = this.createPath(dbName);
    return this._electron ? this._electron.existsFileSync(path) : true;
  }

  public async createDb(db: string): Promise<boolean> {
    const dbName = StringUtils.toFileName(db);
    const path = this.createPath(dbName);
    const datastore = DbBuilder.build(this._testMode).path(path).create();
    try {
      await datastore.loadDatabaseAsync();
      this._dbs[dbName] = datastore;
      Logger.debug('loaded db', path);
      return true;
    } catch (e) {
      Logger.error('could not load db', e);
      return false;
    }
  }

  public async loadData<T>(dbName: string): Promise<T[]> {
    await this.init(dbName);
    return this.find<T>({}, dbName);
  }

  private createPath(dbName: string): string {
    return Path.join(this.getDbDirectoryPath(), `${dbName}.json`);
  }

  public getDbDirectoryPath(): string {
    return Path.join(this._electron ? this._electron.getConfigDirectoryPath() : '', 'db');
  }

  public init(db: string): Promise<boolean> {
    if (!this.existsDb(db)) {
      return this.createDb(db);
    }
    return Promise.resolve(true);
  }

  public findOne<T>(query: any, db: string): Promise<T> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].findOneAsync(query);
  }

  public find<T>(query: any, db: string): Promise<T[]> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].findAsync(query);
  }

  public add<T>(element: T, db: string): Promise<T> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].insertAsync(element);
  }

  public addAll<T>(elements: T[], db: string): Promise<T[]> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].insertAsync(elements);
  }


  public async update<T extends Id>(element: T, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].updateAsync({_id: element._id}, element);
  }

  public async remove(id: string, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].remove({_id: id}, {});
  }

  public async exists(id: string, db: string): Promise<boolean> {
    const dbName = StringUtils.toFileName(db);
    const elem = await this.findOne({_id: id}, dbName);
    return !!elem;
  }

  public async upsert<T extends Id>(element: T, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].updateAsync({_id: element._id}, element, {upsert: true});
  }

  public async getAll<T extends Id>(dbName: string): Promise<T[]> {
    return this.find<T>({}, dbName);
  }

  public async reload(): Promise<void> {
    for (const dbName in this._dbs) {
      await this._dbs[dbName].loadDatabaseAsync();
    }
  }
}

export interface Id {
  _id: string;
}

export interface IDbContext {
  init(db: string): Promise<boolean>;

  exists(id: string, db: string): Promise<boolean>;

  findOne<T extends Id>(query: any, db: string): Promise<T>;

  find<T extends Id>(query: any, db: string): Promise<T[]>;

  upsert<T extends Id>(element: T, db: string): Promise<void>;

  remove(id: string, db: string): Promise<void>;

  getAll<T extends Id>(db: string): Promise<T[]>;

  add<T extends Id>(element: T, db: string): Promise<T>;

  addAll<T extends Id>(element: T[], db: string): Promise<T[]>;

  update<T extends Id>(element: T, db: string): Promise<void>;

  loadData<T>(dbOld: string): Promise<T[]>;

  existsDbFileSync(db: string): boolean;

  getDbDirectoryPath(): string;

  reload(): Promise<void>;
}
