import {WindowFactory} from './window.factory';
import {IpcChannel} from '../shared/ipc.chanels';
import {Logger} from './logger';
const { nativeTheme } = require('electron');

export namespace DarkModeHandler {
  export function init() {
    Logger.debug('Init dark mode handler');
    WindowFactory.sendBroadcast(IpcChannel.DarkMode, nativeTheme.shouldUseDarkColors);
    nativeTheme.on('updated', function (evt) {
      WindowFactory.sendBroadcast(IpcChannel.DarkMode, nativeTheme.shouldUseDarkColors);
    });
  }
}
