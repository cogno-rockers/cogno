import {Clock} from '../../../../../common/clock';
import {CommandTree} from './command.tree';
import {ShellType} from '../../../../../../../shared/models/models';
import {CommandNormalizer} from '../normalizer/command-normalizer';
import {Hash} from '../../../../../+shared/models/hash/hash';
import {MatchShellType, NormalizedSuggestResult} from '../models';
import {Normalizer} from '../../../../../../../shared/normalizer';
import {TestClock, TestHelper} from '../../../../../../../test/helper';
import {ShellLocation} from '../../../../../common/shellLocation';
import {SuggestionType} from '../../suggester';
import {SuggestRequestBuilder} from '../../suggestRequestBuilder';


describe('CommandTree', () => {

  const testClock: Clock = new TestClock();
  let commandNormalizer: CommandNormalizer;
  let requestBuilder: SuggestRequestBuilder;

  beforeEach(() => {
    const normalizer = new Normalizer(new ShellLocation('', ShellType.Powershell));
    commandNormalizer = new CommandNormalizer(normalizer);
    requestBuilder = new SuggestRequestBuilder(normalizer);
  });

  it('should suggest nothing if nothing exists', () => {
    const tree = new CommandTree(testClock);

    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test'], 'cd pr', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([]);
  });

  it('should suggest nothing cd', () => {
    const tree = new CommandTree(testClock);
    const commands = commandNormalizer.normalize(['c'], 'cd test', [ShellType.Bash]);
    tree.saveExecution(...commands);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test'], 'cd pr', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([]);
  });

  it('should suggest deep cd', () => {
    const tree = new CommandTree(testClock);
    const commands1 = commandNormalizer.normalize(['c', 'test'], 'cd devlab', [ShellType.GitBash]);
    const commands2 = commandNormalizer.normalize(['c', 'proj'], 'cd mistral', [ShellType.GitBash]);
    tree.saveExecution(...commands1);
    tree.saveExecution(...commands2);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'cd test dev', ShellType.GitBash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.Navigation,
        executionDate: testClock.now(),
        executionCount: 0,
        globalExecutionDate: testClock.now(),
        globalExecutionCount: 0,
        globalSelectionCount: 0,
        globalSelectionDate: undefined,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: ['test'],
        isPathToDirectoryFromRoot: false,
        matchShellType: MatchShellType.Yes
      },
      {
        suggestionType: SuggestionType.Navigation,
        executionDate: testClock.now(),
        executionCount: 1,
        selectionCount: 0,
        selectionDate: undefined,
        globalExecutionDate: testClock.now(),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalSelectionDate: undefined,
        pathToDirectory: ['test', 'devlab'],
        isPathToDirectoryFromRoot: false,
        matchShellType: MatchShellType.Yes
      }
    ]);
  });

  it('should suggest cd', () => {
    const tree = new CommandTree(testClock);
    const commands = commandNormalizer.normalize(['c', 'test'], 'cd project', [ShellType.Bash]);
    tree.saveExecution(...commands);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test'], 'cd pr', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.Navigation,
        executionDate: testClock.now(),
        executionCount: 1,
        globalExecutionDate: testClock.now(),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalSelectionDate: undefined,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: ['project'],
        isPathToDirectoryFromRoot: false,
        matchShellType: MatchShellType.Yes
      }]);
  });

  it('should remove whitespaces', () => {
    const tree = new CommandTree(testClock);
    const commands = commandNormalizer.normalize(['c', 'test'], ' cd project ', [ShellType.Powershell, ShellType.Bash]);
    tree.saveExecution(...commands);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test'], 'cd pr', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.Navigation,
        executionDate: testClock.now(),
        executionCount: 1,
        globalExecutionDate: testClock.now(),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalSelectionDate: undefined,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: ['project'],
        isPathToDirectoryFromRoot: false,
        matchShellType: MatchShellType.Yes
      }]);
  });

  it('should suggest cd deep - windows', () => {
    const tree = new CommandTree(testClock);
    const commands = commandNormalizer.normalize(['c', 'test'], 'cd project', [ShellType.Powershell]);
    tree.saveExecution(...commands);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'cd pr', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.Navigation,
        executionDate: testClock.now(),
        executionCount: 1,
        globalExecutionDate: testClock.now(),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalSelectionDate: undefined,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: ['test', 'project'],
        isPathToDirectoryFromRoot: false,
        matchShellType: MatchShellType.Yes
      }]);
  });

  it('should suggest cd deep - linux', () => {
    const tree = new CommandTree(testClock);
    const command = commandNormalizer.normalize(['c', 'test'], 'cd project', [ShellType.Bash]);
    tree.saveExecution(...command);

    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'cd pr', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.Navigation,
        executionDate: testClock.now(),
        executionCount: 1,
        globalExecutionDate: testClock.now(),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalSelectionDate: undefined,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: ['test', 'project'],
        isPathToDirectoryFromRoot: false,
        matchShellType: MatchShellType.Yes
      }]);
  });

  it('should suggest cd relative', () => {
    const tree = new CommandTree(testClock);
    const command1 = commandNormalizer.normalize(['c', 'project'], 'cd kingfisher/api', [ShellType.Bash]);
    tree.saveExecution(...command1);
    const command2 = commandNormalizer.normalize(['c', 'project'], 'cd logether/api', [ShellType.Bash]);
    tree.saveExecution(...command2);

    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'project', 'kingfisher', 'api'], 'cd lo ap', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions[0].pathToDirectory).toEqual(['..', '..', 'logether']);
    expect(result.suggestions[0].isPathToDirectoryFromRoot).toBeFalsy();
    expect(result.suggestions[1].pathToDirectory).toEqual(['..', '..', 'logether', 'api']);
    expect(result.suggestions[0].isPathToDirectoryFromRoot).toBeFalsy();
  });

  it('should suggest cd relative - same directory to search', () => {
    const tree = new CommandTree(testClock);
    const command1 = commandNormalizer.normalize(['c', 'project'], 'cd kingfisher', [ShellType.Bash]);
    tree.saveExecution(...command1);

    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'project', 'kingfisher'], 'cd ki', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions.length).toBeFalsy();
  });

  it('should suggest cd absolute', () => {
    const tree = new CommandTree(testClock);
    const command1 = commandNormalizer.normalize(['c', 'kingfisher'], 'cd api', [ShellType.Bash]);
    tree.saveExecution(...command1);
    const command2 = commandNormalizer.normalize(['e', 'logether'], 'cd api', [ShellType.Bash]);
    tree.saveExecution(...command2);

    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'kingfisher', 'api'], 'cd loge', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions[0].pathToDirectory).toEqual(['e', 'logether']);
    expect(result.suggestions[0].isPathToDirectoryFromRoot).toBeTruthy();
    expect(result.suggestions[1].pathToDirectory).toEqual(['e', 'logether', 'api']);
    expect(result.suggestions[0].isPathToDirectoryFromRoot).toBeTruthy();
  });

  it('should recognize connected commands - linux', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Bash)));
    const command = commandNormalizer.normalize(['c', 'test'], 'cd project && npm start', [ShellType.Bash]);
    tree.saveExecution(...command);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'cd pr', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.Navigation,
        executionDate: testClock.now(),
        executionCount: 1,
        globalExecutionDate: testClock.now(),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalSelectionDate: undefined,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: ['test', 'project'],
        isPathToDirectoryFromRoot: false,
        matchShellType: MatchShellType.Yes
      }]);
  });

  it('should recognize connected commands in new folder - linux', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Bash)));
    const command = commandNormalizer.normalize(['c', 'test'], 'cd project && npm start', [ShellType.Bash]);
    tree.saveExecution(...command);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'npm s', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.SemiLocalCommand,
        shellCommand: 'npm start',
        commandHash: Hash.create('npm start'),
        executionDate: testClock.now(),
        executionCount: 1,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: ['test', 'project'],
        isPathToDirectoryFromRoot: false
      }]);
  });

  it('should enrich command', () => {
    const tree = new CommandTree(testClock);
    const command = commandNormalizer.normalize(['c'], 'npm start', [ShellType.Powershell]);
    tree.saveExecution(...command);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'npm s', ShellType.Powershell));

    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.LocalCommand,
        shellCommand: 'npm start',
        executionDate: testClock.now(),
        executionCount: 1,
        selectionCount: 0,
        selectionDate: undefined,
        commandHash: Hash.create('npm start'),
        pathToDirectory: [],
        isPathToDirectoryFromRoot: false
      }]);
  });

  it('should enrich command with cd - linux', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Bash)));
    const command = commandNormalizer.normalize(['c', 'test'], 'npm start', [ShellType.Bash]);
    tree.saveExecution(...command);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'npm s', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.SemiLocalCommand,
        shellCommand: 'npm start',
        executionDate: testClock.now(),
        executionCount: 1,
        selectionCount: 0,
        selectionDate: undefined,
        commandHash: Hash.create('npm start'),
        pathToDirectory: ['test'],
        isPathToDirectoryFromRoot: false
      }]);
  });

  it('should not enrich command with cd if command was executed in other folder', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Powershell)));
    const command1 = commandNormalizer.normalize(['c', 'test'], 'npm start', [ShellType.Powershell]);
    tree.saveExecution(...command1);
    const command2 = commandNormalizer.normalize(['c', 'test'], 'cd test2', [ShellType.Powershell]);
    tree.saveExecution(...command2);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test', 'test2'], 'npm s', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.GlobalCommand,
        shellCommand: 'npm start',
        commandHash: Hash.create('npm start')
      }]);
  });

  it('should return input command if command is not available', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Powershell)));
    const command = commandNormalizer.normalize(['c', 'test'], 'cd project', [ShellType.Powershell]);
    tree.saveExecution(...command);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'npm s', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.GlobalCommand,
        shellCommand: 'npm start',
        commandHash: Hash.create('npm start')
      }]);
  });

  it('should work with pipe', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Powershell)));
    const command = commandNormalizer.normalize(['c', 'test'], 'cat filename | less', [ShellType.Powershell]);
    tree.saveExecution(...command);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test'], 'ca', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'cat filename | less', commandHash: Hash.create('cat filename | less')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.LocalCommand,
        shellCommand: 'cat filename | less',
        commandHash: Hash.create('cat filename | less'),
        executionDate: testClock.now(),
        executionCount: 1,
        selectionCount: 0,
        selectionDate: undefined,
        pathToDirectory: [],
        isPathToDirectoryFromRoot: false
      }]);
  });

  it('should work after restore', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Powershell)));
    const command = commandNormalizer.normalize(['c', 'test'], 'npm start', [ShellType.Powershell])[0];
    tree.restoreCommand(command.directories,{
      _id: undefined,
      commandId: command.commandHash,
      directoryId: 'c_test',
      executionCount: 2,
      selectionCount: 3,
      lastSelectionDate: testClock.now(-1000),
      lastExecutionDate: testClock.now()
    });
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'npm s', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {
        suggestionType: SuggestionType.SemiLocalCommand,
        commandHash: Hash.create('npm start'),
        shellCommand: 'npm start',
        executionDate: testClock.now(),
        executionCount: 2,
        selectionCount: 3,
        selectionDate: testClock.now(-1000),
        pathToDirectory: ['test'],
        isPathToDirectoryFromRoot: false
      }]);
  });

  it('should work with restore null or undefined', () => {
    const tree = new CommandTree(testClock);
    tree.restoreCommand(null, null);
    tree.restoreCommand(undefined, undefined);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'npm s', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([
      {shellCommand: 'npm start', commandHash: Hash.create('npm start')}]);
  });

  it('should work with cd .. - windows', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Powershell)));
    const commands = commandNormalizer.normalize(['c', 'test'], 'cd ..', [ShellType.Bash]);
    tree.saveExecution(...commands);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test'], 'cd', ShellType.Powershell));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([]);
  });

  it('should work with cd .. - linux', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Bash)));
    const commands = commandNormalizer.normalize(['c', 'test'], 'cd ..', [ShellType.Bash]);
    tree.saveExecution(...commands);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c', 'test'], 'cd', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    tree.apply(search, result);
    expect(result.suggestions).toEqual([]);
  });

  it('should work with execute command in root - linux', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Bash)));
    const commands = commandNormalizer.normalize([''], 'npm start', [ShellType.Bash]);
    tree.saveExecution(...commands);
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'npm', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}];
    tree.apply(search, result);
    expect(result.suggestions).toEqual([{
      suggestionType: SuggestionType.LocalCommand,
      shellCommand: 'npm start',
      commandHash: Hash.create('npm start'),
      executionDate: testClock.now(),
      executionCount: 1,
      selectionDate: undefined,
      selectionCount: 0,
      pathToDirectory: [],
      isPathToDirectoryFromRoot: false
    }]);
  });

  it('should set min max date', () => {
    const tree = new CommandTree(testClock);
    commandNormalizer = new CommandNormalizer(new Normalizer(new ShellLocation('', ShellType.Bash)));
    const command1 = commandNormalizer.normalize(['c', 'test'], 'npm start', [ShellType.Bash])[0];
    const command2 = commandNormalizer.normalize(['c', 'test'], 'npm init', [ShellType.Bash])[0];
    tree.restoreCommand(command1.directories,{
      _id: undefined,
      commandId: command1.commandHash,
      directoryId: 'c_test',
      executionCount: 2,
      selectionCount: 3,
      lastSelectionDate: new Date(4000),
      lastExecutionDate: new Date(3000),
    });
    tree.restoreCommand(command2.directories,{
      _id: undefined,
      commandId: command2.commandHash,
      directoryId: 'c_test',
      executionCount: 4,
      selectionCount: 5,
      lastSelectionDate: new Date(2000),
      lastExecutionDate: new Date(1000)
    });
    const search = requestBuilder.build(TestHelper.buildAutocompleteRequest(['c'], 'npm', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'npm start', commandHash: Hash.create('npm start')}, {
      shellCommand: 'npm init',
      commandHash: Hash.create('npm init')
    }];
    tree.apply(search, result);
    expect(result.scoringValues.minExecutionDate).toEqual(new Date(1000));
    expect(result.scoringValues.maxExecutionDate).toEqual(new Date(3000));
    expect(result.scoringValues.minSelectionDate).toEqual(new Date(2000));
    expect(result.scoringValues.maxSelectionDate).toEqual(new Date(4000));
  });
});
