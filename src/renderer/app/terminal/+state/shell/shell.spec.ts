import {Shell} from './shell';
import {SessionDataType, ShellType} from '../../../../../shared/models/models';
import {Chars} from '../../../../../shared/chars';
import {TestHelper} from '../../../../../test/helper';
import {clear, getElectronService} from '../../../../../test/factory';
import {ElectronService} from '../../../+shared/services/electron/electron.service';

describe('Shell', () => {

  let shell: Shell;
  let electron: ElectronService;

  beforeEach(() => {
    clear();
    electron = getElectronService();
    shell = new Shell(electron);
  });

  function testShellStart(shellType: ShellType) {
    const spySend = jest.spyOn(electron, 'send').mockImplementation(() => {});
    const config =  {...TestHelper.createShellConfig(), type: shellType};
    shell.init(config, 'id123');
    expect(spySend.mock.calls[0][1].id).toEqual('id123');
    expect(spySend.mock.calls[0][1].type).toEqual(SessionDataType.New);
    expect(spySend.mock.calls[0][1].settings.type).toEqual(shellType);
  }

  it('should change prompt on init - bash', () => {
    testShellStart(ShellType.Bash);
  });

  it('should change prompt on init - powershell', () => {
    testShellStart(ShellType.Powershell);
  });

  it('should change prompt on init - zsh', () => {
    testShellStart(ShellType.ZSH);
  });

  it('should replace multiline during paste', () => {
    const s = `a\nb`;
    const spy = jest.spyOn(shell, 'write').mockImplementation((s) => {
    });
    jest.spyOn(electron, 'send').mockImplementation(() => {});
    jest.spyOn(electron, 'on').mockImplementation((channel, listener) => {return null});
    shell.init( TestHelper.createShellConfig(), 'id123');
    shell.sessionListener(null, [{id: 'id123', data: 'abc', type: SessionDataType.Output}]);
    shell.sessionListener(null, [{id: 'id123', data: TestHelper.createShellConfig().promptTerminator + ' ', type: SessionDataType.Output}]);
    shell.paste(s, null, {hasSelection: false, start: undefined, end: undefined, text: undefined});
    const params = spy.mock.calls[spy.mock.calls.length - 1];
    expect(params[0]).toEqual('a\tb');
  });
});
