import {ShellConfig, Theme} from '../../../../../../shared/models/settings';
import {Position, RendererState} from '../../../../../../shared/models/models';
import {RegexpHelper} from '../../../../common/regexp';
import {Selection} from '../../models';
import {Chars} from '../../../../../../shared/chars';

export abstract class ShellAdapter {

  public readonly config: ShellConfig;

  constructor(config: ShellConfig) {
    this.config = config;
  }

  abstract cursorLeftSequence(): string;

  abstract cursorRightSequence(): string;

  abstract backspaceSequence(): string;

  abstract abortTask(): string;

  abstract injectCommands(script: string, theme: Theme): string[];

  clearLine(rendererState: RendererState): string {
    if (rendererState.currentInput.lines.length === 0) {
      return '';
    }
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const moveCursorX = Math.max(rendererState.currentInput.input.trimEnd().length - cursorPosition.x, 0);
    const backspaceCount = cursorPosition.x + moveCursorX;
    return this.cursorRightSequence().repeat(moveCursorX) + this.backspaceSequence().repeat(backspaceCount);
  }

  clearLineToEnd(rendererState: RendererState): string {
    if (rendererState.currentInput.lines.length === 0) {
      return '';
    }
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    if (cursorPosition.x > rendererState.currentInput.input.trimEnd().length) {
      return '';
    }
    const moveCursorX = Math.max(rendererState.currentInput.input.trimEnd().length - cursorPosition.x, 0);
    return this.cursorRightSequence().repeat(moveCursorX) + this.backspaceSequence().repeat(moveCursorX);
  }

  clearLineToStart(rendererState: RendererState): string {
    if (rendererState.currentInput.lines.length === 0) {
      return '';
    }
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const backspaceCount = cursorPosition.x;
    return this.backspaceSequence().repeat(backspaceCount);
  }

  cursorToNextWord(rendererState: RendererState): { steps: number; command: string } {
    const steps = this.goToNextWordSteps(rendererState);
    return {command: this.cursorRightSequence().repeat(steps), steps: steps};
  }

  private goToNextWordSteps(rendererState: RendererState): number {
    if (rendererState.currentInput.lines.length === 0) {
      return 0;
    }
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const positionNextWord = RegexpHelper.regexIndexOf(rendererState.currentInput.input, /\s\S/g, cursorPosition.x + 1);
    positionNextWord.index = positionNextWord.index <= 0 ? rendererState.currentInput.input.trimEnd().length : positionNextWord.index + 1;
    return Math.max(positionNextWord.index - cursorPosition.x, 0);
  }

  cursorToPreviousWord(rendererState: RendererState): { steps: number; command: string } {
    const steps = this.goToPreviousWordSteps(rendererState);
    return {command: this.cursorLeftSequence().repeat(steps), steps: steps};
  }

  private goToPreviousWordSteps(rendererState: RendererState): number {
    if (rendererState.currentInput.lines.length === 0) {
      return 0;
    }
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const positionPreviousWord = RegexpHelper.regexIndexOfReverse(rendererState.currentInput.input, /\s\S/g, cursorPosition.x);
    positionPreviousWord.index = positionPreviousWord.index <= 0 ? 0 : positionPreviousWord.index + 1;
    return cursorPosition.x - positionPreviousWord.index;
  }

  goToNextArgument(rendererState: RendererState): {command: string; placeholder?: string} {
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const nextPlaceholder = RegexpHelper.regexIndexOf(rendererState.currentInput.input, new RegExp(RegexpHelper.RegexCommandPlaceholder, 'g'), cursorPosition.x);
    if (nextPlaceholder.index < 0) {
      return {command: '', placeholder: undefined};
    }
    const indexEndOfPlaceholder = nextPlaceholder.index === 0 ? rendererState.currentInput.input.length : nextPlaceholder.index + nextPlaceholder.length;
    const curserRightSteps = Math.max(indexEndOfPlaceholder - cursorPosition.x, 0);
    const backspaceSteps = Math.max(nextPlaceholder.length, 0);
    const placeholder = rendererState.currentInput.input.substring(nextPlaceholder.index, indexEndOfPlaceholder);
    return {command: this.cursorRightSequence().repeat(curserRightSteps) + this.backspaceSequence().repeat(backspaceSteps), placeholder: placeholder};
  }

  goToPreviousArgument(rendererState: RendererState): {command: string; placeholder?: string} {
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const previousPlaceholder = RegexpHelper.regexIndexOfReverse(rendererState.currentInput.input, new RegExp(RegexpHelper.RegexCommandPlaceholder, 'g'), cursorPosition.x);
    if (previousPlaceholder.index < 0) {
      return {command: '', placeholder: undefined};
    }
    const indexEndOfPlaceholder  = previousPlaceholder.index === 0 ? 0 : previousPlaceholder.index + previousPlaceholder.length;
    const curserRightSteps = cursorPosition.x - indexEndOfPlaceholder;
    const backspaceSteps = Math.max(previousPlaceholder.length, 0);
    const placeholder = rendererState.currentInput.input.substring(previousPlaceholder.index, indexEndOfPlaceholder);
    return {command: this.cursorLeftSequence().repeat(curserRightSteps) + this.backspaceSequence().repeat(backspaceSteps), placeholder: placeholder};
  }

  deleteNextWord(rendererState: RendererState): string {
    const steps = this.goToNextWordSteps(rendererState);
    return this.cursorRightSequence().repeat(steps) + this.backspaceSequence().repeat(steps);
  }

  deletePreviousWord(rendererState: RendererState): string {
    const steps = this.goToPreviousWordSteps(rendererState);
    return this.backspaceSequence().repeat(steps);
  }

  cursorLeft(rendererState: RendererState): string | undefined {
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    return cursorPosition.x > 0 ? this.cursorLeftSequence() : undefined;
  }

  cursorRight(rendererState: RendererState): string | undefined {
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    return cursorPosition.x < rendererState.currentInput.input.length ? this.cursorRightSequence() : undefined;
  }

  cursorToStartOfLine(rendererState: RendererState): { command: string; steps: number } {
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const steps = cursorPosition.x;
    return {command: this.cursorLeftSequence().repeat(steps), steps: steps};
  }

  cursorToEndOfLine(rendererState: RendererState): { command: string; steps: number } {
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const steps = Math.max(rendererState.currentInput.input.trimEnd().length - cursorPosition.x, 0);
    return {command: this.cursorRightSequence().repeat(steps), steps: steps};
  }

  cut(rendererState: RendererState, selection: Selection): string {
    if (!selection.hasSelection) {
      return '';
    }
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const flattenSelection = this.normalizedSelection(selection, rendererState, cursorPosition);
    if (!flattenSelection) {
      return '';
    }
    const diffCursorToSelection = cursorPosition.x - flattenSelection.end;
    const moveCursor = diffCursorToSelection < 0 ? this.cursorRightSequence().repeat(Math.abs(diffCursorToSelection)) : this.cursorLeftSequence().repeat(diffCursorToSelection);
    return moveCursor + this.backspaceSequence().repeat(flattenSelection.end - flattenSelection.start);

  }

  paste(stringToPaste: string, rendererState: RendererState, selection: Selection): string {
    if (!stringToPaste) {
      return stringToPaste;
    }
    if (!selection.hasSelection) {
      return stringToPaste;
    }
    const cursorPosition = this.normalizedCursorPosition(rendererState);
    const flattenSelection = this.normalizedSelection(selection, rendererState, cursorPosition);
    if (!flattenSelection) {
      return stringToPaste;
    }
    const diffCursorToSelection = cursorPosition.x - flattenSelection.end;
    const moveCursor = diffCursorToSelection < 0 ? this.cursorRightSequence().repeat(Math.abs(diffCursorToSelection)) : this.cursorLeftSequence().repeat(diffCursorToSelection);
    const backspaceRepeat = flattenSelection.end - flattenSelection.start + (stringToPaste === Chars.Delete.asString ? -1 : 0);
    return moveCursor + this.backspaceSequence().repeat(backspaceRepeat) + stringToPaste;
  }


  private isSelectionValid(start: Position, end: Position): boolean {
    return start.x !== end.x;
  }

  private normalizedSelection(selection: Selection, rendererState: RendererState, cursorPosition: Position): {
    start: number;
    end: number;
  } {
    const start = this.normalizePosition(selection.start, rendererState);
    const end = this.normalizePosition(selection.end, rendererState);
    if (!this.isSelectionValid(start, end)) {
      return undefined;
    }
    if (start.y < cursorPosition.y) {
      start.x = 0;
    }
    return {start: start.x, end: end.x};
  }

  private normalizedCursorPosition(rendererState: RendererState): Position {
    return this.normalizePosition({
      x: rendererState.cursorPosition?.x ?? 0,
      y: rendererState.cursorPosition?.line ?? 0
    }, rendererState);
  }

  private normalizePosition(position: Position, rendererState: RendererState): Position {
    if (!position || !rendererState) {
      return {x: 0, y: 0};
    }
    const promptOffset = rendererState.currentInput.lines.length === 0 || rendererState.currentInput.lines[0].line === position.y ? rendererState.cursorPosition.offsetX : 0;
    let xOffset = 0;
    const lines = rendererState.currentInput.lines;
    let currentLine = lines[0]?.line || rendererState.cursorPosition.line;
    const minLine = lines[0]?.line || rendererState.cursorPosition.line;
    const maxLine = lines[lines.length - 1]?.line || rendererState.cursorPosition.line;
    if (position.y < minLine) {
      return {x: 0, y: 0};
    }
    if (position.y > maxLine) {
      return {x: rendererState.currentInput.lines.reduce((prev, curr) => prev + curr.lineText.length, 0), y: 0};
    }
    while (currentLine < position.y) {
      const line = rendererState.currentInput.lines.find(l => l.line === currentLine);
      xOffset += line?.lineText?.length || 0;
      currentLine++;
    }
    return {x: position.x + xOffset - promptOffset, y: 0};
  }
}
