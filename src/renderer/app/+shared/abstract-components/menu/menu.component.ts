import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {Key} from '../../../common/key';
import {KeyboardService} from '../../services/keyboard/keyboard.service';
import {MenuService} from './menu.service';
import {ShortcutKey} from '../../../../../shared/models/shortcuts';
import {KeytipService} from '../../services/keyboard/keytip.service';

@Component({
    template: '',
    standalone: false
})
export abstract class MenuComponent implements OnDestroy, AfterViewInit{

  protected subscriptions: Subscription[] = [];

  constructor(
    protected eRef: ElementRef,
    protected menuService: MenuService,
    protected keyboardService: KeyboardService,
    protected keytipService: KeytipService,
    ) {
    this.subscriptions.push(this.menuService.selectIsActive().subscribe(isActive => {
      if (isActive) {
        eRef.nativeElement.focus();
      } else {
        eRef.nativeElement.blur();
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  ngAfterViewInit(): void {
    this.eRef.nativeElement.tabIndex = 1;
    this.eRef.nativeElement.addEventListener('keydown', (keyevent) => this.handleKeyDown(keyevent));
  }

  @HostListener('document:click', ['$event'])
  click(event: MouseEvent) {
    if (this.isEventOutside(event)) {
      this.closeMenu();
    }
  }

  private handleKeyDown(event: KeyboardEvent) {
    if (this.menuService.isOpened() && event.key === Key.Escape) {
      this.closeMenu();
      return;
    }
    if(this.menuService.getIsActive()){
      if(this.keyboardService.isShortcut(event)) {
        this.closeMenu();
        this.keyboardService.handleShortcut(event);
      }
      if(this.keytipService.isKeytip(event)) {
        this.closeMenu();
        this.keytipService.handleKeytip(event);
      }
    }
  }

  protected closeMenu($event: MouseEvent = null) {
    if ($event && !$event.relatedTarget) {
      return;
    }
    this.eRef.nativeElement.blur();
    this.menuService.closeMenu();
  }

  protected addKeydownListener(listener: (event: KeyboardEvent) => void) {
    this.eRef.nativeElement.addEventListener('keydown', listener);
  }

  private isEventOutside(event: MouseEvent): boolean {
    return this.menuService.isOpened() && !this.eRef.nativeElement.contains(event.target);
  }

  protected openOnShortcut(key: ShortcutKey) {
    this.subscriptions.push(this.keyboardService.onShortcut(key)
      .subscribe(() => {
        this.menuService.openMenu();
      }));
  }
}
