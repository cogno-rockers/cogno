import {Injectable, OnDestroy} from '@angular/core';
import {finalize, map} from 'rxjs/operators';
import {Color} from '../../../../../shared/color';
import {ColorName, Theme} from '../../../../../shared/models/settings';
import {combineLatest, Observable, of, Subscription} from 'rxjs';
import {createStore, Store} from '../../../common/store/store';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {FontsLoaderService} from '../../../+shared/services/fonts/fonts-loader.service';
import {SettingsService} from '../../../+shared/services/settings/settings.service';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {SettingsTabService} from '../../+state/settings-tab.service';
import {NotificationService} from '../../../+shared/notification/notification.service';
import isequal from 'lodash.isequal';
import clonedeep from 'lodash.clonedeep';
import {ThemesLoaderService} from './themes.loader.service';
import {WindowService} from '../../../+shared/services/window/window.service';

export interface ThemesSettingsState {
  isAvailableInNewTab: boolean;
  isAvailableAfterRestart: boolean;
  forceResizeOnSave: boolean;
  fonts: string[];
  appFonts: string[];
  availableThemes: Theme[];
  installedThemes: Theme[];
  selectedTheme: Theme;
  selectedThemeOriginal: Theme;
  isThemesLoading: boolean;
  isThemesLoadError: boolean;
  isThemeDetailsLoading: boolean;
  isThemeDetailsLoadError: boolean;
}

export const initialState: ThemesSettingsState = {
  isAvailableAfterRestart: false,
  isAvailableInNewTab: false,
  availableThemes: [],
  installedThemes: [],
  fonts: [],
  appFonts: [],
  selectedTheme: null,
  selectedThemeOriginal: null,
  isThemesLoading: false,
  isThemesLoadError: false,
  isThemeDetailsLoading: false,
  isThemeDetailsLoadError: false,
  forceResizeOnSave: false
};

@Injectable()
export class ThemesSettingsService implements OnDestroy {

  private subscriptions: Subscription[] = [];
  private store: Store<ThemesSettingsState>;

  constructor(
    private electron: ElectronService,
    private themesLoader: ThemesLoaderService,
    private fontsLoader: FontsLoaderService,
    private settingsService: SettingsService,
    private tabService: SettingsTabService,
    private notification: NotificationService,
    private windowService: WindowService
  ) {
  }

  public init(id: string) {
    this.store = createStore(id, initialState);
    this.store.update({isThemesLoading: true, isThemesLoadError: false});

    this.subscriptions.push(this.settingsService.selectThemes().subscribe(themes => {
        themes.forEach(t => t.isInstalled = true);
        const selectedTheme = this.store.get(s => s.selectedTheme) || themes.find(t => t.isDefault) || themes[0];
        this.tabService.updateIsLoading(false);
        this.store.update({installedThemes: themes.sort((a, b) => a.name.localeCompare(b.name)), selectedTheme: clonedeep(selectedTheme), selectedThemeOriginal: clonedeep(selectedTheme)});
        if(this.store.get(s => s.forceResizeOnSave)) {
          this.windowService.resize();
          this.store.update({forceResizeOnSave: false});
        }
    }),
      this.themesLoader.loadThemes().subscribe({
        next: themes => {
          this.store.update({
            availableThemes: themes,
            isThemesLoading: false
          });
        },
        error: e => this.store.update({isThemesLoading: false, isThemesLoadError: true})
      }),
      this.fontsLoader.loadFonts().subscribe(fonts => this.store.update({fonts: fonts})),
      this.fontsLoader.loadAppFonts().subscribe(fonts => this.store.update({appFonts: fonts}))
    );
  }

  _isInstalled(theme: Theme): boolean {
    return !!this.store.get(s => s.installedThemes).find(installedTheme => theme.name === installedTheme.name);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.store.destroy();
  }

  getState(): ThemesSettingsState {
    return {...this.store.get(s => s)};
  }

  selectAvailableThemes() {
    return this.store.select(state => state.availableThemes.filter(availableTheme => !this._isInstalled(availableTheme)));
  }

  selectInstalledThemes() {
    return this.store.select(state => state.installedThemes);
  }

  selectSelectedTheme() {
    return this.store.select(state => state.selectedTheme);
  }

  selectFonts() {
    return this.store.select(state => state.fonts);
  }

  selectAppFonts() {
    return this.store.select(state => state.appFonts);
  }

  selectFooterHeaderColor() {
    return this.selectSelectedTheme().pipe(map(t => {
      if (t?.colors?.background) {
        const isLightTheme = Color.isLight(t.colors.background);
        const factore = isLightTheme ? -1 : 1;
        return Color.lightenDarkenColor(t.colors.background, 20 * factore);
      }
      return '';
    }));
  }

  selectThemesIsLoading(): Observable<boolean> {
    return this.store.select(s => s.isThemesLoading);
  }

  selectThemeDetailsIsLoading(): Observable<boolean> {
    return this.store.select(s => s.isThemeDetailsLoading);
  }

  selectThemeDetailsIsLoadError(): Observable<boolean> {
    return this.store.select(s => s.isThemeDetailsLoadError);
  }

  selectIsLoadError(): Observable<boolean> {
    return this.store.select(s => s.isThemesLoadError);
  }

  selectThemeIsChanged(): Observable<boolean> {
    return combineLatest([this.store.select(s => s.selectedTheme), this.store.select(s => s.selectedThemeOriginal)])
      .pipe(map(([theme, original]) => !isequal(theme, original)));
  }

  selectIsAvailableAfterRestart(): Observable<boolean> {
    return this.store.select(s => s.isAvailableAfterRestart);
  }

  selectIsAvailableInNewTab(): Observable<boolean> {
    return this.store.select(s => s.isAvailableInNewTab);
  }

  selectPromptColors(): Observable<{foreground?: string; background?: string}[]> {
    return this.store.select(s => s.selectedTheme?.colors && s.selectedTheme?.colors?.promptColors);
  }

  updateSelectedTheme(theme: Theme): void {
    this.loadThemeDetails(theme).subscribe(t => {
      this.store.update({selectedTheme: t, selectedThemeOriginal: clonedeep(t) });
    });
  }

  private loadThemeDetails(theme: Theme): Observable<Theme> {
    if (theme.colors === null && !!theme.terminalSexyId) {
      this.store.update({isThemeDetailsLoading: true, isThemeDetailsLoadError: false});
      const defaultTheme = this.store.get(s => s.installedThemes).find(t => t.isDefault);
      return this.themesLoader.loadThemeDetails(theme.terminalSexyId, theme.name, defaultTheme).pipe(
        finalize(() => this.store.update({isThemeDetailsLoading: false}))
      );
    } else {
      return of(theme);
    }
  }

  openExternal(link: string) {
    this.electron.openExternal(`https:\\${link}`);
  }

  setPromptVersion(version: string): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, promptVersion: Number.parseInt(version, 10)}, isAvailableInNewTab: true}));
  }

  setFontWeightBold(fontWeight: number): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, fontWeightBold:  fontWeight}}));
  }

  setFontWeight(fontWeight: number): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, fontWeight: fontWeight}}));
  }

  toggleWebgl(): void {
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, enableWebgl: !s.selectedTheme.enableWebgl},
      isAvailableInNewTab: true
    }));
  }

  toggleLigatures(): void {
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, enableLigatures: !s.selectedTheme.enableLigatures},
      isAvailableInNewTab: true
    }));
  }

  toggleBlinkingCursor(): void {
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, cursorBlink: !s.selectedTheme.cursorBlink}
    }));
  }

  setImage(pathToImage: string): void {
    const path = pathToImage.replace(/\\/g, '/');
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, image: path}}));
  }

  setImageBlur(value: number): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, imageBlur: value}}));
  }

  setImageOpacity(value: number): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, imageOpacity: value}}));
  }

  openImageSelection(): void {
    this.electron.openFileDialog('image').subscribe((path) => {
      if (path) {
        this.setImage(path);
      }
    });
  }

  setPrompt(prompt: string): void {
    if (prompt.length > 5) {
      prompt = prompt.substring(0, 5);
    }
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, prompt: prompt}, isAvailableInNewTab: true}));
  }

  setFontSize(size: string): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, fontsize: Number.parseInt(size, 10)}}));
  }

  setAppFontSize(size: string): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, appFontsize: Number.parseInt(size, 10)}, forceResizeOnSave: true}));
  }

  setFont(fontName: string): void {
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, fontFamily: fontName},
      isChanged: true,
      isAvailableInNewTab: true
    }));
  }

  setAppFont(fontName: string): void {
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, appFontFamily: fontName},
      isChanged: true,
      isAvailableAfterRestart: true
    }));
  }

  setCursorWidth(width: number): void {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, cursorWidth: width}}));
  }

  setCursorStyle(value: 'bar' | 'underline') {
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, cursorStyle: value}}));
  }

  setPromptBackgroundColor(index: number, colorName: ColorName): void {
    const colors = this.store.get(s => s.selectedTheme.colors);
    if (!colors.promptColors) {
      colors.promptColors = [];
    }
    for(let i = colors.promptColors.length - 1; i < index; i++) {
      colors.promptColors.push({foreground: ColorName.cyan, background: ColorName.black});
    }
    colors.promptColors[index].background = colorName;
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, colors: colors},
      isChanged: true,
      isAvailableInNewTab: true
    }));
  }

  setPromptColor(index: number, colorName: ColorName) {
    const colors = this.store.get(s => s.selectedTheme.colors);
    if (!colors.promptColors) {
      colors.promptColors = [];
    }
    for(let i = colors.promptColors.length - 1; i < index; i++) {
      colors.promptColors.push({foreground: ColorName.cyan, background: ColorName.black});
    }
    colors.promptColors[index].foreground = colorName;
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, colors: colors},
      isChanged: true,
      isAvailableInNewTab: true
    }));
  }

  saveThemeAndRelaunch() {
    setTimeout(() => this.electron.send(IpcChannel.Relaunch), 2000);
  }

  saveTheme() {
    const theme = this.store.get(s => s.selectedTheme);
    this._saveTheme(theme);
    this.store.update({selectedThemeOriginal: clonedeep(theme), isAvailableAfterRestart: false, isAvailableInNewTab: false});
  }

  setColor(colorAsHex: string, colorName: ColorName) {
    const colors = this.store.get(s => s.selectedTheme.colors);
    colors[colorName] = colorAsHex;
    this.store.update(s => ({selectedTheme: {...s.selectedTheme, colors: colors}}));
  }

  uninstallTheme() {
    const theme = this.store.get(s => s.selectedTheme);
    const installedThemes = this.store.get(s => s.installedThemes).filter(t => t.name !== theme.name);
    this.store.update(s => ({
      installedThemes: installedThemes,
      selectedTheme: installedThemes[0],
      selectedThemeOriginal: clonedeep(installedThemes[0])
    }));
    this.notification
      .create()
      .body(`${theme.name} deleted.`)
      .duration(5000)
      .dialog('Revert')
      .showAsSuccess()
      .subscribe((revert) => {
        if (!revert) {
          this.electron.send(IpcChannel.UninstallTheme, theme);
        } else {
          this.electron.send(IpcChannel.UninstallTheme, null);
        }
        this.tabService.updateIsLoading(true);
      });
  }

  setAsDefaultTheme() {
    const selectedTheme = this.store.get(s => s.selectedTheme);
    selectedTheme.isDefault = true;
    this._saveTheme(selectedTheme);
  }

  _saveTheme(theme: Theme) {
    this.tabService.updateIsLoading(true);
    this.electron.send(IpcChannel.SaveThemeConfig, theme);
  }

  installTheme() {
    const selectedTheme = {...this.store.get(s => s.selectedTheme)};
    selectedTheme.isDefault = true;
    selectedTheme.isInstalled = true;
    this.store.update({selectedTheme, selectedThemeOriginal: clonedeep(selectedTheme)});
    this.tabService.updateIsLoading(true);
    this.electron.send(IpcChannel.InstallTheme, selectedTheme);
  }

  resetTheme() {
    this.store.update(s => ({selectedTheme: clonedeep(s.selectedThemeOriginal), isAvailableAfterRestart: false, isAvailableInNewTab: false}));
  }

  setPadding(padding: string) {
    const theme = this.store.get(s => s.selectedTheme);
    if(!padding) {
      padding = '0';
    }
    theme.padding = padding;
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, padding: padding},
      isChanged: true
    }));
  }

  setAsDarkModeTheme() {
    const selectedTheme = this.store.get(s => s.selectedTheme);
    selectedTheme.isDarkModeTheme = true;
    this._saveTheme(selectedTheme);
  }

  toggleRemoveFullScreenAppPadding() {
    this.store.update(s => ({
      selectedTheme: {...s.selectedTheme, removeFullScreenAppPadding: !s.selectedTheme.removeFullScreenAppPadding},
      isChanged: true
    }));
  }


}
