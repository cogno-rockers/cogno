import {GeneralConfig} from '../models/settings';
import {GeneralParser} from './general.parser';

describe('general-validator', () => {

  let general: GeneralConfig;

  beforeEach(() => {
    general = {
      openTabInSameDirectory: false,
      enableTelemetry: true,
      enablePasteOnRightClick: false,
      enableCopyOnSelect: false,
      scrollbackLines: 100000,
    };
  });

  it('should return valid false if string is undefined', () => {
    expect(GeneralParser.parseGeneralConfig(undefined).isValid).toBeFalsy();
  });

  it('should return valid false if string is null', () => {
    expect(GeneralParser.parseGeneralConfig(null).isValid).toBeFalsy();
  });

  it('should return valid false if string is empty', () => {
    expect(GeneralParser.parseGeneralConfig('').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty object', () => {
    expect(GeneralParser.parseGeneralConfig('{}').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty array', () => {
    expect(GeneralParser.parseGeneralConfig('[]').isValid).toBeFalsy();
  });

  it('should return valid true if is valid', () => {
    expect(GeneralParser.parseGeneralConfig(JSON.stringify(general)).isValid).toBeTruthy();
  });
});
