import {GeneralSettingsService} from './+state/general.settings.service';
import {GeneralComponent} from './general.component';
import {ElectronService} from '../../+shared/services/electron/electron.service';
import {ShellsLoaderService} from '../../+shared/services/shells/shells-loader.service';
import {SettingsService} from '../../+shared/services/settings/settings.service';
import {
  clear,
  getElectronService,
  getGeneralSettingsService,
  getGridService,
  getSettingsService,
  getSettingsTabService,
  getShellsLoaderService
} from '../../../../test/factory';
import {SettingsTabService} from '../+state/settings-tab.service';
import {TestHelper} from '../../../../test/helper';
import {of} from 'rxjs';
import {GridService} from '../../global/grid/+state/grid.service';
import {BinaryTree} from '../../common/tree/binary-tree';
import {TabType} from '../../../../shared/models/models';
import {IpcChannel} from '../../../../shared/ipc.chanels';


describe('GeneralComponent', () => {
  let component: GeneralComponent;
  let service: GeneralSettingsService;
  let electronService: ElectronService;
  let shellsService: ShellsLoaderService;
  let settingsService: SettingsService;
  let settingsTabService: SettingsTabService;
  let gridService: GridService;

  beforeEach(() => {
    clear();
    electronService = getElectronService();
    gridService = getGridService();
    gridService.addNewTab(BinaryTree.ROOT_KEY, TabType.Settings);
    jest.spyOn(electronService, 'getVersion').mockReturnValue('1.0.0');
    shellsService = getShellsLoaderService();
    settingsService = getSettingsService();
    service = getGeneralSettingsService();
    settingsTabService = getSettingsTabService();
    settingsTabService.init(gridService.getActiveTabId());
    component = new GeneralComponent(settingsTabService, service);
  });

  it('should load shell on init component', () => {
    jest.spyOn(shellsService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig(), TestHelper.createShellConfig()]));
    component.ngOnInit();
    expect(shellsService.loadShells).toBeCalledTimes(1);
  });

  it('should toggleEnableTelemetry', () => {
    jest.spyOn(shellsService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig(), TestHelper.createShellConfig()]));
    jest.spyOn(electronService, 'send').mockReturnValue(void(0));
    component.ngOnInit();
    expect(service.getState().config.enableTelemetry).toEqual(true);
    component.toggleEnableTelemetry();
    expect(service.getState().config.enableTelemetry).toEqual(false);
    expect(settingsTabService.getState().isLoading).toEqual(true);
    expect(electronService.send).toBeCalledWith(IpcChannel.SaveGeneralConfig, service.getState().config);
  });

  it('should toggleEnablePasteOnRightClick', () => {
    jest.spyOn(shellsService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig(), TestHelper.createShellConfig()]));
    jest.spyOn(electronService, 'send').mockReturnValue(void(0));
    component.ngOnInit();
    expect(service.getState().config.enablePasteOnRightClick).toEqual(false);
    component.toggleEnablePasteOnRightClick();
    expect(service.getState().config.enablePasteOnRightClick).toEqual(true);
    expect(settingsTabService.getState().isLoading).toEqual(true);
    expect(electronService.send).toBeCalledWith(IpcChannel.SaveGeneralConfig, service.getState().config);
  });

  it('should toggleEnableCopyOnSelect', () => {
    jest.spyOn(shellsService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig(), TestHelper.createShellConfig()]));
    jest.spyOn(electronService, 'send').mockReturnValue(void(0));
    component.ngOnInit();
    expect(service.getState().config.enableCopyOnSelect).toEqual(false);
    component.toggleEnableCopyOnSelect();
    expect(service.getState().config.enableCopyOnSelect).toEqual(true);
    expect(settingsTabService.getState().isLoading).toEqual(true);
    expect(electronService.send).toBeCalledWith(IpcChannel.SaveGeneralConfig, service.getState().config);
  });

  it('should handle editShell correct', () => {
    jest.spyOn(shellsService, 'loadShells').mockReturnValue(of([]));
    jest.spyOn(electronService, 'send').mockReturnValue(void(0));
    component.ngOnInit();
    expect(service.getState().selectedShell).toEqual(null);
    expect(service.getState().isEditShellDialogVisible).toEqual(false);;
    component.editShell(service.getState().shells[0]);
    expect(service.getState().selectedShell).toEqual(service.getState().shells[0]);
    expect(service.getState().isEditShellDialogVisible).toEqual(true);
  });

  it('should handle showAddShellDialog correct', () => {
    jest.spyOn(shellsService, 'loadShells').mockReturnValue(of([]));
    jest.spyOn(electronService, 'send').mockReturnValue(void(0));
    component.ngOnInit();
    expect(service.getState().selectedShell).toEqual(null);
    expect(service.getState().isEditShellDialogVisible).toEqual(false);
    component.showAddShellDialog();
    expect(service.getState().selectedShell).toEqual({path: '', name: '', type: undefined, injectionType: 'Auto'});
    expect(service.getState().isEditShellDialogVisible).toEqual(true);
  });
});
