import {v4 as uuidv4} from 'uuid';

export namespace Uuid {
  export function create(): string {
    return uuidv4();
  }

  export function createSortable(): string {
    // Get the current timestamp (in milliseconds) as a prefix
    const timestamp = Date.now().toString(36); // Convert to base36 to reduce size

    // Generate a random UUID
    const uuid = uuidv4().replace(/-/g, ''); // Remove dashes for a cleaner string

    // Combine timestamp and UUID
    return `${timestamp}-${uuid}`;
  }
}
