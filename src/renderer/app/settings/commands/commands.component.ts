import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {CommandsSettingsService, TableEntry} from './+state/commands.settings.service';
import {SettingsTabService} from '../+state/settings-tab.service';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../../+shared/components/icon/icon.component';
import {FormsModule} from '@angular/forms';
import {FormatModule, SelectRowModule, SortModule, Tabulator} from 'tabulator-tables';
import {DateTime} from 'luxon';
import {Observable, Subscription} from 'rxjs';
import {CopyEditDeleteComponent} from '../../+shared/components/copy-edit-delete/copy-edit-delete.component';
import {EditCommandComponent} from './edit-command/edit-command.component';
import {Hash} from '../../+shared/models/hash/hash';
import {ParameterTableComponent} from './parameter-table/parameter-table.component';
import {GridService} from '../../global/grid/+state/grid.service';
import {SettingsTab} from '../../+shared/models/tab';
import {mdiAccount, mdiAccountHeart} from '@mdi/js';
import {CommandSource} from '../../+shared/models/command';

@Component({
    selector: 'app-commands',
    templateUrl: './commands.component.html',
    styleUrls: ['./commands.component.scss'],
    providers: [CommandsSettingsService],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        FormsModule,
        IconComponent,
        CopyEditDeleteComponent,
        EditCommandComponent
    ]
})
export class CommandsComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('searchInput')
  private searchInput: ElementRef;
  private table: Tabulator;
  tabId: string;
  search: Observable<string>;

  showEditCommand: Observable<boolean>;
  selectedCommand: Observable<TableEntry>;
  subscriptions: Subscription[] = [];

  constructor(
    private settingsService: SettingsTabService,
    private gridService: GridService,
    private service: CommandsSettingsService
  ) {
    Tabulator.registerModule(SortModule);
    Tabulator.registerModule(FormatModule);
    Tabulator.registerModule(SelectRowModule);
    this.tabId = this.settingsService.getTabId();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.searchInput.nativeElement.focus();
      this.showEditCommand = this.service.selectShowEditCommand();
      const tab = this.gridService.getTab(this.tabId) as SettingsTab;
      if(tab.config?.filter) {
        this.service.setSearch(tab.config.filter).then();
      }
    });
    this.table = new Tabulator(`#commands-table-${this.tabId}`, {
      layout: 'fitColumns',
      height: '100%',
      selectableRows: 1,
      columns: [
        {
          title: '', field: 'source', visible: true, width: 40, hozAlign: 'center', vertAlign: 'middle',
          formatter: cell => {
            const data = cell.getData() as TableEntry
            const icon = data.commandSource === CommandSource.EDITOR ? mdiAccountHeart : mdiAccount;
            return '<svg viewBox="2 2 20 20" style="display:inline-block;"> <path d="' + icon +'"/> </svg>'
          }
        },
        {title: 'Tool', field: 'tool', visible: true, hozAlign: 'left', vertAlign: 'middle', widthGrow: 1},
        {title: 'Command', field: 'command', hozAlign: 'left', vertAlign: 'middle', widthGrow: 2},
        {
          title: 'Last Selection', field: 'lastSelectionDate', width: 180, hozAlign: 'right', vertAlign: 'middle', headerSortStartingDir: 'desc',
          formatter: cell => {
            const date = DateTime.fromJSDate(cell.getValue());
            return date.isValid ? date.toLocaleString() : '';
          },
          sorter: (a, b) => DateTime.fromJSDate(a).toMillis() - DateTime.fromJSDate(b).toMillis()
        },
        {title: 'Selections', field: 'selectionCount', width: 110, hozAlign: 'right', vertAlign: 'middle', sorter: 'number', headerSortStartingDir: 'desc'},
        {
          title: 'Last Execution', field: 'lastExecutionDate', width: 180, hozAlign: 'right', vertAlign: 'middle', headerSortStartingDir: 'desc',
          formatter: cell => {
            const date = DateTime.fromJSDate(cell.getValue());
            return date.isValid ? date.toLocaleString() : '-';
          },
          sorter: (a, b) => DateTime.fromJSDate(a).toMillis() - DateTime.fromJSDate(b).toMillis()
        },
        {title: 'Executions', field: 'executionCount', width: 110, hozAlign: 'right', vertAlign: 'middle', sorter: 'number', headerSortStartingDir: 'desc'},
      ],
      initialSort: [
        {column: 'executionCount', dir: 'desc'}
      ]
    });

    this.table.on('tableBuilt', async () => {
      await this.service.loadCommands();
      this.subscriptions.push(this.service.selectCommands().subscribe(commands => {
        this.table.setData(commands).then(() => {
          const selectedCommand = this.service.getSelectedCommand();
          if(selectedCommand) {
            const row = this.table.getRows().find(r => (r.getData() as TableEntry)._id === selectedCommand._id);
            row?.select();
            row?.scrollTo('center');
          } else {
            const row = this.table.getRows().find(r => r.getPosition() === 1);
            row?.select();
            row?.scrollTo('center');
          }
        });
      }));
    });

    this.table.on('rowSelectionChanged', (data: TableEntry[]) => {
      if (!data || !data[0]) {
        return;
      }
      this.service.setSelectedCommand(data[0]);
    });
  }

  ngOnInit(): void {
    this.service.init(this.tabId + 'commands');
    this.search = this.service.selectSearch();
    this.selectedCommand = this.service.selectSelectedCommand();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.table.destroy();
    this.service.destroy();
  }

  async onSearchChange(value: string) {
    await this.service.setSearch(value);
  }

  async editOrDelete(event: 'copy' | 'delete' | 'edit', entry: TableEntry) {
    if(event === 'delete') {
      const row = this.table.getRows().find(r => (r.getData() as TableEntry)._id === entry._id);
      const nextRow = row.getNextRow();
      const prevRow = row.getPrevRow();
      let data: TableEntry;
      if(nextRow) {
        data = nextRow.getData() as TableEntry;
      } else if (prevRow){
        data = prevRow.getData() as TableEntry;
      }
      this.service.setSelectedCommand(data);
      await this.service.delete(entry);
    }
    if(event === 'edit') {
      this.service.setCommandToEdit(entry);
      this.service.setShowEditCommand(true, 'edit');
    }
    if(event === 'copy') {
      this.service.setCommandToEdit(entry);
      this.service.setShowEditCommand(true, 'copy');
    }
  }

  add() {
    this.service.setCommandToEdit({
      _id: Hash.create(''),
      command: '',
      executionCount: 0,
      commandHash: Hash.create(''),
      lastExecutionDate: undefined,
      parameters: [],
      selectionCount: 0,
      shellTypes: [],
      tool: '',
      commandSource: CommandSource.EDITOR
    });
    this.service.setShowEditCommand(true, 'copy');
  }
}
