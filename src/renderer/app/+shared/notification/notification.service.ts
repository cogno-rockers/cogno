import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private _notifications: BehaviorSubject<NotificationContext[]> = new BehaviorSubject([]);

  get isVisible(): Observable<boolean> {
    return this._notifications.pipe(map(messages => messages.length > 0));
  }

  get notifications(): Observable<NotificationContext[]> {
    return this._notifications.asObservable();
  }

  isAnyOpen(): boolean {
    return this._notifications.getValue().length > 0;
  }

  create(): Notification {
    return new Notification(this);
  }

  showMessage(message: NotificationContext): Observable<boolean> {
    if (!!message.singleKey && this._notifications.getValue().find(n => n.singleKey === message.singleKey)) {
      return of(false);
    }
    this._notifications.next([...this._notifications.getValue(), message]);
    if (message.duration > 0) {
      setTimeout(() => {
        if (!message.response.closed) {
          message.response.next(false);
          message.response.complete();
        }
      }, message.duration);
    }
    return message.response.pipe(tap(() => {
      const indexOfMessage = this._notifications.getValue().indexOf(message);
      const newMessages = [...this._notifications.getValue()];
      newMessages.splice(indexOfMessage, 1);
      this._notifications.next(newMessages);
    }));
  }
}

export class Notification {
  private readonly context: NotificationContext;

  constructor(private service: NotificationService) {
    this.context = new NotificationContext();
  }

  title(message: string): Notification {
    this.context.title = message;
    return this;
  }

  body(...content: string[]): Notification {
    this.context.body = content;
    return this;
  }

  footer(...footer: string[]): Notification {
    this.context.footer = footer;
    return this;
  }

  duration(milliseconds: number): Notification {
    this.context.duration = milliseconds;
    return this;
  }

  place(place: 'bottom' | 'top') {
    this.context.place = place;
    return this;
  }

  dialog(confirm: string = 'Ok', cancel: string = null, switchDefault: boolean = false): Notification {
    this.context.confirm = confirm;
    this.context.cancel = cancel;
    this.context.switchDefault = switchDefault;
    return this;
  }

  single(key: string): Notification {
    this.context.singleKey = key;
    return this;
  }

  showAsSuccess(): Observable<boolean> {
    this.context.type = NotificationType.Success;
    return this.service.showMessage(this.context);
  }

  showAsUpdate(): Observable<boolean> {
    this.context.type = NotificationType.Update;
    return this.service.showMessage(this.context);
  }

  showAsWarning(): Observable<boolean> {
    this.context.type = NotificationType.Warning;
    return this.service.showMessage(this.context);
  }

  showAsError(): Observable<boolean> {
    this.context.type = NotificationType.Error;
    return this.service.showMessage(this.context);
  }
}

export class NotificationContext {
  response: Subject<boolean> = new Subject();
  type: NotificationType;
  title: string;
  body: string[] = [];
  duration = 0;
  singleKey: string;
  cancel: string;
  confirm: string;
  footer: string[];
  place: 'bottom' | 'top' = 'bottom';
  switchDefault: boolean;
}

export enum NotificationType {
  Success, Warning, Error, Update
}
