import MenuItemConstructorOptions = Electron.MenuItemConstructorOptions;
import MenuItem = Electron.MenuItem;
import {Logger} from './logger';
const { app, Menu } = require('electron');

const isMac = process.platform === 'darwin';

const template: (MenuItemConstructorOptions | MenuItem)[] = [
  // { role: 'appMenu' }
  {
    label: app.name,
    submenu: [
      { role: 'hide' },
      { role: 'hideOthers' },
      { type: 'separator' },
      { role: 'quit' }
    ]
  },
  // { role: 'editMenu' }
  {
    label: 'Edit',
    submenu: [
      { role: 'cut' },
      { role: 'copy' },
      { role: 'paste' }
    ]
  },
  // { role: 'viewMenu' }
  {
    label: 'View',
    submenu: [
      { role: 'reload' },
      { role: 'forceReload' },
      { role: 'toggleDevTools' },
      { type: 'separator' },
      { role: 'togglefullscreen' }
    ]
  },
  // { role: 'windowMenu' }
  {
    label: 'Window',
    submenu: [
      { role: 'minimize' },
      { role: 'zoom' },

        { type: 'separator' },
        { role: 'front' },
        { type: 'separator' },
        { role: 'window' }

    ]
  }
];

export function createAppMenu() {
  if (isMac) {
    Logger.debug('Create Menu');
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
  } else {
    Menu.setApplicationMenu(null);
  }
}
