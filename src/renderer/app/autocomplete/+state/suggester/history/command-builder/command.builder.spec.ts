import {CommandBuilder} from './command.builder';
import {ShellType} from '../../../../../../../shared/models/models';
import {Normalizer} from '../../../../../../../shared/normalizer';
import {ShellLocation} from '../../../../../common/shellLocation';
import {SuggestionCalculation} from '../models';
import {AnsiEscSequence} from '../../../../../common/ansi-escape';

describe('CommandBuilder', () => {
  let commandBuilder: CommandBuilder;

  beforeEach(() => {
    commandBuilder = new CommandBuilder(new Normalizer(new ShellLocation('', ShellType.Bash)));
  });

  it('should work with null or undefined', () => {
    expect(() => commandBuilder.apply(null, ['cd', 'te'])).not.toThrow();
    expect(() => commandBuilder.apply(undefined, ['cd', 'te'])).not.toThrow();
  });

  it('should work with empty tokens', () => {
    const suggestions: SuggestionCalculation = {pathToDirectory: ['test']};
    commandBuilder.apply(suggestions, []);
    expect(suggestions.labelMatches).toBe(0);
  });

  it('should work with null token', () => {
    const suggestions: SuggestionCalculation = {pathToDirectory: ['test']};
    commandBuilder.apply(suggestions, [null]);
    expect(suggestions.labelMatches).toBe(0);
  });

  it('should count matches in navigation', () => {
    const suggestions: SuggestionCalculation = {pathToDirectory: ['test']};
    commandBuilder.apply(suggestions, ['cd', 'te']);
    expect(suggestions.labelMatches).toBe(2);
  });

  it('should count matches in command', () => {
    const suggestions: SuggestionCalculation = {shellCommand: 'npm install'};
    commandBuilder.apply(suggestions, ['npm', 'i']);
    expect(suggestions.labelMatches).toBe(2);
    expect(suggestions.labelMatchesStartOfWord).toBe(2);
  });

  it('should not count matches twice', () => {
    const suggestions: SuggestionCalculation = {pathToDirectory: ['test'], shellCommand: 'npm install install'};
    commandBuilder.apply(suggestions, ['inst']);
    expect(suggestions.labelMatches).toBe(1);
  });

  it('should create correct label and command', () => {
    const suggestions: SuggestionCalculation = {pathToDirectory: ['test'], shellCommand: 'npm install'};
    commandBuilder.apply(suggestions, ['npm']);
    expect(suggestions.label).toEqual('npm install');
    expect(suggestions.pathToDirectory).toEqual(['test']);
    expect(suggestions.shellCommand).toEqual('npm install');
  });

  it('should create correct label and command with variable arguments', () => {
    const suggestion: SuggestionCalculation = {pathToDirectory: ['test'], shellCommand: 'npm {{?}}'};
    commandBuilder.apply(suggestion, ['npm']);
    expect(suggestion.label).toEqual('npm {{?}}');
    expect(suggestion.pathToDirectory).toEqual(['test']);
    expect(suggestion.shellCommand).toEqual('npm ');
  });

  it('should work if no matches are found', () => {
    const suggestions: SuggestionCalculation = {pathToDirectory: ['test'], shellCommand: 'npm {{?}}'};
    commandBuilder.apply(suggestions, ['xyz']);
    expect(suggestions.label).toEqual('npm {{?}}');
    expect(suggestions.pathToDirectory).toEqual(['test']);
    expect(suggestions.shellCommand).toEqual('npm ');
  });

  it('should create correct label and command with many variable arguments', () => {
    const suggestion: SuggestionCalculation = {pathToDirectory: ['test'], shellCommand: 'npm {{?}} {{?}}'};
    commandBuilder.apply(suggestion, ['npm']);
    expect(suggestion.label).toEqual('npm {{?}} {{?}}');
    expect(suggestion.pathToDirectory).toEqual(['test']);
    expect(suggestion.shellCommand).toEqual('npm  {{?}}' + AnsiEscSequence.moveCursorLeft(6));
  });
});
