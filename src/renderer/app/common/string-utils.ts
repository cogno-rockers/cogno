export namespace StringUtils {
  export function toFileName(s: string): string {
    return s.replace(new RegExp('[\\s]', 'g'), '_').replace(new RegExp('[/\\\\]', 'g'), '-');
  }

  export function hash(str: string) {
    /*jshint bitwise:false */
    let i, l,
      hval = 0x811c9dc5;

    for (i = 0, l = str.length; i < l; i++) {
      hval ^= str.charCodeAt(i);
      hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
    }
      // Convert to 8 digit hex string
    return ('0000000' + (hval >>> 0).toString(16)).substr(-8);
  }
}
