import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MenuType} from '../menuType';
import {createStore} from '../../../../common/store/store';
export interface MenuState {
  currentActiveMenu: MenuType;
}

const initialState: MenuState = {
  currentActiveMenu: 'AllClosed'
};

@Injectable({
  providedIn: 'root'
})
export class GlobalMenuService {

  private store = createStore('menu', initialState);

  toggleMenu(menu: MenuType) {
    if (this.store.get(s => s.currentActiveMenu) === menu) {
      this.store.update({currentActiveMenu: 'AllClosed'});
    } else {
      setTimeout(() => {
        this.store.update({currentActiveMenu: menu});
      });
    }
  }

  selectCurrentActiveMenu(): Observable<MenuType> {
    return this.store.select(s => s.currentActiveMenu);
  }

  updateCurrentActiveMenu(currentActiveMenu: MenuType): void {
    this.store.update({currentActiveMenu});
  }

  isAnyOpened(): boolean {
    return this.getCurrentActiveMenu() !== 'AllClosed';
  }

  getCurrentActiveMenu(): MenuType {
    return this.store.get(s => s.currentActiveMenu);
  }
}
