import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs';
import {ColorName, Theme} from '../../../../shared/models/settings';
import {Color} from '../../../../shared/color';
import {ThemesSettingsService} from './+state/themes.settings.service';
import {SettingsTabService} from '../+state/settings-tab.service';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {FilterNamePipe} from '../pipe/filter-name.pipe';
import {ColorPickerModule} from 'ngx-color-picker';
import {IconComponent} from '../../+shared/components/icon/icon.component';
import {TerminalThemePreviewComponent} from '../../+shared/components/terminal-theme-preview/terminal-theme-preview.component';
import {Key} from '../../common/key';
import {ThemesLoaderService} from './+state/themes.loader.service';
import {LoaderComponent} from '../../+shared/components/loader/loader.component';
import {ChangeContext, NgxSliderModule, Options} from '@angular-slider/ngx-slider';

@Component({
    selector: 'app-themes',
    templateUrl: './themes.component.html',
    styleUrls: ['./themes.component.scss'],
    providers: [ThemesSettingsService, ThemesLoaderService],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        FormsModule,
        ColorPickerModule,
        NgxSliderModule,
        IconComponent,
        FilterNamePipe,
        TerminalThemePreviewComponent,
        LoaderComponent
    ]
})
export class ThemesComponent implements OnInit {

  public search: string;

  public availableThemes: Observable<Theme[]>;
  public installedThemes: Observable<Theme[]>;
  public selectedTheme: Observable<Theme>;
  public footerHeaderColor: Observable<string>;
  public isThemeLoadError: Observable<boolean>;
  public isThemeLoading: Observable<boolean>;
  public isThemesLoading: Observable<boolean>;
  public isThemesLoadError: Observable<boolean>;
  public isChanged: Observable<boolean>;
  public isAvailableAfterRestart: Observable<boolean>;
  public isAvailableInNewTab: Observable<boolean>;
  public promptColors: Observable<{foreground?: string; background?: string}[]>;
  public fonts: Observable<string[]>;
  public appFonts: Observable<string[]>;
  public fontWeights = ['normal', 'bold', '100', '200', '300', '400', '500', '600', '700', '800', '900'];
  public promptVersions = [
    {version: 1, label: 'Lean', colorsCount: 1},
    {version: 2, label: 'Triangle', colorsCount: 2},
    {version: 3, label: 'Triangle Two', colorsCount: 2},
    {version: 4, label: 'Semicircle', colorsCount: 2},
    {version: 5, label: 'Semicircle Two', colorsCount: 2},
    {version: 6, label: 'Triangle Git Branch', colorsCount: 4},
    {version: 7, label: 'Triangle Two Git Branch', colorsCount: 4},
    {version: 8, label: 'Semicircle Git Branch', colorsCount: 4},
    {version: 9, label: 'Semicircle Two Git Branch', colorsCount: 4},
    {version: 10, label: 'Chop Triangle', colorsCount: 2},
    {version: 11, label: 'Chop Triangle Two', colorsCount: 2},
    {version: 12, label: 'Chop Semicircle', colorsCount: 2},
    {version: 13, label: 'Chop Semicircle Two', colorsCount: 2},
    {version: 14, label: 'Chop Triangle Git Branch', colorsCount: 4},
    {version: 15, label: 'Chop Triangle Two Git Branch', colorsCount: 4},
    {version: 16, label: 'Chop Semicircle Git Branch', colorsCount: 4},
    {version: 17, label: 'Chop Semicircle Two Git Branch', colorsCount: 4}
  ];
  public colors = [
    {color: ColorName.white, label: 'White'},
    {color: ColorName.brightWhite, label: 'Bright White'},
    {color: ColorName.black, label: 'Black'},
    {color: ColorName.brightBlack, label: 'Bright Black'},
    {color: ColorName.red, label: 'Red'},
    {color: ColorName.brightRed, label: 'Bright Red'},
    {color: ColorName.yellow, label: 'Yellow'},
    {color: ColorName.brightYellow, label: 'Bright Yellow'},
    {color: ColorName.blue, label: 'Blue'},
    {color: ColorName.brightBlue, label: 'Bright Blue'},
    {color: ColorName.green, label: 'Green'},
    {color: ColorName.brightGreen, label: 'Bright Green'},
    {color: ColorName.magenta, label: 'Magenta'},
    {color: ColorName.brightMagenta, label: 'Bright Magenta'},
    {color: ColorName.cyan, label: 'Cyan'},
    {color: ColorName.brightCyan, label: 'Bright Cyan'}
  ];

  public ColorNames = ColorName;

  cursorSliderOptions: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 1 },
      { value: 2 },
      { value: 3 },
      { value: 4 },
      { value: 5 },
      { value: 6 },
      { value: 7 }
    ]
  };

  opacitySliderOptions: Options = {
    showTicksValues: false,
    floor: 0,
    ceil: 100
  };

  blurSliderOptions: Options = {
    showTicksValues: false,
    floor: 0,
    ceil: 20
  };

  constructor(private settingsService: SettingsTabService, private service: ThemesSettingsService) { }

  ngOnInit(): void {
    this.service.init(this.settingsService.getTabId() + 'themessettings');
    this.availableThemes = this.service.selectAvailableThemes();
    this.installedThemes = this.service.selectInstalledThemes();
    this.selectedTheme = this.service.selectSelectedTheme();
    this.fonts = this.service.selectFonts();
    this.appFonts = this.service.selectAppFonts();
    this.footerHeaderColor = this.service.selectFooterHeaderColor();
    this.isThemeLoadError = this.service.selectThemeDetailsIsLoadError();
    this.isThemeLoading = this.service.selectThemeDetailsIsLoading();
    this.isThemesLoading = this.service.selectThemesIsLoading();
    this.isThemesLoadError = this.service.selectIsLoadError();
    this.isChanged = this.service.selectThemeIsChanged();
    this.isAvailableAfterRestart = this.service.selectIsAvailableAfterRestart();
    this.isAvailableInNewTab = this.service.selectIsAvailableInNewTab();
    this.promptColors = this.service.selectPromptColors();
  }

  selectTheme(theme: Theme): void {
    this.service.updateSelectedTheme(theme);
  }

  isLight(color: string): boolean {
    return Color.isLight(color);
  }

  installTheme(): void {
    this.service.installTheme();
  }

  setAsDefaultTheme(): void {
    this.service.setAsDefaultTheme();
  }

  uninstallTheme(): void {
    this.service.uninstallTheme();
  }

  openExternal(link: string): void {
    this.service.openExternal(link);
  }

  setColor(colorAsHex: string, colorName: ColorName): void {
    this.service.setColor(colorAsHex, colorName);
  }

  saveTheme() {
    this.service.saveTheme();
  }

  saveThemeAndRelaunch() {
    this.service.saveThemeAndRelaunch();
  }

  setPromptColor(index: number, event: Event): void {
    this.service.setPromptColor(index, (event.target as any).value);
  }

  setPromptBackgroundColor(index: number, event: Event): void {
    this.service.setPromptBackgroundColor(index, (event.target as any).value);
  }

  setCursorWidth(context: ChangeContext) {
    this.service.setCursorWidth(context.value);
  }

  setFont(event: Event) {
    this.service.setFont((event.target as any).value);
  }

  setFontSize(event: Event) {
    this.service.setFontSize((event.target as any).value);
  }

  setAppFontSize(event: Event) {
    this.service.setAppFontSize((event.target as any).value);
  }

  setPrompt(event: Event) {
    this.service.setPrompt((event.target as any).value);
  }

  openImagePathSelection() {
    this.service.openImageSelection();
  }

  changeImagePath($event: FocusEvent | KeyboardEvent) {
    if (($event as KeyboardEvent)?.key === Key.Enter || $event instanceof FocusEvent) {
      const value = ($event.target as HTMLInputElement).value;
      this.service.setImage(value);
    }
  }

  setImageOpacity(context: ChangeContext) {
    this.service.setImageOpacity(context.value);
  }

  setImageBlur(context: ChangeContext) {
    this.service.setImageBlur(context.value);
  }

  clearImagePath() {
    this.service.setImage('');
  }

  toggleBlinkingCursor() {
    this.service.toggleBlinkingCursor();
  }

  toggleLigatures() {
    this.service.toggleLigatures();
  }

  toggleWebgl() {
    this.service.toggleWebgl();
  }

  toggleRemoveFullScreenAppPadding() {
    this.service.toggleRemoveFullScreenAppPadding();
  }

  setFontWeight(event: Event) {
    this.service.setFontWeight((event.target as any).value);
  }

  setFontWeightBold(event: Event) {
    this.service.setFontWeightBold((event.target as any).value);
  }

  setPromptVersion(event: Event) {
    this.service.setPromptVersion((event.target as any).value);
  }

  resetTheme() {
    this.service.resetTheme();
  }

  setPadding(event: any) {
    this.service.setPadding((event.target as any).value);
  }

  setAsDarkModeTheme() {
    this.service.setAsDarkModeTheme();
  }

  setCursorStyle(event: Event) {
    this.service.setCursorStyle((event.target as any).value);
  }

  setAppFont($event: any) {
    this.service.setAppFont((event.target as any).value);
  }
}
