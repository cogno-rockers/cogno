import {Injectable} from '@angular/core';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {catchError, concatAll, firstValueFrom, forkJoin, from, Observable, of, zip} from 'rxjs';
import {PluginConfig} from '../../../+shared/models/plugin-config';
import {Uuid} from '../../../common/uuid';
import {map} from 'rxjs/operators';
import {Logger} from '../../../logger/logger';

@Injectable()
export class PluginsLoader {

  private readonly keyword = 'cogno-plugin';
  private readonly registry = 'registry.npmjs.com';

  constructor(private electron: ElectronService) {
  }

  loadPlugins(): Observable<PluginConfig[]> {
    return this.electron.httpGet(this.registry, 443, `-/v1/search?text=keywords%3A${this.keyword}%20&size=250`).pipe(
      catchError(err => {
        Logger.error('Could not search for plugins', err);
        return of(Buffer.from('{}'));
      }),
      map(searchResult => {
        const json = new TextDecoder().decode(searchResult);
        const dataFromNpm = JSON.parse(json);
        const names = dataFromNpm.objects.map(o => o?.package?.name || '') as (string)[];
        return forkJoin(names.filter(n => !!n).map(name => this.electron.httpGet(this.registry, 443, `${name}/latest`)));
      }),
      concatAll(),
      map(results => results.map(buffer => {
          const json = new TextDecoder().decode(buffer);
          const dataFromNpm = JSON.parse(json);
          const data: PluginConfig = {
            name: dataFromNpm?.name || '',
            type: 'Autocomplete',
            availableVersion: dataFromNpm?.version,
            description: dataFromNpm?.description,
            username: dataFromNpm?.author?.name || dataFromNpm?._npmUser?.name || '',
            email: dataFromNpm?.author?.email || dataFromNpm?._npmUser?.email || '',
            repository: dataFromNpm?.repository?.url || '',
            homepage: dataFromNpm?.homepage || '',
            tarballUrl: dataFromNpm?.dist?.tarball,
            regexp: dataFromNpm?.cogno?.regexp || undefined,
            placeholder: dataFromNpm?.cogno?.placeholder || undefined,
            icon: dataFromNpm?.cogno?.icon || '',
            label: dataFromNpm?.cogno?.label || '',
            isInstalled: false,
            isUpdateAvailable: false,
            workerPath: null,
            installDir: null,
            installedVersion: null,
          };
          return data;
        }))
    );
  }

  async downloadPlugin(tarballUrl: string) {
    const pathToTar = this.electron.getPath().join(this.electron.getTempDirectoryPath(), `${Uuid.create()}.tar.gz`);
    await firstValueFrom(this.electron.downloadFile(tarballUrl, pathToTar));
    return pathToTar;
  }
}
