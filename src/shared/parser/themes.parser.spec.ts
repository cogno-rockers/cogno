import {Theme} from '../models/settings';
import {ThemesParser} from './themes.parser';
import {TestHelper} from '../../test/helper';

describe('theme-validator', () => {

  let themes: Theme[];

  beforeEach(() => {
    themes = createThemes();
  });

  it('should return valid false if string is undefined', () => {
    expect(ThemesParser.parseThemes(undefined).isValid).toBeFalsy();
  });

  it('should return valid false if string is null', () => {
    expect(ThemesParser.parseThemes(null).isValid).toBeFalsy();
  });

  it('should return valid false if string is empty', () => {
    expect(ThemesParser.parseThemes('').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty object', () => {
    expect(ThemesParser.parseThemes('{}').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty array', () => {
    expect(ThemesParser.parseThemes('[]').isValid).toBeFalsy();
  });

  it('should return valid false if theme contains invalid colors', () => {
    themes[0].colors.highlight = 'abx';
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeFalsy();
  });

  it('should return valid false if theme contains invalid imageOpacity', () => {
    themes[0].imageOpacity = 101;
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeFalsy();
  });

  it('should return valid false if theme contains invalid prompt', () => {
    themes[0].prompt = undefined;
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeFalsy();
  });

  it('should enrich theme values', () => {
    themes[0].fontsize = undefined;
    themes[0].appFontsize = undefined;
    themes[0].fontFamily = undefined;
    themes[0].cursorWidth = undefined;
    themes[0].cursorBlink = undefined;
    themes[0].prompt = undefined;
    expect(ThemesParser.validateThemes(themes, true).themes[0].cursorWidth).toBe(3);
    expect(ThemesParser.validateThemes(themes, true).themes[0].fontFamily).toBe('monospace');
    expect(ThemesParser.validateThemes(themes, true).themes[0].fontsize).toBe(14);
    expect(ThemesParser.validateThemes(themes, true).themes[0].appFontsize).toBe(14);
    expect(ThemesParser.validateThemes(themes, true).themes[0].prompt).toBe('⯈');
    expect(ThemesParser.validateThemes(themes, true).themes[0].cursorBlink).toBe(true);
  });

  it('should return valid true if string is array of themes', () => {
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeTruthy();
  });

  it('should return valid true if padding is ok', () => {
    themes[0].padding = undefined;
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeTruthy();
    themes[0].padding = '';
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeTruthy();
    themes[0].padding = '3';
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeTruthy();
    themes[0].padding = '3 0.5';
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeTruthy();
    themes[0].padding = '3 1 2';
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeTruthy();
    themes[0].padding = '3 3 4 3';
    expect(ThemesParser.parseThemes(JSON.stringify(themes)).isValid).toBeTruthy();
  });

  it('should return parse padding correct', () => {
    themes[0].padding = undefined;
    expect((ThemesParser.parseThemes(JSON.stringify(themes))).themes[0].paddingAsArray).toEqual([1, 1, 1, 1]);
    themes[0].padding = '2';
    expect((ThemesParser.parseThemes(JSON.stringify(themes))).themes[0].paddingAsArray).toEqual([1, 2, 1, 2]);
    themes[0].padding = '2 3';
    expect((ThemesParser.parseThemes(JSON.stringify(themes))).themes[0].paddingAsArray).toEqual([1, 2, 1, 3]);
    themes[0].padding = '2 3 4';
    expect((ThemesParser.parseThemes(JSON.stringify(themes))).themes[0].paddingAsArray).toEqual([1, 2, 1, 3]);
  });
});

function createThemes(): Theme[] {
  return [TestHelper.createTheme('test')];
}
