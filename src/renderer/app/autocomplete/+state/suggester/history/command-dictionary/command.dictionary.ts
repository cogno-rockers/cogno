import * as SearchApi from 'js-search';
import {MatchShellType, NormalizedCommand, NormalizedSuggestResult, SuggestionCalculation} from '../models';
import {Clock, DefaultClock} from '../../../../../common/clock';
import {ShellType} from '../../../../../../../shared/models/models';
import {ShellLocation} from '../../../../../common/shellLocation';
import {Suggestion, SuggestionType, SuggestRequest} from '../../suggester';
import {Command, CommandSource} from '../../../../../+shared/models/command';
import {Hash} from '../../../../../+shared/models/hash/hash';

export class CommandDictionary {
  private searchApi = new SearchApi.Search('commandHash');
  private c: Clock = new DefaultClock();

  constructor(private location: ShellLocation) {
    this.searchApi.searchIndex = new SearchApi.UnorderedSearchIndex();
    this.searchApi.indexStrategy = new SearchApi.AllSubstringsIndexStrategy();
    this.searchApi.addIndex('command');
  }

  public set clock(clock: Clock) {
    this.c = clock;
  }

  public saveSelection(suggestion: Suggestion): Command {
    let doc = this.find(suggestion.label);
    if (doc === null) {
      doc = {
        command: suggestion.label,
        commandHash: Hash.create(suggestion.label),
        executionCount: 0,
        selectionCount: 0,
        shellTypes: [suggestion.location.shellType],
        commandSource: suggestion.commandSource
      };
      this.searchApi.addDocument(doc);
    }
    doc.selectionCount++;
    doc.lastSelectionDate = this.c.now();
    doc.shellTypes = doc.shellTypes.concat([suggestion.location.shellType].filter((item) => doc.shellTypes.indexOf(item) < 0));
    return doc;
  }

  public saveExecution(...commands: NormalizedCommand[]): Command[] {
    const result = [];
    for (const command of commands) {
      if (!command || !command.command) {
        continue;
      }
      let doc = this.find(command.command);
      if (doc === null) {
        doc = {
          command: command.command,
          commandHash: command.commandHash,
          executionCount: 0,
          selectionCount: 0,
          shellTypes: command.shellTypes,
          commandSource: CommandSource.TERMINAL,
        };
        this.searchApi.addDocument(doc);
      }
      doc.executionCount++;
      doc.lastExecutionDate = this.c.now();
      doc.shellTypes = doc.shellTypes.concat(command.shellTypes.filter((item) => doc.shellTypes.indexOf(item) < 0));
      result.push(doc);
    }
    return result;
  }


  private find(command: string): Command {
    let docs = this.searchApi.search(command);
    docs = docs.filter(d => d.command === command);
    return docs.length > 0 ? docs[0] : null;
  }

  public apply(suggestRequest: SuggestRequest, searchResult: NormalizedSuggestResult) {
    const result = new Map<string, SuggestionCalculation>();
    let minExecutionDate = new Date(8640000000000000);
    let maxExecutionDate = new Date(0);
    let minSelectionDate = new Date(8640000000000000);
    let maxSelectionDate = new Date(0);

    const docs = !suggestRequest.input ? this.searchApi['_documents'].filter(d => !d.isDeleted) : this.searchApi.search(suggestRequest.input).filter(d => !d.isDeleted);
    for (const doc of docs) {
      result.set(doc.commandHash, {
        commandHash: doc.commandHash,
        shellCommand: doc.command,
        globalExecutionCount: doc.executionCount,
        globalSelectionCount: doc.selectionCount,
        globalExecutionDate: doc.lastExecutionDate,
        globalSelectionDate: doc.lastSelectionDate,
        commandSource: doc.commandSource,
        matchShellType: doc.shellTypes.includes(this.location.shellType) ? MatchShellType.Yes : MatchShellType.Partly
      });
      if (minExecutionDate > doc.lastExecutionDate) {
        minExecutionDate = doc.lastExecutionDate;
      }
      if (maxExecutionDate < doc.lastExecutionDate) {
        maxExecutionDate = doc.lastExecutionDate;
      }
      if (minSelectionDate > doc.lastSelectionDate) {
        minSelectionDate = doc.lastSelectionDate;
      }
      if (maxSelectionDate < doc.lastSelectionDate) {
        maxSelectionDate = doc.lastSelectionDate;
      }
    }
    searchResult.scoringValues.minGlobalExecutionDate = minExecutionDate;
    searchResult.scoringValues.minGlobalSelectionDate = minSelectionDate;
    searchResult.scoringValues.maxGlobalExecutionDate = maxExecutionDate;
    searchResult.scoringValues.maxGlobalSelectionDate = maxSelectionDate;
    searchResult.suggestions.push(...Array.from(result.values()).map(t => {
      t.suggestionType = SuggestionType.GlobalCommand;
      return t;
    }));
  }

  public restore(command: Command) {
    if (!command || !command.command || !command.commandHash) {return;}

      const doc = {
        commandHash: command.commandHash,
        command: command.command,
        executionCount: command.executionCount ?? 0,
        selectionCount: command.selectionCount ?? 0,
        lastExecutionDate: command.lastExecutionDate,
        lastSelectionDate: command.lastSelectionDate,
        shellTypes: command.shellTypes,
        commandSource: command.commandSource
      };
      this.searchApi.addDocument(doc);

  }
}
