export enum Nav {
  General, Themes, Shortcuts, Autocomplete, Plugins, Commands
}
