import {Injectable} from '@angular/core';
import {SettingsService} from '../../../../+shared/services/settings/settings.service';
import * as Logger from 'electron-log/renderer';
import {PluginFileManager} from '../../../../+shared/services/plugins/plugin.file.manager';
import {PluginSuggester} from './plugin.suggester';

@Injectable({
  providedIn: 'root'
})
export class PluginSuggesterManager {

  private plugins: Map<string, PluginSuggester> = new Map();

  constructor(private pluginManager: PluginFileManager, private settingsService: SettingsService) {
    this.settingsService.selectSettingsLoaded().subscribe(() => this.loadPlugins());
  }

  public loadPlugins() {
    Logger.info('load plugins');
    const pluginDatas = this.pluginManager.getInstalledPlugins();
    for (const pluginData of pluginDatas) {
      if (this.plugins.has(pluginData.name)) {
        continue;
      }
      Logger.info('create worker', pluginData.workerPath);
      const plugin = new Worker('file://' + pluginData.workerPath);
      Logger.info('loaded plugin', pluginData.name, pluginData.regexp, pluginData.placeholder);
      const suggester = new PluginSuggester(plugin, pluginData);
      this.plugins.set(pluginData.name, suggester);
      if (pluginData.placeholder) {
        this.plugins.set(pluginData.placeholder, suggester);
      }
    }
  }

  public get(nameOrPlaceholder: string): PluginSuggester | undefined {
    return this.plugins.get(nameOrPlaceholder);
  }

  public getByInput(input: string): PluginSuggester[] | undefined {
    const suggesters = [];
    for (const suggester of this.plugins.values()) {
      if(!suggester.regexp) continue;
      if (input.match(suggester.regexp)) {
        suggesters.push(suggester);
      }
    }
    return suggesters;
  }

  destroy() {
    for (const plugin of this.plugins.values()) {
      plugin.destroy();
    }
    this.plugins.clear();
  }

  has(nameOrPlaceholder: string): boolean {
    return this.plugins.has(nameOrPlaceholder);
  }

  getPlugins(): {label: string; placeholder: string}[] {
    return Array.from(new Set(this.plugins.values())).filter(plugin => !!plugin.placeholder).map(p => ({label: p.label, placeholder: p.placeholder}));
  }
}
