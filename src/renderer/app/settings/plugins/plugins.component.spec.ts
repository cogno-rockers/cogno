import {
  clear,
  getGridService, getPluginFileManager, getPluginSuggesterManager, getPluginsLoader, getPluginsSettingsService,
  getSettingsTabService
} from '../../../../test/factory';
import {SettingsTabService} from '../+state/settings-tab.service';
import {TestHelper} from '../../../../test/helper';
import {of} from 'rxjs';
import {GridService} from '../../global/grid/+state/grid.service';
import {BinaryTree} from '../../common/tree/binary-tree';
import {TabType} from '../../../../shared/models/models';
import {PluginsComponent} from './plugins.component';
import {PluginsSettingsService} from './+state/plugins.settings.service';
import {PluginsLoader} from './+state/plugins.loader';
import {PluginFileManager} from '../../+shared/services/plugins/plugin.file.manager';
import {PluginSuggesterManager} from '../../autocomplete/+state/suggester/plugins/plugin-suggester.manager';


describe('PluginsComponent', () => {
  let component: PluginsComponent;
  let pluginsSettingsService: PluginsSettingsService;
  let pluginsLoader: PluginsLoader;
  let pluginsHandler: PluginSuggesterManager;
  let pluginFileManager: PluginFileManager;
  let settingsTabService: SettingsTabService;
  let gridService: GridService;

  beforeEach(() => {
    clear();
    gridService = getGridService();
    gridService.addNewTab(BinaryTree.ROOT_KEY, TabType.Settings);
    pluginsSettingsService = getPluginsSettingsService();
    pluginsLoader = getPluginsLoader();
    pluginsHandler = getPluginSuggesterManager();
    pluginFileManager = getPluginFileManager();
    settingsTabService = getSettingsTabService();
    settingsTabService.init(gridService.getActiveTabId());
    component = new PluginsComponent(pluginsSettingsService, settingsTabService);
  });

  it('should load plugins on init', () => {
    const pluginA = {...TestHelper.createPlugin(), name: 'PluginA'};
    const pluginB = {...TestHelper.createPlugin(), name: 'PluginB'};
    jest.spyOn(pluginsLoader, 'loadPlugins').mockReturnValue(of([pluginA, pluginB]));
    component.ngOnInit();
    expect(pluginsLoader.loadPlugins).toBeCalledTimes(1);
  });

  it('should select plugin on startup and on select', (done) => {
    const pluginA = {...TestHelper.createPlugin(), name: 'PluginA'};
    const pluginB = {...TestHelper.createPlugin(), name: 'PluginB'};
    jest.spyOn(pluginsLoader, 'loadPlugins').mockReturnValue(of([pluginA, pluginB]));
    let counter = 0;
    component.selectedPlugin.subscribe((t) => {
      counter++;
      if(counter === 1) {
        expect(t).toEqual(null);
      }
      if(counter === 2) {
        expect(t).toEqual(pluginA);
      }
      if(counter === 3) {
        expect(t).toEqual(pluginB);
        done();
      }
    });
    component.ngOnInit();
    component.selectPlugin(pluginB);
  });

  it('should remove plugin from available', (done) => {
    const pluginA = {...TestHelper.createPlugin(), name: 'PluginA'};
    const pluginB = {...TestHelper.createPlugin(), name: 'PluginB'};
    const plugins = [pluginA, pluginB];
    jest.spyOn(pluginsHandler, 'loadPlugins').mockImplementation(() => {});
    jest.spyOn(pluginsLoader, 'loadPlugins').mockReturnValue(of(plugins));
    jest.spyOn(pluginsLoader, 'downloadPlugin').mockReturnValue(Promise.resolve(''));
    jest.spyOn(pluginFileManager, 'installPlugin').mockReturnValue(Promise.resolve(''));
    jest.spyOn(pluginFileManager, 'getInstalledPlugins').mockReturnValueOnce([]).mockReturnValue([{...pluginA, installedVersion: '1.0.0'}]);
    let counter = 0;
    component.availablePlugins.subscribe((t) => {
      counter++;
      if(counter <= 2) {
        expect(t).toEqual([]);
      }
      if(counter === 3) {
        expect(t).toEqual(plugins);
      }
      if(counter === 4) {
        expect(t).toEqual([pluginB]);
        done();
      }
    });
    component.ngOnInit();
    component.selectPlugin(pluginA);
    component.installPlugin();
  });

  it('should add plugin to installed', (done) => {
    const pluginA = {...TestHelper.createPlugin(), name: 'PluginA'};
    const pluginB = {...TestHelper.createPlugin(), name: 'PluginB'};
    const plugins = [pluginA, pluginB];
    jest.spyOn(pluginsHandler, 'loadPlugins').mockImplementation(() => {});
    jest.spyOn(pluginsLoader, 'loadPlugins').mockReturnValue(of(plugins));
    jest.spyOn(pluginsLoader, 'downloadPlugin').mockReturnValue(Promise.resolve(''));
    jest.spyOn(pluginFileManager, 'installPlugin').mockReturnValue(Promise.resolve(''));
    jest.spyOn(pluginFileManager, 'getInstalledPlugins').mockReturnValueOnce([]).mockReturnValue([{...pluginA, installedVersion: '1.0.0'}]);
    let counter = 0;
    component.installedPlugins.subscribe((t) => {
      counter++;
      if(counter <= 2) {
        expect(t).toEqual([]);
      }
      if(counter === 3) {
        pluginA.installedVersion = '1.0.0';
        expect(t).toEqual([pluginA]);
        done();
      }
    });
    component.ngOnInit();
    component.selectPlugin(pluginA);
    component.installPlugin();
  });

  it('should uninstall plugin', (done) => {
    const pluginA = {...TestHelper.createPlugin(), name: 'PluginA'};
    const pluginB = {...TestHelper.createPlugin(), name: 'PluginB'};
    const plugins = [pluginA, pluginB];

    jest.spyOn(pluginsHandler, 'loadPlugins').mockImplementation(() => {});
    jest.spyOn(pluginsLoader, 'loadPlugins').mockReturnValue(of(plugins));
    jest.spyOn(pluginsLoader, 'downloadPlugin').mockReturnValue(Promise.resolve(''));
    jest.spyOn(pluginFileManager, 'installPlugin').mockReturnValue(Promise.resolve(''));
    jest.spyOn(pluginFileManager, 'uninstallPlugin').mockReturnValue(Promise.resolve());
    jest.spyOn(pluginFileManager, 'getInstalledPlugins').mockReturnValueOnce([]).mockReturnValue([{...pluginA, installedVersion: '1.0.0'}]);
    let counter = 0;
    component.installedPlugins.subscribe((t) => {
      counter++;
      if(counter === 3) {
        pluginA.installedVersion = '1.0.0';
        expect(t).toEqual([pluginA]);
        component.uninstallPlugin();
      }
      if(counter === 4) {
        expect(t).toEqual([]);
        done();
      }
    });
    component.ngOnInit();
    component.selectPlugin(pluginA);
    component.installPlugin();
  });
});
