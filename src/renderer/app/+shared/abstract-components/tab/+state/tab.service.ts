import {filter} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs';
import {createStore, Store} from '../../../../common/store/store';
import {GridService} from '../../../../global/grid/+state/grid.service';

export abstract class TabService<State> {
  protected store: Store<State>;
  protected _isStoreReady = new BehaviorSubject<boolean>(false);

  protected subscription: Subscription[] = [];

  protected constructor(protected gridService: GridService, private initialState: State) {
  }

  getTabId(): string {
    return this.store.id;
  }

  getState(): State {
    return {...this.store.get(s => s)};
  }

  selectIsStoreReady(): Observable<boolean> {
    return this._isStoreReady.asObservable();
  }

  public init(tabId: string) {
    this.store = createStore(tabId, this.initialState);
    this._isStoreReady.next(true);
    this.untilDestroy(
      this.gridService.onCloseTab().pipe(filter(tabId => this.store.id === tabId)).subscribe(() => {
        const promise = instanceOfOnClose(this) ? this.tabCloseFinished() : null;
        if (promise) {
          promise.then(() => this.destroy());
        } else {
          this.destroy();
        }
      })
    );
    const isReady = instanceOfOnReady(this) ? this.onReady() : void(0);
    if (isReady) {
      isReady
        .then(() => {
          this.gridService.updateTabIsReady(this.store.id);
        })
        .catch(() => {
          this.gridService.updateTabHasError(this.store.id);
        });
    } else {
      setTimeout(() => this.gridService.updateTabIsReady(this.store.id));
    }
  }

  destroy() {
    this.unsubscribe();
    setTimeout(() => this.store.destroy());
    this.gridService.removeTab(this.store.id);
  }

  public focus() {
    this.gridService.focusTab(this.store.id);
  }

  protected unsubscribe() {
    this.subscription.forEach(s => s.unsubscribe());
  }

  protected untilDestroy(...subscriptions: Subscription[]) {
    this.subscription.push(...subscriptions);
  }
}

export interface OnTabClose {
  tabCloseFinished(): Promise<any> | void;
}

function instanceOfOnClose(object: any): object is OnTabClose {
  return 'tabCloseFinished' in object;
}

function instanceOfOnReady(object: any): object is OnReady {
  return 'onReady' in object;
}

export interface OnReady {
  onReady(): Promise<unknown> | void;
}
