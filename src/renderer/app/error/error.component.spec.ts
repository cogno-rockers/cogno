import {
  clear,
  getElectronService,
  getSettingsService,
} from '../../../test/factory';
import {ErrorComponent} from './error.component';
import {SettingsService} from '../+shared/services/settings/settings.service';
import {ElectronService} from '../+shared/services/electron/electron.service';

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let settingsService: SettingsService;
  let electronService: ElectronService;

  beforeEach(() => {
    clear();
    electronService = getElectronService();
    jest.spyOn(electronService, 'getSettingsPath').mockReturnValue('/settings.txt');

    settingsService = getSettingsService();
    component = new ErrorComponent(settingsService, electronService);
  });

  it('should init errors - no errors', (done) => {
    component.settingsErrors.subscribe(errors => {
      expect(errors).toEqual([]);
      done();
    });
  });

  it('should init errors - 10 errors', (done) => {
    const errors = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
    settingsService.update({errors: errors});
    component.settingsErrors.subscribe(errors => {
      expect(errors).toEqual(errors);
      done();
    });
  });

  it('should init errors - 11 errors', (done) => {
    const errors = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'];
    settingsService.update({errors: errors});
    component.settingsErrors.subscribe(errors => {
      expect(errors).toEqual(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',  '...']);
      done();
    });
  });

  it('should open settings', () => {
    const openPathSpy = jest.spyOn(electronService, 'openPath').mockImplementation(() => {});
    component.openSettings();
    expect(openPathSpy.mock.calls.length).toEqual(1);
    expect(openPathSpy.mock.calls[0][0]).toEqual('/settings.txt');
  });

});
