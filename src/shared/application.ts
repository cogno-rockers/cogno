import { app } from 'electron';
import * as path from 'path';
import * as fs from 'fs';

export namespace Application {

  let appConfigDirName = '';

  export function setAppConfigDirName(isDev: boolean) {
    if (!isDev && fs.existsSync(path.join(app.getPath('home'), '.cogno'))) {
      appConfigDirName = '.cogno';
      return;
    }
    if (!isDev && fs.existsSync(path.join(app.getPath('home'), '.wisp'))) {
      appConfigDirName = '.wisp';
      return;
    }
    if(isDev && fs.existsSync(path.join(app.getPath('home'), '.cogno-dev'))){
      appConfigDirName = '.cogno-dev';
      return;
    }
    appConfigDirName = isDev ? '.cogno-dev' : '.cogno';
    return;
  }

  export function getAppPath(): string {
    if (process.env.PORTABLE_EXECUTABLE_DIR && process.platform.indexOf('win') !== -1) {
      return process.env.PORTABLE_EXECUTABLE_DIR;
    }
    const appPath = app.getAppPath();
    if (appPath.endsWith('asar')) {
      return path.join(appPath, '../../');
    }
    return appPath;
  }

  export function getTempPath(): string {
    return app.getPath('temp');
  }

  export function getVersion(): string {
    return app.getVersion();
  }

  export function getUserHomeDirPath(): string {
    return app.getPath('home');
  }

  export function getAppConfigDirPath(): string {
    return path.join(app.getPath('home'), appConfigDirName);
  }
}
