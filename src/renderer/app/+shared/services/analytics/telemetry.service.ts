import {Injectable} from '@angular/core';
import * as mixpanel from 'mixpanel';
import {Uuid} from '../../../common/uuid';
import {ElectronService} from '../electron/electron.service';
import {AppConfig} from '../../../../environments/environment';
import {Logger} from '../../../logger/logger';

@Injectable({
  providedIn: 'root'
})
export class TelemetryService {

  private readonly mixpanelId = '9036c80d912179e6fc0be50f383e94d4';
  private isRunning = false;
  private interval: any;


  constructor(private electron: ElectronService) {
  }

  public enable(): void {
    if (this.isRunning) {
      return;
    }
    this.isRunning = true;
    const m = mixpanel.init(this.mixpanelId, {debug: !AppConfig.production});
    if (!window.localStorage.userId) {
      window.localStorage.userId = Uuid.create();
      this.trackNewInstallation(m);
    }
    this.trackLaunch(m);
    this.trackLongRunning(m);
  }

  private trackNewInstallation(m: mixpanel.Mixpanel): void {
    Logger.info(`Track new installation. User ${window.localStorage.userId}, Dev mode: ${!AppConfig.production}`);
    if (!AppConfig.production) {
      return;
    }
    m.track(`New Installation`, this.getTelemetry());
  }

  private trackLaunch(m: mixpanel.Mixpanel) {
    Logger.info(`Track launch. User: ${window.localStorage.userId}. Dev mode: ${!AppConfig.production}`);
    if (!AppConfig.production) {
      return;
    }
    m.track(`Launch`, this.getTelemetry());
  }

  private trackLongRunning(m: mixpanel.Mixpanel) {
    if (!AppConfig.production) {
      return;
    }
    if (this.interval) {
      window.clearInterval(this.interval);
    }
    this.interval = window.setInterval(() => {
      Logger.info(`Track long running. User ${window.localStorage.userId}`);
      m.track(`Long Running`, this.getTelemetry());
    }, 24 * 60 * 60 * 1000);
  }

  public disable(): void {
    Logger.info(`Disable telemetry. User ${window.localStorage.userId}`);
    this.isRunning = false;
    if (this.interval) {
      window.clearInterval(this.interval);
    } else {
      Logger.info(`[Test] Disable telemetry. User ${window.localStorage.userId}`);
    }
  }

  private getTelemetry(): { userId: string, os: string, osRelease: string, version: string, distinct_id: string } {
    return {
      userId: window.localStorage.userId,
      os: process.platform,
      osRelease: this.electron.getOSRelease(),
      version: this.electron.getVersion(),
      distinct_id: window.localStorage.userId
    };
  }
}
