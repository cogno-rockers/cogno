import {Command, Directory} from '../../models/command';

export type Id = {
  _id: string;
};

export type CommandEntity = Id & Command;

export type DirectoryEntity = Id & Directory;

export type CommandDirectoryEntity = Id & {
  directoryId: string;
  commandId: string;
  executionCount: number;
  lastExecutionDate?: Date;
  selectionCount: number;
  lastSelectionDate?: Date;
};
