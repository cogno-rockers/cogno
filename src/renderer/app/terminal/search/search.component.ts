import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Observable, pairwise, tap} from 'rxjs';
import {MenuComponent} from '../../+shared/abstract-components/menu/menu.component';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';
import {SearchMenuService} from './+state/search-menu.service';
import {map} from 'rxjs/operators';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../../+shared/components/icon/icon.component';
import {SearchOptions} from './+state/models';
import {KeytipService} from '../../+shared/services/keyboard/keytip.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    imports: [
        CommonModule,
        IconComponent
    ]
})
export class SearchComponent extends MenuComponent implements OnInit{

  public searchOptions: Observable<SearchOptions>;
  public isVisible: Observable<boolean>;
  public searchResult: Observable<{hasMatch: boolean; count: number; position: number}>;

  @ViewChild('searchInputField') searchInputField: ElementRef;

  constructor(elementRef: ElementRef, protected menuService: SearchMenuService, protected keyboardService: KeyboardService, public keytipService: KeytipService) {
    super(elementRef, menuService, keyboardService, keytipService);
    super.openOnShortcut('find');
  }

  ngOnInit(): void {
    this.menuService.init();
    this.isVisible = this.menuService.selectIsActive().pipe(
      pairwise(),
      tap(([previous, actual]) => {
        if(actual) {
          setTimeout(() => this.searchInputField.nativeElement.focus());
        } else if (previous && !actual) {
          this.menuService.clear();
          this.menuService.focusTerminal();
        }
      }),
      map(([previous, actual]) => actual)
    );
    this.searchOptions = this.menuService.selectSearchOptions();
    this.searchResult = this.menuService.selectSearchResult();
  }

  search(e) {
    this.menuService.search(e.target.value);
  }

  searchNext(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.searchNext();
    setTimeout(() => this.searchInputField.nativeElement.focus());
  }

  searchPrevious(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.searchPrevious();
    setTimeout(() => this.searchInputField.nativeElement.focus());
  }

  searchClear(e) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.clear();
    setTimeout(() => this.searchInputField.nativeElement.focus());
  }

  toggleCaseSensitive(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.toggleCaseSensitive();
    setTimeout(() => this.searchInputField.nativeElement.focus());
  }

  toggleWholeWord(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.toggleWholeWord();
    setTimeout(() => this.searchInputField.nativeElement.focus());
  }

  toggleRegex(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
    this.menuService.toggleRegex();
    setTimeout(() => this.searchInputField.nativeElement.focus());
  }

  close() {
    this.menuService.closeMenu();
  }
}
