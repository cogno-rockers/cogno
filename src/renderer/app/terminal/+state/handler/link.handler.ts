import {IBufferRange, ILinkHandler} from '@xterm/xterm';
import {Subject} from 'rxjs';

export class LinkHandler implements ILinkHandler {
  constructor(private linkClicked: Subject<string>) {
  }
  activate(event: MouseEvent, text: string, range: IBufferRange): void {
    this.linkClicked.next(text);
  }
}
