import { Pipe, PipeTransform } from '@angular/core';
import {Shortcut} from '../../../../../shared/models/shortcuts';

@Pipe({
  name: 'shortcutFilter',
  standalone: true
})
export class ShortcutFilterPipe implements PipeTransform {

  transform(value: Shortcut[] | null, searchValue: string, alias: boolean): unknown {
    return value.filter(s => !!s.command === alias && s.label.toLowerCase().indexOf(searchValue.toLowerCase()) > -1);
  }

}
