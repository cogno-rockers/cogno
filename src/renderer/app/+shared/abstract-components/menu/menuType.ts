import {InjectionToken} from '@angular/core';

export type MenuType = 'AllClosed' |
  'PasteHistory' |
  'TabContext' |
  'Actions' |
  'Workspaces' |
  'TitleBar' |
  'TerminalSearch' |
  'TerminalContext' |
  'PromptContext'
export const DefaultMenuType = new InjectionToken<MenuType>('AllClosed');
