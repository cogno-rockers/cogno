import * as crypt from 'crypto';

export namespace Hash {
  export function create(command: string): string {
    if(!command) { command = '';}
    return crypt.createHash('sha1').update(command.trim()).digest('base64');
  }
}
