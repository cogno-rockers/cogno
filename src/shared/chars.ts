export class Char {
  constructor(public asString) {
  }

  get asUtf8(): string {
    return Char.convertUnicode(this.asString);
  }

  public static convertUnicode(s: string): string {
    if(!s) {return s;}
    const rex = /[\u{200a}\u{e0b0}-\u{e0b7}\u{1f300}-\u{1f5ff}\u{1f900}-\u{1f9ff}\u{1f600}-\u{1f64f}\u{1f680}-\u{1f6ff}\u{2600}-\u{26ff}\u{2700}-\u{27bf}\u{1f1e6}-\u{1f1ff}\u{1f191}-\u{1f251}\u{1f004}\u{1f0cf}\u{1f170}-\u{1f171}\u{1f17e}-\u{1f17f}\u{1f18e}\u{3030}\u{2b50}\u{2b55}\u{2934}-\u{2935}\u{2b05}-\u{2b07}\u{2b1b}-\u{2b1c}\u{3297}\u{3299}\u{303d}\u{00a9}\u{00ae}\u{2122}\u{23f3}\u{24c2}\u{23e9}-\u{23ef}\u{25b6}\u{23f8}-\u{23fa}]/ug;
    return s.replace(rex, match => `\\U${match.codePointAt(0).toString(16)}`);
  }
}

export namespace Chars {
  export const Backspace= new Char(String.fromCharCode(8));
  export const EndOfText = new Char(String.fromCharCode(3));
  export const Delete = new Char(String.fromCharCode(127));
  export const Enter = new Char(String.fromCharCode(13));
  export const VertivalTab = new Char(String.fromCharCode(11));
  export const NegAcknowledge = new Char(String.fromCharCode(21));
  export const Escape = new Char(String.fromCharCode(27));
  export const Branch = new Char(String.fromCodePoint(57504));
  export const TriangleLeft = new Char(String.fromCodePoint(57520));
  export const TriangleSeparatorLeft = new Char(String.fromCodePoint(57521));
  export const TriangleRight = new Char(String.fromCodePoint(57522));
  export const TriangleSeparatorRight = new Char(String.fromCodePoint(57523));
  export const CircleLeft = new Char(String.fromCodePoint(57524));
  export const CircleSeparatorLeft = new Char(String.fromCodePoint(57525));
  export const CircleRight = new Char(String.fromCodePoint(57526));
  export const CircleSeparatorRight = new Char(String.fromCodePoint(57527));

  export const TopLeftCorner = new Char(String.fromCodePoint(9484));
  export const BottomLeftCorner = new Char(String.fromCodePoint(9492));
}
