import {ShellType} from '../../../../shared/models/models';

export enum CommandSource {
  TERMINAL, EDITOR
}

export type Command = {
  executionCount: number;
  selectionCount: number;
  lastExecutionDate?: Date;
  lastSelectionDate?: Date;
  command: string;
  commandHash: string;
  shellTypes: ShellType[];
  commandSource: CommandSource;
};

export type Directory = {
  directories: string[];
  executionCount: number;
  lastExecutionDate?: Date;
  selectionCount: number;
  lastSelectionDate?: Date;
};
