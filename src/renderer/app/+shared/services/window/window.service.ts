import {Injectable} from '@angular/core';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {startWith, Subject, switchMap, throttleTime} from 'rxjs';
import {ElectronService} from '../electron/electron.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WindowService {

  private _onResize: Subject<any> = new Subject<any>();
  public get onResize() {
    return this._onResize;
  }

  constructor(private electronService: ElectronService) {
    this.electronService?.on(IpcChannel.OnResize, (sender) => this.resize());
  }

  resize() {
    this._onResize.next(Date.now());
  }

}
