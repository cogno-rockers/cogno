import {TestHelper} from '../../../../../test/helper';
import {KeyboardService} from './keyboard.service';
import {getSettingsService, getKeyboardService} from '../../../../../test/factory';
import {SettingsService} from '../settings/settings.service';

describe('shortcutsService', () => {

  let shortcutsService: KeyboardService;
  let settingsService: SettingsService;

  beforeEach(() => {
    settingsService = getSettingsService();
    shortcutsService = getKeyboardService();
  });

  it('isShortcutEvent should return false when ask if "e" is shortcut', () => {
    expect(shortcutsService.isShortcut(TestHelper.createKeyboardEvent({code: 'KeyE', key: 'e'}))).toBeFalsy();
  });

  it('isShortcutEvent should return true when ask if "space + ctrl" is shortcut', () => {
    expect(shortcutsService.isShortcut(TestHelper.createKeyboardEvent({code: 'Space', key: ' ', ctrlKey: true}))).toBeTruthy();
  });

  it('isShortcut should work correct - ctrl key truthy', () => {
    expect(shortcutsService.isShortcut(TestHelper.createKeyboardEvent({code: 'KeyU', key: 'u', ctrlKey: true}))).toBeTruthy();
  });

  it('isShortcut should work correct - alt key truthy', () => {
    const settings = TestHelper.createSettings();
    settings.shortcuts.copy = 'Alt+U';
    settingsService.update({settings: settings});
    expect(shortcutsService.isShortcut(TestHelper.createKeyboardEvent({code: 'KeyU', key: 'u', altKey: true}))).toBeTruthy();
  });

  it('isShortcut should work correct - empty shortcut', () => {
    const settings = TestHelper.createSettings();
    settings.shortcuts.copy = undefined;
    settingsService.update({settings: settings});
    expect(shortcutsService.isShortcut(TestHelper.createKeyboardEvent({code: 'KeyU', key: 'u', altKey: true}))).toBeFalsy();
  });

  it('isShortcut should work correct - falsy', () => {
    expect(shortcutsService.isShortcut(TestHelper.createKeyboardEvent({code: 'KeyX', key: 'x'}))).toBeFalsy();
  });
});
