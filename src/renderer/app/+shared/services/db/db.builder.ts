import Datastore from '@seald-io/nedb';

export class DbBuilder {

  private _isInTestMode = false;
  private _path: string;

  private constructor() {
  }

  public static build(testmode: boolean = false): DbBuilder {
    const builder = new DbBuilder();
    builder._isInTestMode = testmode;
    return builder;
  }

  public path(path: string): DbBuilder {
    this._path = path;
    return this;
  }

  public create(): Datastore {
    const db = window.require('@seald-io/nedb');
    if (this._isInTestMode) {
      return new db({inMemoryOnly: true});
    }
    return new db({filename: this._path, autoload: false});
  }
}
