import {SuggestionCalculation} from '../models';
import {RegexpHelper} from '../../../../../common/regexp';
import {AnsiEscSequence} from '../../../../../common/ansi-escape';
import {Chars} from '../../../../../../../shared/chars';
import {Normalizer} from '../../../../../../../shared/normalizer';
import {ShellType} from '../../../../../../../shared/models/models';

export class CommandBuilder {

  constructor(private normalizer: Normalizer) {
  }

  apply(calculation: SuggestionCalculation, tokens: string[]) {
    if (!tokens || !calculation ) {
      return;
    }
    const command = this.buildCommand(calculation);
    const regexpResult = command.match(this.buildSearchRegexp(tokens))?.filter(s => s !== '');
    const matches = regexpResult ? new Set(regexpResult).size : 0;
    const regexpStartOfWordResult = command.match(this.buildSearchRegexpStartOfWord(tokens))?.filter(s => s !== '');
    const matchesStartOfWord = regexpStartOfWordResult ? new Set(regexpStartOfWordResult).size : 0;
    const c = this.buildExecCommand(command);
    calculation.labelMatches = matches;
    calculation.labelMatchesStartOfWord = matchesStartOfWord;
    calculation.label = this.buildLabel(command);
    calculation.shellCommand = c.command;
    calculation.terminalCommand = calculation.label;
    calculation.nextPlaceholder = c.placeholder;
    calculation.executedInDirectory = this.buildPathToDirectoryLabel(calculation);
  }

  private buildExecCommand(command: string): {command: string, placeholder?: string} {
    const commandPlaceholder = new RegExp(RegexpHelper.RegexCommandPlaceholder, 'g').exec(command);
    if(!commandPlaceholder) {return {command};}
    const indexFirstMatch = commandPlaceholder.index;
    const firstMatch = commandPlaceholder[0];
    const moveCursor = AnsiEscSequence.moveCursorLeft(command.length - firstMatch.length - indexFirstMatch);
    return {command: command.replace(commandPlaceholder[0], "") + moveCursor, placeholder: firstMatch};
  }

  private buildPathToDirectoryLabel(calculation: SuggestionCalculation): string {
    let label = '';
    if (calculation.shellCommand && calculation.pathToDirectory && calculation.pathToDirectory.length > 0) {
      label = './' + this.normalizer.toPath(calculation.pathToDirectory, calculation.isPathToDirectoryFromRoot);
    }
    return label;
  }

  private buildCommand(calculation: SuggestionCalculation): string {
    let command = '';
    if (calculation.shellCommand) {
      command += calculation.shellCommand;
    } else if (calculation.pathToDirectory && calculation.pathToDirectory.length > 0) {
      command += `cd ${this.normalizer.toPath(calculation.pathToDirectory, calculation.isPathToDirectoryFromRoot)}/`;
    }
    return command.trim();
  }

  private buildLabel(command: string): string {
    let c;
    if(command.startsWith('cd ../')) {
      c = `cd .../${command.replace('cd ', '').replace(/\.\.\//g, '')}`;
    } else {
      c = command;
    }
    return this.normalizer.location.shellType === ShellType.Powershell ? c.toLowerCase() : c;
  }

  private buildSearchRegexp(tokens: string[]): RegExp {
    const regexp = tokens.map(t => RegexpHelper.escapeWeekRegExp(t)).reduce((v1, v2) => {
      if (!v2) {
        return v1;
      }
      if (v1.length > 0) {
        v1 += '|';
      }
      return v1 + `(${v2})`;
    }, '');
    return new RegExp(regexp, 'ig');
  }

  private buildSearchRegexpStartOfWord(tokens: string[]): RegExp {
    const regexp = tokens.map(t => RegexpHelper.escapeWeekRegExp(t)).reduce((v1, v2) => {
      if (!v2) {
        return v1;
      }
      if (v1.length > 0) {
        v1 += '|';
      }
      return v1 + `((^|\\s)${v2})`;
    }, '');
    return new RegExp(regexp, 'ig');
  }
}
