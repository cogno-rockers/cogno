import {Injectable} from '@angular/core';
import {combineLatest, Observable} from 'rxjs';
import {
  AutocompleteConfig,
  Settings,
  ShellConfig,
  Theme
} from '../../../../../shared/models/settings';
import {filter, first, map} from 'rxjs/operators';
import {Color} from '../../../../../shared/color';
import {createStore, Store} from '../../../common/store/store';
import {Shortcut, Shortcuts, shortcutsToShortcutsMap} from '../../../../../shared/models/shortcuts';

export interface SettingsState {
  settings: Settings;
  shortcutsMap: Map<string, Shortcut[]>;
  errors: string[];
  showOnboarding: boolean;
  isInDarkMode: boolean;
}

const initialState: SettingsState = {
  settings: null,
  errors: undefined,
  showOnboarding: false,
  shortcutsMap: new Map<string, Shortcut[]>(),
  isInDarkMode: false
};

@Injectable({
  providedIn: 'root'
})
export class SettingsService{
  private store: Store<SettingsState> = createStore('settings', initialState);

  constructor() {
  }

  update(settings: Partial<SettingsState>) {
    if (settings?.settings?.shortcuts) {
      settings.shortcutsMap = shortcutsToShortcutsMap(settings.settings.shortcuts);
    }
    this.store.update(settings);
  }

  selectHasErrors(): Observable<boolean> {
    return this.store.select(s => !!s.errors);
  }

  selectActiveThemeWithScrollback(): Observable<{theme: Theme, scrollbackLines: number}> {
    return combineLatest([this.store.select(s => s.settings), this.store.select(s => s.isInDarkMode)]).pipe(
      filter(([theme, darkMode]) => !!theme),
      map(([settings, darkMode]) => ({theme: this.getActiveTheme(), scrollbackLines: settings.general.scrollbackLines})));
  }

  selectActiveTheme(): Observable<Theme> {
    return combineLatest([this.store.select(s => s.settings), this.store.select(s => s.isInDarkMode)]).pipe(
      filter(([theme, darkMode]) => !!theme),
      map(([settings, darkMode]) => this.getActiveTheme()));
  }

  getActiveTheme(): Theme {
    const themes = this.store.get(s => s.settings.themes);
    const defaultTheme = themes.find(s => s.isDefault);
    const darkTheme = themes.find(s => s.isDarkModeTheme);
    const isInDarkMode = this.store.get(s => s.isInDarkMode);
    if (isInDarkMode && darkTheme) {
      return darkTheme;
    }
    if (defaultTheme) {
      return defaultTheme;
    }
    return themes[0];
  }

  selectIsLightBackground(): Observable<boolean> {
    return this.selectActiveTheme().pipe(map(activeTheme => Color.isLight(activeTheme.colors.background)));
  }

  getShells(): ShellConfig[] {
    return this.store.get(s => s.settings?.shells) || [];
  }

  selectShells(): Observable<ShellConfig[]> {
    return this.store.select(s => s.settings?.shells || []);
  }

  selectSettingsLoaded(): Observable<number> {
    return this.store.select(s => s.settings).pipe(filter(s => !!s && !!s.shells && s.shells.length > 0), first(), map(() => Date.now()));
  }

  getDefaultShellConfig(): ShellConfig {
    return this.getShells().find(a => a.default);
  }

  selectShortcuts(): Observable<Shortcuts> {
    return this.store.select(s => s.settings?.shortcuts);
  }

  selectShortcutsAsArray(): Observable<Shortcut[]>{
    return this.store.select(s => Array.from(s.shortcutsMap.values()).flat());
  }

  getShortcuts(): Shortcuts {
    return this.store.get(s => s.settings?.shortcuts);
  }

  getShortcutsAsMap(): Map<string, Shortcut[]> {
    return this.store.get(s => s.shortcutsMap);
  }
  getShortcutsAsArray(): Shortcut[] {
    return this.store.get(s => Array.from(s.shortcutsMap.values()).flat());
  }

  selectThemes() {
    return this.store.select(s => s.settings.themes);
  }

  getAutocompleteConfig(): AutocompleteConfig {
    return this.store.get(state => state.settings.autocomplete);
  }

  selectSettings(): Observable<Settings> {
    return this.store.select(s => s.settings);
  }

  selectErrors(): Observable<string[]> {
    return this.store.select(s => s.errors || []);
  }

  selectShowOnboarding(): Observable<boolean> {
    return this.store.select(s => s.showOnboarding);
  }

  getAutocompletePosition(): 'cursor' | 'line' {
    return this.store.get(s => s.settings?.autocomplete?.position);
  }

  getAutocompleteMode(): 'always' | 'shortcut' {
    return this.store.get(s => s.settings?.autocomplete?.mode);
  }

  setDarkMode(darkMode: boolean) {
    return this.store.update({isInDarkMode: darkMode});
  }

  selectIsDarkModeAvailable(): Observable<boolean> {
    return this.store.select(s => s.settings?.themes).pipe(
      map(themes => {
        const defaultTheme = themes?.find(s => s.isDefault);
        const darkTheme = themes?.find(s => s.isDarkModeTheme);
        return !!defaultTheme && !!darkTheme && defaultTheme !== darkTheme;
      })
    );
  }

  toggleDarkMode() {
    this.store.update(s => ({isInDarkMode: !s.isInDarkMode}));
  }

  selectIsInDarkMode() {
    return this.store.select(s => s.isInDarkMode);
  }

  getIsPasteOnRightClickEnabled(): boolean {
    return this.store.get(s => s.settings?.general?.enablePasteOnRightClick || false);
  }

  getIsCopyOnSelectEnabled(): boolean {
    return this.store.get(s => s.settings?.general?.enableCopyOnSelect || false);
  }

  getIsOpenTabInSameDirectoryEnabled(): boolean {
    return this.store.get(s => s.settings?.general?.openTabInSameDirectory || false);
  }
}
