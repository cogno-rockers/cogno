import {Injectable} from '@angular/core';

export interface Clock {
  now(offset?: number): Date;
}

@Injectable({
  providedIn: 'root'
})
export class DefaultClock implements Clock {
  now(offset?: number): Date {
    return new Date();
  }
}
