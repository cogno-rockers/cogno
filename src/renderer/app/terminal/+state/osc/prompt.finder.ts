import {RegexpHelper} from '../../../common/regexp';
import {Chars} from '../../../../../shared/chars';

export class PromptFinder {

  private regexp: string;

  constructor(private promptTerminator: string, private usesFinalSpacePromptTerminator: boolean) {
    const char = RegexpHelper.escapeStrongRegExp(this.promptTerminator || '');
    const postWhitespace = this.usesFinalSpacePromptTerminator ? '\\s?' : '';
    this.regexp = `^(?<prompt>.*?\\s?${char}${postWhitespace})\\s*$`;
  }

  public tryToFindPrompt(lineText: string): string | undefined {
    if(!lineText) {return undefined;}
    if (!this.promptTerminator) {
      return this.tryToGuessPromptTerminator(lineText);
    } else {
      return lineText.match(new RegExp(this.regexp))?.groups?.prompt;
    }
  }

  private tryToGuessPromptTerminator(lineText: string): string | undefined  {
    return lineText.match(/(?<prompt>(\(.+\)\s)?(?:PS.+>\s?$))/)?.groups?.prompt || // Powershell
      lineText.match(/^(?<prompt>.*?\s?#\s?)\s*$/)?.groups?.prompt || // ZSH Prompt on Linux
      lineText.match(/^(?<prompt>.*?\s?%\s?)\s*$/)?.groups?.prompt || // ZSH Prompt on Mac
      lineText.match(/^(?<prompt>.*?\s?\$\s?)\s*$/)?.groups?.prompt || // Bash Prompt
      lineText.match(/^(?<prompt>.*?\s?\u276f\s?)\s*$/)?.groups?.prompt || // Custom prompts like starship or new Powershell end in the common \u276f (❯) character;
      lineText.match(/^(?<prompt>.*?)\u200A$/)?.groups?.prompt;
  }
}
