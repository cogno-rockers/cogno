import * as log from 'electron-log/main';
import * as path from 'path';
import { LogLevel } from '../shared/loglevel';
import * as electron from 'electron';



export namespace Logger {

  const instance = log;

  export function init(dirPath: string, level: LogLevel = null) {
    instance.initialize({ preload: true });
    instance.transports.file.resolvePathFn = () => path.join(dirPath, `cogno.log`);
    instance.transports.file.level = level || LogLevel.ERROR;
    instance.transports.file.format = '{y}/{m}/{d} {h}:{i}:{s}:{ms} {level} {text}';
// Set approximate maximum log size in bytes. When it exceeds,
// the archived log will be saved as the log.old.log file
    instance.transports.file.maxSize = 5 * 1024 * 1024;

    instance.transports.console.level = level || LogLevel.ERROR;
    instance.transports.console.format = '{h}:{i}:{s}:{ms} [{level}] {text}';

    instance.errorHandler.startCatching({
      showDialog: false,
      onError({ createIssue, error, processType, versions }) {
        if (processType === 'renderer' || error.message.includes('EPIP')) {
          return;
        }
        electron.dialog.showMessageBox({
          title: 'An error occurred',
          message: error.message,
          detail: error.stack,
          type: 'error',
          buttons: ['Ignore', 'Report', 'Exit'],
        })
          .then((result) => {
            if (result.response === 1) {
              createIssue('https://gitlab.com/cogno-rockers/cogno/-/issues/new', {
                'issue[title]': `Error report for ${versions.app}`,
                'issue[description]': 'Error:\n```' + error.stack + '\n```\n' + `OS: ${versions.os}`
              });
              return;
            }

            if (result.response === 2) {
              electron.app.quit();
            }
          });
      }
    });
  }

  export function _zipName(): string {
    const d = new Date();
    const now = `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()}_${d.getHours()}_${d.getMinutes()}_${d.getSeconds()}`;
    return `suite_log_${now}.zip`
  }

  const instanceVariablesAsString = () => {
    return `[${instance.variables['processType']}]`;
  }
  export function info(...params: any[]) {
    instance.info(instanceVariablesAsString(), ...params);
  }

  export function debug(...params: any[]) {
    instance.debug(instanceVariablesAsString(), ...params);
  }

  export function warn(...params: any[]) {
    instance.warn(instanceVariablesAsString(), ...params);
  }

  export function error(...params: any[]) {
    instance.error(instanceVariablesAsString(), ...params);
  }

  export function silly(...params: any[]) {
    instance.silly(instanceVariablesAsString(), ...params);
  }

  export function infoScope(scope: string = 'NoScope', ...params: any[]) {
    instance.info(`[${scope}]`+ instanceVariablesAsString(), ...params);
  }

  export function debugScope(scope: string = 'NoScope', ...params: any[]) {
    instance.debug(`[${scope}]`+ instanceVariablesAsString(), ...params);
  }

  export function warnScope(scope: string = 'NoScope', ...params: any[]) {
    instance.warn(`[${scope}]`+ instanceVariablesAsString(), ...params);
  }

  export function errorScope(scope: string = 'NoScope', ...params: any[]) {
    instance.error(`[${scope}]`+ instanceVariablesAsString(), ...params);
  }

  export function sillyScope(scope: string = 'NoScope', ...params: any[]) {
    instance.silly(`[${scope}]`+ instanceVariablesAsString(), ...params);
  }
  export function setLevel(level: LogLevel) {
    instance.transports.file.level = level;
    instance.transports.console.level = level;
  }
}
