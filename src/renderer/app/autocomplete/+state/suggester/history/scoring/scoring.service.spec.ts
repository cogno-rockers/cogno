import {MatchShellType, SuggestionCalculation} from '../models';
import {ScoringService} from './scoring.service';
import {SuggestionType} from '../../suggester';

describe('ScoringService', () => {

  let scoringService: ScoringService;

  beforeEach(() => {
    scoringService = new ScoringService();
  });

  it('should calculate correct score for label matches greater than tokenCount', () => {
    const suggestion1: SuggestionCalculation = {labelMatches: 3};
    const suggestion2: SuggestionCalculation = {labelMatches: 4};
    scoringService.apply(suggestion1, {tokenCount: 3});
    scoringService.apply(suggestion2, {tokenCount: 3});
    expect(suggestion1.scoreDetails.matches).toEqual(suggestion2.scoreDetails.matches);
  });

  it('should calculate correct score for label matches', () => {
    const suggestion1: SuggestionCalculation = {labelMatches: 3};
    const suggestion2: SuggestionCalculation = {labelMatches: 4};
    scoringService.apply(suggestion1, {tokenCount: 4});
    scoringService.apply(suggestion2, {tokenCount: 4});
    expect(suggestion2.scoreDetails.matches).toBeGreaterThan(suggestion1.scoreDetails.matches);
  });

  it('should calculate correct score for label matches if tokencount is 0', () => {
    const suggestion1: SuggestionCalculation = {labelMatches: 3};
    scoringService.apply(suggestion1, {tokenCount: 0});
    expect(suggestion1.score).not.toEqual(Number.NaN);
  });

  it('should calculate correct score directory length', () => {
    const suggestion1: SuggestionCalculation = {pathToDirectory: ['a']};
    const suggestion2: SuggestionCalculation = {pathToDirectory: ['a', 'b']};
    scoringService.apply(suggestion1, {});
    scoringService.apply(suggestion2, {});
    expect(suggestion1.scoreDetails.directory).toBeGreaterThan(suggestion2.scoreDetails.directory);
  });

  it('should calculate correct score execution count', () => {
    const suggestion1: SuggestionCalculation = {executionCount: 2};
    const suggestion2: SuggestionCalculation = {executionCount: 3};
    scoringService.apply(suggestion1, {});
    scoringService.apply(suggestion2, {});
    expect(suggestion2.scoreDetails.executionCount).toBeGreaterThan(suggestion1.scoreDetails.executionCount);
  });

  it('should calculate correct score execution date', () => {
    const date1 = new Date('2020-11-02T00:00:00');
    const date2 = new Date('2020-11-03T00:00:00');
    const minDate = new Date('2020-11-01T00:00:00');
    const maxDate = new Date('2020-11-03T00:00:00');
    const suggestion1: SuggestionCalculation = {executionDate: date1};
    const suggestion2: SuggestionCalculation = {executionDate: date2};
    scoringService.apply(suggestion1, {minExecutionDate: minDate, maxExecutionDate: maxDate});
    scoringService.apply(suggestion2, {minExecutionDate: minDate, maxExecutionDate: maxDate});
    expect(suggestion2.scoreDetails.executionDate).toBeGreaterThan(suggestion1.scoreDetails.executionDate);
  });

  it('should calculate correct score execution date - min > date', () => {
    const date = new Date(0);
    const minDate = new Date('2020-11-01T00:00:00');
    const maxDate = new Date('2020-11-03T00:00:00');
    const suggestion: SuggestionCalculation = {executionDate: date};
    scoringService.apply(suggestion, {minExecutionDate: minDate, maxExecutionDate: maxDate});
    expect(suggestion.scoreDetails.executionDate).toEqual(1);
  });

  it('should calculate correct score execution date - date > max', () => {
    const date = new Date('2020-11-04T00:00:00');
    const minDate = new Date('2020-11-01T00:00:00');
    const maxDate = new Date('2020-11-03T00:00:00');
    const suggestion: SuggestionCalculation = {executionDate: date};
    scoringService.apply(suggestion, {minExecutionDate: minDate, maxExecutionDate: maxDate});
    expect(suggestion.scoreDetails.executionDate).toEqual(1);
  });

  it('should calculate correct score global execution date', () => {
    const date1 = new Date('2020-11-02T00:00:00');
    const date2 = new Date('2020-11-03T00:00:00');
    const minDate = new Date('2020-11-01T00:00:00');
    const maxDate = new Date('2020-11-03T00:00:00');
    const suggestion1: SuggestionCalculation = {globalExecutionDate: date1};
    const suggestion2: SuggestionCalculation = {globalExecutionDate: date2};
    scoringService.apply(suggestion1, {minGlobalExecutionDate: minDate, maxGlobalExecutionDate: maxDate});
    scoringService.apply(suggestion2, {minGlobalExecutionDate: minDate, maxGlobalExecutionDate: maxDate});
    expect(suggestion2.scoreDetails.globalExecutionDate).toBeGreaterThan(suggestion1.scoreDetails.globalExecutionDate);
  });

  it('should calculate correct score global execution date - min > date', () => {
    const date = new Date(0);
    const minDate = new Date('2020-11-01T00:00:00');
    const maxDate = new Date('2020-11-03T00:00:00');
    const suggestion: SuggestionCalculation = {globalExecutionDate: date};
    scoringService.apply(suggestion, {minGlobalExecutionDate: minDate, maxGlobalExecutionDate: maxDate});
    expect(suggestion.scoreDetails.globalExecutionDate).toBe(1);
  });

  it('should calculate correct score global execution date - date > max', () => {
    const date = new Date('2020-11-04T00:00:00');
    const minDate = new Date('2020-11-01T00:00:00');
    const maxDate = new Date('2020-11-03T00:00:00');
    const suggestion: SuggestionCalculation = {globalExecutionDate: date};
    scoringService.apply(suggestion, {minGlobalExecutionDate: minDate, maxGlobalExecutionDate: maxDate});
    expect(suggestion.scoreDetails.globalExecutionDate).toBe(1);
  });

  it('should calculate correct score sum', () => {
    const date = new Date('2020-11-03T00:00:00');
    const tokenCount = 4;
    const scoringValues = {
      minGlobalExecutionDate: date,
      maxGlobalExecutionDate: date,
      minExecutionDate: date,
      maxExecutionDate: date,
      tokenCount: tokenCount
    };
    const suggestion1: SuggestionCalculation = {labelMatches: 4, labelMatchesStartOfWord: 2, pathToDirectory: [], executionCount: 2, executionDate: date, globalExecutionDate: date, matchShellType: MatchShellType.Yes};
    const suggestion2: SuggestionCalculation = {labelMatches: 3, labelMatchesStartOfWord: 2, pathToDirectory: [], executionCount: 20, executionDate: date, globalExecutionDate: date, matchShellType: MatchShellType.Yes};
    scoringService.apply(suggestion1, scoringValues);
    scoringService.apply(suggestion2, scoringValues);
    expect(suggestion1.score).toBeGreaterThan(suggestion2.score);
  });

  it('should add boost to score sum', () => {
    const date = new Date('2020-11-03T00:00:00');
    const tokenCount = 4;
    const scoringValues = {
      minGlobalExecutionDate: date,
      maxGlobalExecutionDate: date,
      minExecutionDate: date,
      maxExecutionDate: date,
      tokenCount: tokenCount
    };
    const suggestion1: SuggestionCalculation = {labelMatches: 3, labelMatchesStartOfWord: 2, pathToDirectory: [], executionCount: 2, executionDate: date, globalExecutionDate: date, suggestionType: SuggestionType.LocalCommand, matchShellType: MatchShellType.Yes};
    const suggestion2: SuggestionCalculation = {labelMatches: 4, labelMatchesStartOfWord: 2, pathToDirectory: [], executionCount: 20, executionDate: date, globalExecutionDate: date, suggestionType: SuggestionType.GlobalCommand, matchShellType: MatchShellType.Yes};
    scoringService.apply(suggestion1, scoringValues);
    scoringService.apply(suggestion2, scoringValues);
    expect(suggestion1.score).toBeGreaterThan(suggestion2.score);
  });

  it('should calc boost for shellTypes correct', () => {
    const date = new Date('2020-11-03T00:00:00');
    const tokenCount = 4;
    const scoringValues = {
      minGlobalExecutionDate: date,
      maxGlobalExecutionDate: date,
      minExecutionDate: date,
      maxExecutionDate: date,
      tokenCount: tokenCount
    };
    const suggestion1: SuggestionCalculation = {labelMatches: 4, labelMatchesStartOfWord: 2, pathToDirectory: [], executionCount: 20, executionDate: date, globalExecutionDate: date, suggestionType: SuggestionType.LocalCommand, matchShellType: MatchShellType.Partly};
    const suggestion2: SuggestionCalculation = {labelMatches: 4, labelMatchesStartOfWord: 2, pathToDirectory: [], executionCount: 20, executionDate: date, globalExecutionDate: date, suggestionType: SuggestionType.LocalCommand, matchShellType: MatchShellType.Yes};
    scoringService.apply(suggestion1, scoringValues);
    scoringService.apply(suggestion2, scoringValues);
    expect(suggestion2.score).toBeGreaterThan(suggestion1.score);
  });
});
