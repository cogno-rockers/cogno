import {ShellConfig, Theme} from '../../../../../../shared/models/settings';
import {RendererState} from '../../../../../../shared/models/models';
import {ShellAdapter} from './shell.adapter';
import {TestHelper} from '../../../../../../test/helper';

class TestShell extends ShellAdapter {

  injectCommands(script: string, theme: Theme): string[] {
      return []
  }

  readonly clear: string;

  constructor(config: ShellConfig) {
    super(config);
  }


  abortTask(): string {
    return '';
  }

  cursorLeftSequence(): string {
    return 'l';
  }

  cursorRightSequence(): string {
    return 'r';
  }

  isValid(param: { returnCode: any; machine: any; directory: string[]; user: any; branch: any; timestamp: any }) {
  }

  backspaceSequence(): string {
    return 'b';
  }
}

describe('ShellAdapter', () => {

  let rendererState: RendererState;
  const shell = new TestShell(TestHelper.createShellConfig());

  beforeEach(() => {
    rendererState = {
      currentInput: {input: '', lines: []},
      cursorPosition: {x: 2, y: 2, line: 2, appWindowX: 0, appWindowY: 0, windowX: 0, windowY: 0, height: 0, width: 0, offsetX: 2},
    };
  });

  it('should calc clearLine correct - empty line', () => {
    const result = shell.clearLine(rendererState);
    expect(result).toBe('');
  });

  it('should calc clearLine correct - cursor position begin', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    const result = shell.clearLine(rendererState);
    expect(result).toBe('rrrrbbbb');
  });

  it('should calc clearLine correct - cursor position middle', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    rendererState.cursorPosition.x = rendererState.cursorPosition.x + 2;
    const result = shell.clearLine(rendererState);
    expect(result).toBe('rrbbbb');
  });

  it('should calc clearLine correct - cursor position end', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    rendererState.cursorPosition.x = rendererState.cursorPosition.x + rendererState.currentInput.input.length;
    const result = shell.clearLine(rendererState);
    expect(result).toBe('bbbb');
  });

  it('should calc clearLine correct multiline - cursor position end of second line', () => {
    const firstLine = 't';
    const secondLine = 't';
    rendererState.currentInput = {input: firstLine + secondLine,lines: [{line: rendererState.cursorPosition.line - 1, lineText: firstLine}, {line: rendererState.cursorPosition.line, lineText: secondLine}]};
    rendererState.cursorPosition.x = secondLine.length;
    const result = shell.clearLine(rendererState);
    expect(result).toBe('b'.repeat(firstLine.length + secondLine.length));
  });

  it('should calc clearLine correct multiline - cursor position after second line', () => {
    const firstLine = 't';
    const secondLine =    't';
    rendererState.currentInput = {input:'tt',lines: [{line: rendererState.cursorPosition.line - 1, lineText: firstLine}, {line: rendererState.cursorPosition.line, lineText: secondLine}]};
    rendererState.cursorPosition.x = secondLine.length + 1;
    const result = shell.clearLine(rendererState);
    expect(result).toBe('b'.repeat(firstLine.length + secondLine.length + 1));
  });

  it('should calc clearLineToEnd correct - empty line', () => {
    const result = shell.clearLineToEnd(rendererState);
    expect(result).toBe('');
  });

  it('should calc clearLineToEnd correct - cursor position begin', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    const result = shell.clearLineToEnd(rendererState);
    expect(result).toBe('rrrrbbbb');
  });

  it('should calc clearLineToEnd correct - cursor position middle', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    rendererState.cursorPosition.x = rendererState.cursorPosition.x + 2;
    const result = shell.clearLineToEnd(rendererState);
    expect(result).toBe('rrbb');
  });

  it('should calc clearLineToEnd correct - cursor position end', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    rendererState.cursorPosition.x = rendererState.cursorPosition.x + rendererState.currentInput.input.length;
    const result = shell.clearLineToEnd(rendererState);
    expect(result).toBe('');
  });

  it('should calc clearLineToEnd correct multiline - cursor position end of second line', () => {
    const firstLine = 'test ';
    const secondLine = 'my position';
    rendererState.currentInput = {input: firstLine + secondLine,lines: [{line: rendererState.cursorPosition.line - 1, lineText: firstLine}, {line: rendererState.cursorPosition.line, lineText: secondLine}]};
    rendererState.cursorPosition.x = secondLine.length + 1;
    const result = shell.clearLineToEnd(rendererState);
    expect(result).toBe('');
  });

  it('should calc clearLineToEnd correct multiline - cursor position end of second line with whitespace', () => {
    const firstLine = 'test myposi';
    const secondLine =    'tion    ';
    rendererState.currentInput = {input:'test myposition    ',lines: [{line: rendererState.cursorPosition.line - 1, lineText: firstLine}, {line: rendererState.cursorPosition.line, lineText: secondLine}]};
    rendererState.cursorPosition.x = secondLine.length + 1;
    const result = shell.clearLineToEnd(rendererState);
    expect(result).toBe('');
  });


  it('should calc clearLineToStart correct - empty line', () => {
    const result = shell.clearLineToStart(rendererState);
    expect(result).toBe('');
  });

  it('should calc clearLineToStart correct - cursor position begin', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    const result = shell.clearLineToStart(rendererState);
    expect(result).toBe('');
  });

  it('should calc clearLineToStart correct - cursor position middle', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    rendererState.cursorPosition.x = rendererState.cursorPosition.x + 2;
    const result = shell.clearLineToStart(rendererState);
    expect(result).toBe('bb');
  });

  it('should calc clearLineToStart correct - cursor position end', () => {
    rendererState.currentInput = {input: 'test',lines: [{line: rendererState.cursorPosition.line, lineText: 'test'}]};
    rendererState.cursorPosition.x = rendererState.cursorPosition.x + rendererState.currentInput.input.length;
    const result = shell.clearLineToStart(rendererState);
    expect(result).toBe('bbbb');
  });

  it('should calc clearLineToStart correct multiline - cursor position end of second line', () => {
    const firstLine = 't';
    const secondLine = 't';
    rendererState.currentInput = {input: firstLine + secondLine,lines: [{line: rendererState.cursorPosition.line - 1, lineText: firstLine}, {line: rendererState.cursorPosition.line, lineText: secondLine}]};
    rendererState.cursorPosition.x = secondLine.length;
    const result = shell.clearLineToStart(rendererState);
    expect(result).toBe('bb');
  });

  it('should calc clearLineToStart correct multiline - cursor position after second line', () => {
    const firstLine = 't';
    const secondLine =    't';
    rendererState.currentInput = {input:'tt',lines: [{line: rendererState.cursorPosition.line - 1, lineText: firstLine}, {line: rendererState.cursorPosition.line, lineText: secondLine}]};
    rendererState.cursorPosition.x = secondLine.length + 1;
    const result = shell.clearLineToStart(rendererState);
    expect(result).toBe('bbb');
  });

  it('should calc cursorToNextWord correct - empty', () => {
    const firstLine = '';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorToNextWord(rendererState);
    expect(result).toEqual({command: '', steps: 0});
  });

  it('should calc cursorToNextWord correct - with one word', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorToNextWord(rendererState);
    expect(result).toEqual({command: 'rrrr', steps: 4});
  });

  it('should calc cursorToNextWord correct - with one word and whitespace', () => {
    const firstLine = 'test ';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorToNextWord(rendererState);
    expect(result).toEqual({command: 'rrrr', steps: 4});
  });

  it('should calc cursorToNextWord correct - with two words', () => {
    const firstLine = 'test t';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorToNextWord(rendererState);
    expect(result).toEqual({command: 'rrrrr', steps: 5});
  });

  it('should calc cursorToNextWord correct - with three words', () => {
    const firstLine = 'test t t';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorToNextWord(rendererState);
    expect(result).toEqual({command: 'rrrrr', steps: 5});
  });

  it('should calc cursorToNextWord correct - with one word and whitespace, cursor at end of line', () => {
    const firstLine = 'test ';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.cursorToNextWord(rendererState);
    expect(result).toEqual({command: '', steps: 0});
  });

  it('should calc goToPreviousWord correct - empty', () => {
    const firstLine = '';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorToPreviousWord(rendererState);
    expect(result).toEqual({command: '', steps: 0});
  });

  it('should calc goToPreviousWord correct - with one word', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.cursorToPreviousWord(rendererState);
    expect(result).toEqual({command: 'llll', steps: 4});
  });

  it('should calc goToPreviousWord correct - with two words', () => {
    const firstLine = 'test t';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 1;
    const result = shell.cursorToPreviousWord(rendererState);
    expect(result).toEqual({command: 'lllll', steps: 5});
  });

  it('should calc goToPreviousWord correct - with three words', () => {
    const firstLine = 'test t t';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.cursorToPreviousWord(rendererState);
    expect(result).toEqual({command: 'l', steps: 1});
  });

  it('should calc goToPreviousWord correct - with three words - cursor between words', () => {
    const firstLine = 'test t t';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 1;
    const result = shell.cursorToPreviousWord(rendererState);
    expect(result).toEqual({command: 'll', steps: 2});
  });

  it('should calc deleteNextWord correct - empty', () => {
    const firstLine = '';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.deleteNextWord(rendererState);
    expect(result).toBe('');
  });

  it('should calc deleteNextWord correct - one word, cursor begin of word', () => {
    const firstLine = 'ttt';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.deleteNextWord(rendererState);
    expect(result).toBe('rrrbbb');
  });

  it('should calc deleteNextWord correct - one word, cursor middle of word', () => {
    const firstLine = 'ttt';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 2;
    const result = shell.deleteNextWord(rendererState);
    expect(result).toBe('rb');
  });

  it('should calc deleteNextWord correct - one word, cursor end of word', () => {
    const firstLine = 'ttt';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 3;
    const result = shell.deleteNextWord(rendererState);
    expect(result).toBe('');
  });

  it('should calc deleteNextWord correct - one word with empty space, cursor begin of word', () => {
    const firstLine = 'ttt ';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.deleteNextWord(rendererState);
    expect(result).toBe('rrrbbb');
  });

  it('should calc deleteNextWord correct - two words with, cursor begin of word one', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.deleteNextWord(rendererState);
    expect(result).toBe('rrrrbbbb');
  });

  it('should calc deleteNextWord correct - two words with, cursor begin of word two', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 3;
    const result = shell.deleteNextWord(rendererState);
    expect(result).toBe('rrrrbbbb');
  });

  it('should calc deletePreviousWord correct - empty', () => {
    const firstLine = '';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.deletePreviousWord(rendererState);
    expect(result).toBe('');
  });

  it('should calc deletePreviousWord correct - one word, cursor begin of word', () => {
    const firstLine = 'ttt';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.deletePreviousWord(rendererState);
    expect(result).toBe('');
  });

  it('should calc deletePreviousWord correct - one word, cursor middle of word', () => {
    const firstLine = 'ttt';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 2;
    const result = shell.deletePreviousWord(rendererState);
    expect(result).toBe('bb');
  });

  it('should calc deletePreviousWord correct - one word, cursor end of word', () => {
    const firstLine = 'ttt';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 3;
    const result = shell.deletePreviousWord(rendererState);
    expect(result).toBe('bbb');
  });

  it('should calc deletePreviousWord correct - one word with empty space, cursor end of word', () => {
    const firstLine = 'ttt ';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 4;
    const result = shell.deletePreviousWord(rendererState);
    expect(result).toBe('bbbb');
  });

  it('should calc deletePreviousWord correct - two words with, cursor begin of word two', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 4;
    const result = shell.deletePreviousWord(rendererState);
    expect(result).toBe('bbbb');
  });

  it('should calc deletePreviousWord correct - two words with, cursor end of word two', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const result = shell.deletePreviousWord(rendererState);
    expect(result).toBe('bbb');
  });

  it('should calc paste correct - no selections', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const result = shell.paste('aaa', rendererState,  {hasSelection: false, text: '', start: undefined, end: undefined});
    expect(result).toBe('aaa');
  });

  it('should calc paste correct - nothing to paste', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const result = shell.paste('', rendererState,  {hasSelection: true, text: '', start: undefined, end: undefined});
    expect(result).toBe('');
  });

  it('should calc paste correct - with selection in line before current input', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const selection = {hasSelection: true, text: 'assd', start: {x: 0, y: rendererState.currentInput.lines[0].line - 1}, end: {x: 4, y: rendererState.currentInput.lines[0].line - 1}};
    const result = shell.paste('aaa', rendererState,  selection);
    expect(result).toBe('aaa');
  });

  it('should calc paste correct - with selection in same line, cursor after selection', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const result = shell.paste('aaa', rendererState,  {hasSelection: true, text: 'ttt', start: {x: 2, y: rendererState.currentInput.lines[0].line}, end: {x: 5, y: rendererState.currentInput.lines[0].line}});
    expect(result).toBe('llllbbbaaa');
  });

  it('should calc paste correct - with selection in same line, cursor before selection', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.paste('aaa', rendererState,  {hasSelection: true, text: 'ttt', start: {x: rendererState.cursorPosition.x, y: rendererState.currentInput.lines[0].line}, end: {x: rendererState.cursorPosition.x + 3, y: rendererState.currentInput.lines[0].line}});
    expect(result).toBe('rrrbbbaaa');
  });

  it('should calc paste correct - with selection in same line and line before, cursor after selection', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const selection = {hasSelection: true, text: 'asd/nttt', start: {x: 0, y: rendererState.currentInput.lines[0].line - 1}, end: {x: rendererState.cursorPosition.x, y: rendererState.currentInput.lines[0].line}};
    const result = shell.paste('aaa', rendererState,  selection);
    expect(result).toBe('bbbbbbbaaa');
  });

  it('should calc paste correct - with selection in line after', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const selection = {hasSelection: true, text: '    ', start: {x: 0, y: rendererState.currentInput.lines[0].line + 2}, end: {x: 7, y: rendererState.currentInput.lines[0].line + 2}};
    const result = shell.paste('aaa', rendererState,  selection);
    expect(result).toBe('aaa');
  });

  it('should calc cut correct - no selections', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const result = shell.cut(rendererState,  {hasSelection: false, text: '', start: undefined, end: undefined});
    expect(result).toBe('');
  });

  it('should calc cut correct - with selection in line before current input', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const selection = {hasSelection: true, text: 'assd', start: {x: 0, y: rendererState.currentInput.lines[0].line - 1}, end: {x: 4, y: rendererState.currentInput.lines[0].line - 1}};
    const result = shell.cut(rendererState,  selection);
    expect(result).toBe('');
  });

  it('should calc cut correct - with selection in same line, cursor after selection', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const result = shell.cut(rendererState,  {hasSelection: true, text: 'ttt', start: {x: 2, y: rendererState.currentInput.lines[0].line}, end: {x: 5, y: rendererState.currentInput.lines[0].line}});
    expect(result).toBe('llllbbb');
  });

  it('should calc cut correct - with selection in same line, cursor before selection', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cut(rendererState,  {hasSelection: true, text: 'ttt', start: {x: rendererState.cursorPosition.x, y: rendererState.currentInput.lines[0].line}, end: {x: rendererState.cursorPosition.x + 3, y: rendererState.currentInput.lines[0].line}});
    expect(result).toBe('rrrbbb');
  });

  it('should calc cut correct - with selection in same line and line before, cursor after selection', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += 7;
    const selection = {hasSelection: true, text: 'asd/nttt', start: {x: 0, y: rendererState.currentInput.lines[0].line - 1}, end: {x: rendererState.cursorPosition.x, y: rendererState.currentInput.lines[0].line}};
    const result = shell.cut(rendererState,  selection);
    expect(result).toBe('bbbbbbb');
  });

  it('should calc cut correct - with selection in line after', () => {
    const firstLine = 'ttt ddd';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const selection = {hasSelection: true, text: '    ', start: {x: 0, y: rendererState.currentInput.lines[0].line + 2}, end: {x: 7, y: rendererState.currentInput.lines[0].line + 2}};
    const result = shell.cut(rendererState,  selection);
    expect(result).toBe('');
  });

  it('should calc nextArgument correct - empty', () => {
    const firstLine = '';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.goToNextArgument(rendererState);
    expect(result).toEqual({command: '', placeholder: undefined});
  });

  it('should calc nextArgument correct - with one placeholder', () => {
    const firstLine = 'test {{a}}';
    rendererState.currentInput = {input:firstLine, lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.goToNextArgument(rendererState);
    expect(result).toEqual({command: 'rrrrrrrrrrbbbbb', placeholder: '{{a}}'});
  });

  it('should calc nextArgument correct - with one placeholder and whitespace', () => {
    const firstLine = 'test {{?}} ';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.goToNextArgument(rendererState);
    expect(result).toEqual({command: 'rrrrrrrrrrbbbbb', placeholder: '{{?}}'});
  });

  it('should calc nextArgument correct - with two placeholders', () => {
    const firstLine = 'test {{ab}} {{?}}';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.goToNextArgument(rendererState);
    expect(result).toEqual({command: 'rrrrrrrrrrrbbbbbb', placeholder: '{{ab}}'});
  });

  it('should calc previousArgument correct - empty', () => {
    const firstLine = '';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.goToNextArgument(rendererState);
    expect(result).toEqual({command: '', placeholder: undefined});
  });

  it('should calc previousArgument correct - with one placeholder', () => {
    const firstLine = 'test {{bc}}';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.goToPreviousArgument(rendererState);
    expect(result).toEqual({command: 'bbbbbb', placeholder: '{{bc}}'});
  });

  it('should calc previousArgument correct - with two placeholders', () => {
    const firstLine = 'test {{?}} {{?}}';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 5;
    const result = shell.goToPreviousArgument(rendererState);
    expect(result).toEqual({command: 'lbbbbb', placeholder: '{{?}}'});
  });

  it('should calc previousArgument correct - with three placeholders', () => {
    const firstLine = 'test {{?}} {{?}} {{?}}';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.goToPreviousArgument(rendererState);
    expect(result).toEqual({command: 'bbbbb', placeholder: '{{?}}'});
  });

  it('should calc previousArgument correct - with three placeholders - cursor between placeholders', () => {
    const firstLine = 'test {{?}} {{?}} {{?}}';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 5;
    const result = shell.goToPreviousArgument(rendererState);
    expect(result).toEqual({command: 'lbbbbb', placeholder: '{{?}}'});
  });

  it('should calc cursorLeft correct - cursor middle of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 2;
    const result = shell.cursorLeft(rendererState);
    expect(result).toBe('l');
  });

  it('should calc cursorLeft correct - cursor start of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorLeft(rendererState);
    expect(result).toBe(undefined);
  });

  it('should calc cursorRight correct - cursor middle of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 2;
    const result = shell.cursorRight(rendererState);
    expect(result).toBe('r');
  });

  it('should calc cursorRight correct - cursor end of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.cursorRight(rendererState);
    expect(result).toBe(undefined);
  });

  it('should calc cursorToStartOfLine correct - cursor middle of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 2;
    const result = shell.cursorToStartOfLine(rendererState);
    expect(result).toEqual({command: 'll', steps: 2});
  });

  it('should calc cursorLeft correct - cursor start of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    const result = shell.cursorToStartOfLine(rendererState);
    expect(result).toEqual({command: '', steps: 0});
  });

  it('should calc cursorToEndOfLine correct - cursor middle of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length - 2;
    const result = shell.cursorToEndOfLine(rendererState);
    expect(result).toEqual({command: 'rr', steps: 2});
  });

  it('should calc cursorToEndOfLine correct - cursor end of command', () => {
    const firstLine = 'test';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.cursorToEndOfLine(rendererState);
    expect(result).toEqual({command: '', steps: 0});
  });

  it('should calc cursorToEndOfLine correct - cursor after end of command', () => {
    const firstLine = 'test   ';
    rendererState.currentInput = {input:firstLine,lines: [{line: rendererState.cursorPosition.line, lineText: firstLine}]};
    rendererState.cursorPosition.x += firstLine.length;
    const result = shell.cursorToEndOfLine(rendererState);
    expect(result).toEqual({command: '', steps: 0});
  });
});
