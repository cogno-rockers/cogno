import {CommandDictionary} from './command.dictionary';
import {Hash} from '../../../../../+shared/models/hash/hash';
import {CommandNormalizer} from '../normalizer/command-normalizer';
import {ShellType} from '../../../../../../../shared/models/models';
import {MatchShellType, NormalizedSuggestResult} from '../models';
import {Normalizer} from '../../../../../../../shared/normalizer';
import {TestClock, TestHelper} from '../../../../../../../test/helper';
import {ShellLocation} from '../../../../../common/shellLocation';
import {SuggestionType} from '../../suggester';
import {SuggestRequestBuilder} from '../../suggestRequestBuilder';
import {CommandSource} from '../../../../../+shared/models/command';


describe('CommandDictionary', () => {
  let dictionary: CommandDictionary;
  let clock: TestClock;
  let commandNormalizer: CommandNormalizer;
  let requestBuilder: SuggestRequestBuilder;

  beforeEach(() => {
    clock = new TestClock();
    dictionary = new CommandDictionary(new ShellLocation('', ShellType.Bash));
    dictionary.clock = clock;
    const normalizer = new Normalizer(new ShellLocation('', ShellType.Bash));
    commandNormalizer = new CommandNormalizer(normalizer);
    requestBuilder = new SuggestRequestBuilder(normalizer);
  });

  it('should work with invalid input', () => {
    clock.clock = new Date();
    expect(() => dictionary.saveExecution(null)).not.toThrow();
    expect(() => dictionary.saveExecution(undefined)).not.toThrow();
  });

  it('should work with start string', () => {
    clock.clock = new Date();
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'test', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'te', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([
      {
        shellCommand: 'test',
        commandHash: Hash.create('test'),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        commandSource: CommandSource.TERMINAL,
        globalSelectionDate: undefined,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      }]);
  });

  it('should work with middle string', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'test', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'est', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([
      {
        shellCommand: 'test',
        commandHash: Hash.create('test'),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        globalSelectionDate: undefined,
        commandSource: CommandSource.TERMINAL,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      }]);
  });

  it('should work with many', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'test a', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'a', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([{
      shellCommand: 'test a',
      commandHash: Hash.create('test a'),
      globalExecutionCount: 1,
      globalSelectionCount: 0,
      globalExecutionDate: clock.now(),
      globalSelectionDate: undefined,
      commandSource: CommandSource.TERMINAL,
      matchShellType: MatchShellType.Yes,
      suggestionType: SuggestionType.GlobalCommand
    }]);
  });

  it('should only return docs with matches', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'test a', [ShellType.Bash]));
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'test b', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'test a', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([{
      shellCommand: 'test a',
      commandHash: Hash.create('test a'),
      globalExecutionCount: 1,
      globalSelectionCount: 0,
      globalExecutionDate: clock.now(),
      globalSelectionDate: undefined,
      commandSource: CommandSource.TERMINAL,
      matchShellType: MatchShellType.Yes,
      suggestionType: SuggestionType.GlobalCommand
    }]);
  });

  it('should work with many matches', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'git commit', [ShellType.Bash]));
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'git -c whatever', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git c', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([
      {
        shellCommand: 'git commit',
        commandHash: Hash.create('git commit'),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        globalSelectionDate: undefined,
        commandSource: CommandSource.TERMINAL,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      },
      {
        shellCommand: 'git -c whatever',
        commandHash: Hash.create('git -c whatever'),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        globalSelectionDate: undefined,
        commandSource: CommandSource.TERMINAL,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      }
    ]);
  });

  it('should increment execution number', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'git commit', [ShellType.Bash]));
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'git commit', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git c', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([
      {
        shellCommand: 'git commit',
        commandHash: Hash.create('git commit'),
        globalExecutionCount: 2,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        globalSelectionDate: undefined,
        commandSource: CommandSource.TERMINAL,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      }
    ]);
  });

  it('should add if command is extended', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'git commit -a', [ShellType.Bash]));
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'git commit', [ShellType.Bash]));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git c', ShellType.Bash));
    dictionary.apply(data, result);
    expect(result.suggestions.length).toEqual(2);
  });

  it('should ignore navigation', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'cd test', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'cd', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions.length).toEqual(0);
  });

  it('should work with pipe', () => {
    dictionary.saveExecution(...commandNormalizer.normalize([''], 'cat filename | less', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'c f l', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([
      {
        shellCommand: 'cat filename | less',
        commandHash: Hash.create('cat filename | less'),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        globalSelectionDate: undefined,
        commandSource: CommandSource.TERMINAL,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      }
    ]);
  });

  it('should work after restore', () => {
    const data1 = commandNormalizer.normalize([''], 'git commit', [ShellType.Bash])[0];
    const data2 = commandNormalizer.normalize([''], 'git status', [ShellType.Bash])[0];
    const data3 = commandNormalizer.normalize([''], 'ls', [ShellType.Bash])[0];
    dictionary.restore({commandHash: data3.commandHash, command:data3.command, executionCount: 1, selectionCount: 0, lastExecutionDate: clock.now(), shellTypes: [ShellType.Bash], commandSource: CommandSource.TERMINAL});
    dictionary.restore({commandHash: data1.commandHash, command:data1.command, executionCount: 1, selectionCount: 0, lastExecutionDate: clock.now(), shellTypes: [ShellType.Bash], commandSource: CommandSource.TERMINAL});
    dictionary.restore({commandHash: data2.commandHash, command:data2.command, executionCount: 1, selectionCount: 0, lastExecutionDate: clock.now(), shellTypes: [ShellType.Bash], commandSource: CommandSource.TERMINAL});
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git ', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([
      {
        shellCommand: 'git commit',
        commandHash: Hash.create('git commit'),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        globalSelectionDate: undefined,
        commandSource: CommandSource.TERMINAL,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      },
      {
        shellCommand: 'git status',
        commandHash: Hash.create('git status'),
        globalExecutionCount: 1,
        globalSelectionCount: 0,
        globalExecutionDate: clock.now(),
        globalSelectionDate: undefined,
        commandSource: CommandSource.TERMINAL,
        matchShellType: MatchShellType.Yes,
        suggestionType: SuggestionType.GlobalCommand
      }
    ]);
  });

  it('should work after restore with null or undefined', () => {
    dictionary.restore({commandHash: null, command: null, executionCount: 1, selectionCount: 0, lastExecutionDate: clock.now(), shellTypes: [ShellType.Bash], commandSource: CommandSource.TERMINAL});
    dictionary.restore({commandHash: undefined, command: undefined, executionCount: 1, selectionCount: 0, lastExecutionDate: clock.now(), shellTypes: [ShellType.Bash], commandSource: CommandSource.TERMINAL});
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git c', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.suggestions).toEqual([]);
  });

  it('should set min and max date', () => {
    const data1 = commandNormalizer.normalize([''], 'git commit', [ShellType.Bash])[0];
    const data2 = commandNormalizer.normalize([''], 'git status', [ShellType.Bash])[0];
    dictionary.restore({commandHash: data1.commandHash, command:data1.command, executionCount: 1, selectionCount: 1, lastExecutionDate: new Date(1), lastSelectionDate: new Date(2), shellTypes: [], commandSource: CommandSource.TERMINAL});
    dictionary.restore({commandHash: data2.commandHash, command:data2.command, executionCount: 1, selectionCount: 1, lastExecutionDate: new Date(1000), lastSelectionDate: new Date(2000),  shellTypes: [], commandSource: CommandSource.TERMINAL});
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git', ShellType.Bash));
    const result: NormalizedSuggestResult = {suggestions: [], scoringValues: {}};
    dictionary.apply(data, result);
    expect(result.scoringValues.maxGlobalExecutionDate).toEqual(new Date(1000));
    expect(result.scoringValues.maxGlobalSelectionDate).toEqual(new Date(2000));
    expect(result.scoringValues.minGlobalExecutionDate).toEqual(new Date(1));
    expect(result.scoringValues.minGlobalSelectionDate).toEqual(new Date(2));
  });
});
