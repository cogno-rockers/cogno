import {AfterViewInit, Component, ElementRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {AsyncPipe, JsonPipe, NgIf, NgSwitchCase} from '@angular/common';
import {LocalComponent} from '../../general/edit-shell-dialog/local/local.component';
import {RemoteComponent} from '../../general/edit-shell-dialog/remote/remote.component';
import {Observable} from 'rxjs';
import {CommandsSettingsService, TableEntry} from '../+state/commands.settings.service';
import {CopyEditDeleteComponent} from '../../../+shared/components/copy-edit-delete/copy-edit-delete.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ParameterTableComponent} from '../parameter-table/parameter-table.component';

@Component({
    selector: 'app-edit-command',
    imports: [
        AsyncPipe,
        ReactiveFormsModule,
        FormsModule,
        ParameterTableComponent
    ],
    templateUrl: './edit-command.component.html',
    styleUrl: './edit-command.component.scss',
    encapsulation: ViewEncapsulation.None
})
export class EditCommandComponent implements AfterViewInit{
  isDialogValid: Observable<boolean>;
  command: Observable<TableEntry>;

  @ViewChild('commandInput')
  private commandInput: ElementRef;

  constructor(private service: CommandsSettingsService) {
    this.isDialogValid = this.service.selectIsValid();
    this.command = this.service.selectCommandToEdit();

  }

  ngAfterViewInit(): void {
    setTimeout(() => this.commandInput.nativeElement.focus());
  }

  closeDialog() {
    this.service.setShowEditCommand(false);
  }

  async saveAndCloseDialog() {
    await this.service.saveCommandToEdit();
    this.service.setShowEditCommand(false);
  }

  updateCommand(event: KeyboardEvent) {
    this.service.updateCommandString((event.target as any).value);
  }
}
