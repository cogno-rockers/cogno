export namespace Tokenizer {

  const SPLIT_ARGS_REGEX = /("[^"\\]*(?:\\[\S\s][^"\\]*)*"|'[^'\\]*(?:\\[\S\s][^'\\]*)*'|\/[^\/\\]*(?:\\[\S\s][^\/\\]*)*\/[gimy]*(?=\s|$)|(?:\\\s|\S)+)/g;

  export function tokenize(input: string): string[] {
    if (!input) {
      return [];
    }
    const pres = input.match(SPLIT_ARGS_REGEX);
    const tokens = [];
    for (const token of pres) {
      if (token.indexOf('=') > -1) {
        const split = token.split('=', 2);
        tokens.push(`${split[0]}=`);
        if (split[1].length > 0) { tokens.push(split[1]); }
      } else {
        tokens.push(token);
      }
    }
    return tokens;
  }
}
