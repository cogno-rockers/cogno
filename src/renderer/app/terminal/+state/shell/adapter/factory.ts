import {ShellConfig} from '../../../../../../shared/models/settings';
import {ShellType} from '../../../../../../shared/models/models';
import {ShellAdapter} from './shell.adapter';
import {BashAdapter} from './bash/bash.adapter';
import {PowershellAdapter} from './powershell/powershell.adapter';
import {ZshAdapter} from './zsh/zsh.adapter';


export namespace ShellAdapterFactory {
  export function create(config: ShellConfig): ShellAdapter {
    switch (config.type) {
      case ShellType.Bash:
        return new BashAdapter(config);
      case ShellType.GitBash:
        return new BashAdapter(config);
      case ShellType.Powershell:
        return new PowershellAdapter(config);
      case ShellType.ZSH:
        return new ZshAdapter(config);
      default:
        throw new Error(`Unsupported shell ${config.type}`);
    }
  }
}
