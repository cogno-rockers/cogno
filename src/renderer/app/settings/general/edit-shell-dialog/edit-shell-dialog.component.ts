import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {GeneralSettingsService} from '../+state/general.settings.service';
import {Observable} from 'rxjs';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {LocalComponent} from './local/local.component';
import {RemoteComponent} from './remote/remote.component';

@Component({
    selector: 'app-edit-shell-dialog',
    templateUrl: './edit-shell-dialog.component.html',
    styleUrls: ['./edit-shell-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        FormsModule,
        LocalComponent,
        RemoteComponent
    ]
})
export class EditShellDialogComponent implements OnInit {

  isDialogValid: Observable<boolean>;
  showRemoteShellDialog: Observable<boolean>;
  showRemoteShellSelection: Observable<boolean>;
  command: Observable<boolean>;

  constructor(private generalSettingsService: GeneralSettingsService) {
    this.isDialogValid = generalSettingsService.selectIsDialogValid();
    this.showRemoteShellDialog = generalSettingsService.selectShowRemoteShellDialog();
    this.showRemoteShellSelection = generalSettingsService.selectShowRemoteShellSelection();
  }

  ngOnInit(): void {
  }
  closeDialog(){
    this.generalSettingsService.closeEditShellDialog();
  }

  saveAndCloseDialog(){
    this.generalSettingsService.saveSelectedShell();
    this.closeDialog();
  }

  toggleShowRemoteShellDialog(event) {
    event.stopPropagation();
    event.preventDefault();
    this.generalSettingsService.toggleShowRemoteShellDialog();
  }
}
