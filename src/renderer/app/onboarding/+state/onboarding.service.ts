import {Injectable} from '@angular/core';
import {Observable, zip} from 'rxjs';
import {filter, first, map} from 'rxjs/operators';
import {Settings, ShellConfig} from '../../../../shared/models/settings';
import {Logger} from '../../logger/logger';
import {IpcChannel} from '../../../../shared/ipc.chanels';
import {createStore, Store} from '../../common/store/store';
import {ShellsLoaderService} from '../../+shared/services/shells/shells-loader.service';
import {FontsLoaderService} from '../../+shared/services/fonts/fonts-loader.service';
import {ElectronService} from '../../+shared/services/electron/electron.service';
import {SettingsService} from '../../+shared/services/settings/settings.service';
import { upsertArray } from '../../common/store/store';
import { InjectionType } from '../../../../shared/models/models';

export interface State {
  site: Site;
  settings: Settings;
  fonts: Font[];
}

export interface Font {
  name: string;
  isSelected: boolean;
}

export enum Site {
  Welcome = 1,
  Shells = 2,
  DefaultShell = 3,
  Theme = 4,
  Font = 5,
  Telemetry = 6,
  By = 7
}


const initialState: State = {
  site: 1,
  settings: null,
  fonts: []
};

@Injectable()
export class OnboardingService {

  private store: Store<State> = createStore('onboarding', initialState);
  constructor(
    private shellsLoader: ShellsLoaderService,
    private fontsLoader: FontsLoaderService,
    private electronService: ElectronService,
    private settingsService: SettingsService
  ) {
  }

  loadData(): Observable<any> {
    return zip(
      this.shellsLoader.loadShells(),
      this.fontsLoader.loadFonts(),
      this.settingsService.selectSettings().pipe(filter(s => !!s), first())
    ).pipe(map(([shells, fontFamilies, settings]) => {
      if (shells.length > 0) {
        shells[0].default = true;
      }
      const fonts: Font[] = fontFamilies.map(f => ({name: f, isSelected: false}));
      if (fonts.length > 0) {
        fonts[0].isSelected = true;
      }
      const newSettings = JSON.parse(JSON.stringify(settings));
      newSettings.shells = shells;
      this.store.update(s => ({settings: newSettings, fonts: fonts, site: Site.Welcome}));
    })).pipe(first());
  }

  incrementSite() {
    this.store.update(s => {
      let newSite = ++s.site;
      newSite = this.proofAndSkipSite(newSite, s, '+');
      newSite = newSite > Site.By ? Site.By : newSite;
      return {site: newSite};
    });
  }

  decrementSite() {
    this.store.update(s => {
      let newSite = --s.site;
      newSite = this.proofAndSkipSite(newSite, s, '-');
      newSite = newSite < 1 ? 1 : newSite;
      return {site: newSite};
    });
  }

  proofAndSkipSite(site: number, s: State, direction: '+' | '-'): number {
    switch (direction){
      case '+': {
        if (site === Site.Shells && s.settings.shells.length === 1) {
          site = site + 1;
        }
        if (site === Site.DefaultShell && s.settings.shells.length === 1) {
          site = site + 1;
        }
        if (site === Site.Font && s.fonts.length === 1) {
          site = site + 1;
        }
        break;
      }
      case '-': {
        if (site === Site.Font && s.fonts.length === 1) {
          site = site - 1;
        }
        if (site === Site.DefaultShell && s.settings.shells.length === 1) {
          site = site - 1;
        }
        if (site === Site.Shells && s.settings.shells.length === 1) {
          site = site - 1;
        }
        break;
      }
    }

    return site;
  }

  selectHasPreviousSite(): Observable<boolean> {
    return this.selectSite().pipe(map(s => s > 1));
  }

  selectSite(): Observable<number> {
    return this.store.select(s => s.site);
  }

  selectEnableTelemetry(): Observable<boolean> {
    return this.store.select(s => s.settings.general.enableTelemetry);
  }

  selectFonts(): Observable<Font[]> {
    return this.store.select(s => s.fonts);
  }

  selectHasNextSite(): Observable<boolean> {
    return this.store.select(s => s).pipe(map(s => s.site < Site.By));
  }

  selectShells(): Observable<ShellConfig[]> {
    return this.store.select(s => s.settings?.shells || []);
  }

  toggleEnableTelemetry() {
    this.store.update(s => ({
      ...s,
      settings: {...s.settings, general: {...s.settings.general, enableTelemetry: !s.settings.general.enableTelemetry}}
    }));
  }

  toggleShell(name: string) {
    const shells = [...this.store.get(s => s.settings.shells)];
    if (shells.length === 0) {
      return;
    }
    const shell = {...shells.find(s => s.name === name)};
    if (shell.isSelected && shells.filter(s => s.isSelected).length <= 1) {
      return;
    }
    shell.isSelected = !shell.isSelected;
    shell.default = shell.default && shell.isSelected;
    const newShells = upsertArray('name', shells, shell);
    if (!newShells.find(s => s.default && s.isSelected)) {
      newShells.find(s => s.isSelected).default = true;
    }
    this.store.update(s => ({...s, settings: {...s.settings, shells: newShells}}));
  }

  toggleDefaultShell(name: string) {
    const shells = [...this.store.get(s => s.settings.shells)];
    shells.forEach(s => s.default = false);
    const shell = shells.find(s => s.name === name);
    shell.default = true;
    this.store.update(s => ({...s, settings: {...s.settings, shells}}));
  }

  selectThemes() {
    return this.store.select(s => s.settings?.themes || []);
  }

  toggleTheme(name: string) {
    const themes = [...this.store.get(s => s.settings.themes)];
    themes.forEach(s => s.isDefault = false);
    const t = themes.find(s => s.name === name);
    t.isDefault = true;
    this.store.update(s => ({...s, settings: {...s.settings, themes}}));
  }

  toggleFont(name: string) {
    const fonts = [...this.store.get(s => s.fonts)];
    fonts.forEach(s => s.isSelected = false);
    const t = fonts.find(s => s.name === name);
    t.isSelected = true;
    this.store.update(s => ({...s, fonts: fonts}));
  }

  finish() {
    const font = this.store.get(s => s.fonts?.find(f => f.isSelected) || null);
    const settings = this.store.get(s => s.settings);
    settings.themes.forEach(t => t.fontFamily = `${font.name}`);
    settings.shells = settings.shells.filter(s => s.isSelected);
    Logger.debug('Save Settings', settings);
    this.electronService.send(IpcChannel.SaveSettings, settings);
  }

  openDocumentation(page: string) {
    this.electronService.openExternal(`https://gitlab.com/cogno-rockers/cogno/-/wikis/${page}`);
  }

  setInjectionType(type: InjectionType) {
    const shells = [...this.store.get(s => s.settings.shells)];
    shells.forEach(s => s.injectionType = type);
    this.store.update(s => ({...s, settings: {...s.settings, shells}}));
  }
}
