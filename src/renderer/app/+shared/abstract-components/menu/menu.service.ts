import {GlobalMenuService} from './+state/global-menu.service';
import {Observable} from 'rxjs';
import {DefaultMenuType, MenuType} from './menuType';
import {map} from 'rxjs/operators';
import {Inject} from '@angular/core';

export abstract class MenuService {

  constructor(
    @Inject(DefaultMenuType) protected menuType: MenuType,
    protected globalMenuService: GlobalMenuService) {
  }

  selectIsActive(): Observable<boolean> {
    return this.globalMenuService.selectCurrentActiveMenu().pipe(
      map((currentMenu) => currentMenu === this.menuType)
    );
  }

  getIsActive(): boolean {
    return this.globalMenuService.getCurrentActiveMenu() === this.menuType;
  }

  closeMenu(): void {
    this.globalMenuService.updateCurrentActiveMenu('AllClosed');
  }

  getCurrentActiveMenu(): MenuType {
    return this.globalMenuService.getCurrentActiveMenu();
  }

  isOpened(): boolean {
    return this.getCurrentActiveMenu() === this.menuType;
  }

  openMenu() {
    if (this.getCurrentActiveMenu() === this.menuType) {
      this.closeMenu();
    }
    this.globalMenuService.updateCurrentActiveMenu(this.menuType);
  }
}
