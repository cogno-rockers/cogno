import {NormalizedCommand, NormalizedSuggestResult, SuggestionCalculation} from '../models';
import {Tokenizer} from '../../tokenizer';
import {ShellType} from '../../../../../../../shared/models/models';
import {ShellLocation} from '../../../../../common/shellLocation';
import {SuggestionType, SuggestRequest} from '../../suggester';
import {RegexpHelper} from '../../../../../common/regexp';

export class ArgumentDictionary {
  private commands: Map<string, Command> = new Map<string, Command>();

  constructor(private location: ShellLocation) {
  }

  public add(...commands: NormalizedCommand[]) {
    for (const command of commands) {
      if (!command.commandHash || !command.tokens || command.tokens.length <= 1 || command.isCombinedCommand) {
        continue;
      }
      this._add(command.tokens[0], command.tokens.slice(1, command.tokens.length), 1, command.shellTypes);
    }
  }

  public apply(suggestRequest: SuggestRequest, searchResult: NormalizedSuggestResult) {
    if (!suggestRequest) { return; }
    const reducedSuggestions = new Map<string, {suggestions: SuggestionCalculation[]; placeholderCommand?: string}>();
    for (const suggestion of searchResult.suggestions) {
      if (!suggestion) {
        continue;
      }
      if (!suggestion.shellCommand) { // this is a cd command
        reducedSuggestions.set(Math.random().toString(), {suggestions: [suggestion]});
        continue;
      }
      if (suggestion.shellCommand.match(new RegExp(RegexpHelper.RegexCommandPlaceholder))){ // this is a placeholder suggestion
        const key = suggestion.shellCommand.replace(new RegExp(RegexpHelper.RegexCommandPlaceholder, 'g'), '{{?}}');
        reducedSuggestions.set(key, {suggestions: [suggestion], placeholderCommand: suggestion.shellCommand});
        continue;
      }
      const tokens = Tokenizer.tokenize(suggestion.shellCommand);
      if (!tokens || tokens.length <= 0) { continue; }
      let key = suggestion.shellCommand;
      const commandName = tokens[0];
      if (this.commands.has(commandName)) {
        const command = this.commands.get(commandName);
        for (let argumentIndex = 1; argumentIndex < tokens.length; argumentIndex++) {
          if (command.isArgumentUsedOnce(tokens[argumentIndex])) {
            const argToReplace = tokens[argumentIndex].replace(/"/g, '');
            key = key.replace(argToReplace, '{{?}}');
          }
        }
        if (!reducedSuggestions.has(key)) {
          reducedSuggestions.set(key, {suggestions: [], placeholderCommand: key});
        }
      }
      if (!reducedSuggestions.has(key)) {
        reducedSuggestions.set(key, {suggestions: []});
      }
      reducedSuggestions.get(key).suggestions.push(suggestion);
    }
    const suggestions: SuggestionCalculation[] = [];
    for (const key of reducedSuggestions.keys()) {
      if (reducedSuggestions.get(key).suggestions.length < 3) {
        suggestions.push(...reducedSuggestions.get(key).suggestions);
      } else {
        const reduced: {
          executionCount?: number,
          selectionCount?: number,
          globalExecutionCount?: number,
          suggestionType?: SuggestionType,
          executionDate?: Date,
          globalExecutionDate?: Date
        } = reducedSuggestions.get(key).suggestions.reduce((pre, cur) => {
          pre.globalExecutionDate = cur.globalExecutionDate > pre.globalExecutionDate ? cur.globalExecutionDate : pre.globalExecutionDate;
          pre.executionDate = cur.executionDate > pre.executionDate ? cur.executionDate : pre.executionDate;
          pre.executionCount += cur.executionCount || 0;
          pre.selectionCount += cur.selectionCount || 0;
          pre.globalExecutionCount += cur.globalExecutionCount || 0;
          pre.suggestionType = cur.suggestionType === SuggestionType.LocalCommand ? SuggestionType.LocalCommand : pre.suggestionType;
          return pre;
        }, {
          globalExecutionDate: new Date(0),
          executionDate: new Date(0),
          executionCount: 0,
          selectionCount: 0,
          globalExecutionCount: 0,
          suggestionType: SuggestionType.GlobalCommand,
        });

        const data = reducedSuggestions.get(key);
        const suggestion = data.suggestions[0];
        suggestion.shellCommand = data.placeholderCommand || suggestion.shellCommand;
        suggestion.executionCount = reduced.executionCount;
        suggestion.selectionCount = reduced.selectionCount;
        suggestion.globalExecutionCount = reduced.globalExecutionCount;
        suggestion.suggestionType = reduced.suggestionType;
        suggestion.executionDate = reduced.executionDate;
        suggestion.globalExecutionDate = reduced.globalExecutionDate;
        suggestions.push(suggestion);
      }
    }
    searchResult.suggestions = suggestions;
  }

  restore(tokens: string[], executionCount: number, shellTypes: ShellType[]) {
    if (!tokens || tokens.length <= 1) {
      return;
    }
    this._add(tokens[0], tokens.slice(1, tokens.length), executionCount, shellTypes);
  }

  private _add(command: string, args: string[], executionCount: number, shellTypes: ShellType[]) {
    if (!this.commands.has(command)) {
      this.commands.set(command, new Command());
    }
    const c = this.commands.get(command);
    for (const arg of args) {
      c.incrementArgumentUsage(arg.trim(), executionCount);
    }
    c.addShellTypes(shellTypes);
  }
}

class Command {
  private argsUsageCount: Map<string, number> = new Map<string, number>();
  private shellTypes: ShellType[] = [];

  public isArgumentUsedOnce(arg: string): boolean {
    return (this.argsUsageCount.get(arg) || 0) <= 1;
  }

  public incrementArgumentUsage(arg: string, executionCount: number) {
    if(!this.argsUsageCount.has(arg)) {
      this.argsUsageCount.set(arg, 0);
    }
    const currentCount = this.argsUsageCount.get(arg) || 0;
    const count = executionCount || 0;
    this.argsUsageCount.set(arg, currentCount + count);
  }

  public addShellTypes(shellTypes: ShellType[]) {
    this.shellTypes = this.shellTypes.concat(shellTypes.filter((item) => this.shellTypes.indexOf(item) < 0));
  }
}
