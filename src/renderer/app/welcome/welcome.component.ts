import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {WelcomeService} from './+state/welcome.service';
import {KeyboardService} from '../+shared/services/keyboard/keyboard.service';
import {SettingsService} from '../+shared/services/settings/settings.service';
import {Shortcuts} from '../../../shared/models/shortcuts';
import {CommonModule} from '@angular/common';
import {LogoComponent} from '../+shared/components/logo/logo.component';
import {IconComponent} from '../+shared/components/icon/icon.component';
import {ShortcutPipe} from '../+shared/pipes/shortcut/shortcut.pipe';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.scss'],
    providers: [WelcomeService],
    imports: [
        CommonModule,
        LogoComponent,
        IconComponent,
        ShortcutPipe
    ]
})
export class WelcomeComponent implements OnInit, OnDestroy {

  public hasBackgroundImage: Observable<boolean>;
  public shortcuts: Observable<Shortcuts>;

  constructor(private service: WelcomeService, private readonly shortcutsService: KeyboardService, private readonly settingsService: SettingsService) { }

  ngOnDestroy(): void {
     this.shortcutsService.deactivateGlobalShortcuts();
  }

  ngOnInit(): void {
    this.shortcuts = this.shortcutsService.selectShortcuts().pipe(filter(t => !!t));
    this.hasBackgroundImage = this.settingsService.selectActiveTheme().pipe(map(t =>  t.image && t.isImageAvailable));
    this.shortcutsService.activateGlobalShortcuts();
  }

  openTerminal() {
    this.service.openTerminal();
  }

  openSettings() {
    this.service.openSettings();
  }

  openGit() {
    this.service.openGit();
  }

  openReportIssue() {
    this.service.openReportIssue();
  }
}
