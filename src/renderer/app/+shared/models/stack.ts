export class Stack<T> {
  private items: T[] = [];

  push(item: T) {
    this.items.push(item);
  }

  pop(): T {
    return this.items.pop();
  }

  top(): T {
    return this.items.length > 0 ? this.items[this.items.length - 1] : null;
  }

  last(): T {
    return this.items.length > 0 ? this.items[0] : null;
  }

  size() {
    return this.items.length;
  }

  // Clear all elements from the queue
  clear(): void {
    this.items = [];
  }

  filter(predicate: (s: T) => boolean): T[] {
    return this.items.filter(predicate);
  }
}
