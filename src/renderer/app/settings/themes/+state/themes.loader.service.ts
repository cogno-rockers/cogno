import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {Theme} from '../../../../../shared/models/settings';
import {TerminalSexyThemeParser} from './terminal.sexy.theme.parser';
import {Logger} from '../../../logger/logger';

@Injectable()
export class ThemesLoaderService {
  constructor(private electron: ElectronService) {
  }

  loadThemes(): Observable<Theme[]> {
    return from(this.electron.httpGet('terminal.sexy', 443, '/schemes/index.json?v=0.4.0')).pipe(
      map(result => {
        try {
          const json = new TextDecoder().decode(result);
          const themes = TerminalSexyThemeParser.createAll(JSON.parse(json));
          return themes;
        } catch (e) {
          Logger.error('Could not parse themes.', e);
          throw e;
        }
      }));
  }

  loadThemeDetails(id: string, name: string, defaultTheme: Theme): Observable<Theme> {
    return from(this.electron.httpGet('terminal.sexy', 443, encodeURIComponent(`/schemes/${id}.json`))).pipe(
      map(result => {
        try {
          const json = new TextDecoder().decode(result);
          const theme = TerminalSexyThemeParser.parse(json, name, id, defaultTheme);
          return theme;
        } catch (e) {
          Logger.error('Could not parse theme.', e);
          throw e;
        }
      })
    );
  }
}
