import {Component, Input, OnInit} from '@angular/core';
import {CommandsSettingsService, Parameter, TableEntry} from '../+state/commands.settings.service';
import {AsyncPipe, JsonPipe, NgForOf, NgIf} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-parameter-table',
    imports: [
        FormsModule,
        NgForOf,
        AsyncPipe
    ],
    templateUrl: './parameter-table.component.html',
    styleUrl: './parameter-table.component.scss'
})
export class ParameterTableComponent implements OnInit {

  plugins: Observable<{label: string; placeholder: string}[]>;
  command: Observable<TableEntry>;

  constructor(private service: CommandsSettingsService) {
  }

  ngOnInit(): void {
    this.plugins = this.service.selectPlugins();
    this.command = this.service.selectCommandToEdit();
  }


  setPlaceholder(param: Parameter, event: Event) {
    this.service.updatePlaceholder(param, (event.target as any).value);
  }

  toggleVariableParameter(param: Parameter) {
    this.service.toggleVariableParameter(param);
  }
}
