import {IDbContext} from '../db/db-context';
import {ShellLocation} from '../../../common/shellLocation';
import {MigrationV4ToV5} from './migrationV4ToV5';
import {MigrationV5ToV6} from './migrationV5ToV6';

export namespace Migration {


  export async function migrate(db: IDbContext, location: ShellLocation): Promise<unknown> {
    if (global.__unittest__) {
      return Promise.resolve(true);
    }
    await MigrationV4ToV5.migrate(location, db);
    return await MigrationV5ToV6.migrate(location, db);
  }
}

