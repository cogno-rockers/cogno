import { Injectable } from '@angular/core';
import { KeyboardService } from './keyboard.service';
import { Key } from '../../../common/key';
import { fromEvent } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class KeytipService {

  public static allowedChars = [...'1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'];
  public static keytipAttributeName = 'data-keytip';
  public static containerAttributeName = 'data-keytip-container';
  public static depthAttributeName = 'data-keytip-depth';
  public static hasChildAttributeName = 'data-keytip-parent';

  private keytipElements: Map<string, {
    keytipElement: HTMLElement;
    actionElement: HTMLElement;
    depth: number;
    hasChilds: boolean
  }> = new Map();

  private currentDepth: number | undefined;
  private mode: 'global' | 'context' | undefined;

  constructor(private keyboardService: KeyboardService) {
    this.keyboardService.onShortcut('showKeytips').subscribe(() => {
      this.mode = 'global';
      this.scanAndDecorate();
      this.disableOnMouseMove();
      this.currentDepth = 1;
    });
    this.keyboardService.onShortcut('showContextKeytips').subscribe(() => {
      this.mode = 'context';
      this.scanAndDecorate();
      this.disableOnMouseMove();
      this.currentDepth = 1;
    });
  }

  private disableOnMouseMove(): void {
    fromEvent<MouseEvent>(window, 'mousemove')
      .pipe(first())
      .subscribe(e => {
        this.clearAll();
      });
  }

  public handleKeytip(e: KeyboardEvent): boolean {
    if (!this.currentDepth || Key.isModifierKeyPressed(e)) {
      return false;
    }
    if (e.key === Key.Escape) {
      this.clearAll();
      return true;
    }
    const key = e.code.replace('Digit', '').replace('Key', '');
    if (this.keytipElements.has(key)) {
      this.keytipElements.get(key).actionElement.click();
      if (this.keytipElements.get(key).hasChilds) {
        this.currentDepth = this.keytipElements.get(key).depth + 1;
        this.scanAndDecorate();
      } else {
        this.clearAll();
      }
    }
    return true;
  }

  private clearAll() {
    this.cleanupKeytips();
    setTimeout(() => {
      this.currentDepth = undefined;
      this.mode = undefined;
    });
  }

  /**
   * Scans the document and decorates visible elements with keytips.
   */
  private scanAndDecorate(): void {
    setTimeout(() => {
      // Clean up existing keytips to avoid duplicates
      this.cleanupKeytips();
      // Find all elements with the data-keytip attribute
      const elements = this.getKeytipElements();
      elements.forEach((element) => {
        const htmlElement = element as HTMLElement;

        // Check if the element is visible
        if (this.shouldDisplayKeytipForElement(htmlElement)) {
          // Add the keytip to the element
          this.addKeytip(htmlElement);
        }
      });
    }, this.currentDepth === 1 ? 0 : 100);
  }

  private getKeytipElements(): Element[] {
    if (!this.mode) {
      return [];
    }
    if (this.mode === 'global') {
      const keytipElements = document.querySelectorAll(`[${KeytipService.keytipAttributeName}]`);
      const found = Array.from(keytipElements).filter(element => {
        return !element.closest(`[${KeytipService.containerAttributeName}]`);
      });

      return found;
    }
    if (this.mode === 'context') {
      const focusedElement = document.activeElement;
      const contextElement = focusedElement.closest(`[${KeytipService.containerAttributeName}]`);
      if (!contextElement) {
        return [];
      }
      return Array.from(contextElement.querySelectorAll(`[${KeytipService.keytipAttributeName}]`));
    }
  }

  /**
   * Checks if an element is visible in the DOM and has the current depth.
   * @param element The HTML element to check.
   * @returns True if the element is visible; otherwise, false.
   */
  private shouldDisplayKeytipForElement(element: HTMLElement): boolean {
    const style = window.getComputedStyle(element);
    const isDisplayed = style.display !== 'none';
    const isVisible = style.visibility !== 'hidden';
    const keytipOrder = +(element.getAttribute(`${KeytipService.depthAttributeName}`) || 1);
    return isDisplayed && isVisible && keytipOrder === this.currentDepth;
  }

  /**
   * Adds a keytip to the specified element.
   * @param element The element to which the keytip is added.
   */
  private addKeytip(element: HTMLElement): void {
    const text = element.getAttribute(`${KeytipService.keytipAttributeName}`) || '';
    const depth = +(element.getAttribute(`${KeytipService.depthAttributeName}`) || 1);
    const hasChilds = element.hasAttribute(`${KeytipService.hasChildAttributeName}`);

    const char = this.getNextChar(text);

    if (!char) { return; }

    // Create the keytip element
    const keytip = document.createElement('div');
    keytip.classList.add('keytip');

    // Set the keytip text from the data attribute
    keytip.innerText = char;

    // Style the keytip
    this.setStyleForKeytip(keytip);

    // Position the keytip relative to the element
    this.positionKeytip(element, keytip);

    // Append the keytip to the body
    document.body.appendChild(keytip);

    // Keep a reference for cleanup
    this.keytipElements.set(char, { keytipElement: keytip, actionElement: element, depth: depth, hasChilds: hasChilds });
  }

  getNextChar(wish: string): string | null {

    let index = KeytipService.allowedChars.indexOf(wish);

    // Check if the wish character is in the pool and not used
    if (index >= 0 && !this.keytipElements.has(wish)) {
      return wish;
    }

    // If the wish character is not in the pool, start from the beginning
    if (index === -1) {
      index = 0;
    }

    // Search for the next available character in the pool
    for (let i = 0; i < KeytipService.allowedChars.length; i++) {
      const nextIndex = (index + i) % KeytipService.allowedChars.length; // Wrap around to the beginning if needed
      const c = KeytipService.allowedChars[nextIndex];
      if (!this.keytipElements.has(c)) {
        return c;
      }
    }

    // All characters are used
    return null;
  }

  /**
   * Sets the style for the keytip element.
   * @param keytipElement The keytip element to style.
   */
  private setStyleForKeytip(keytipElement: HTMLElement): void {
    keytipElement.style.position = 'absolute';
    keytipElement.style.width = '1.1rem';
    keytipElement.style.height = '1.1rem';
    keytipElement.style.backgroundColor = 'var(--background-color-40l)';
    keytipElement.style.color = 'var(--foreground-color)';
    keytipElement.style.opacity = '.8';
    keytipElement.style.fontSize = '0.9rem';
    keytipElement.style.textAlign = 'center';
    keytipElement.style.lineHeight = '1.1rem';
    keytipElement.style.borderRadius = '4px';
    keytipElement.style.zIndex = '1000';
    keytipElement.style.pointerEvents = 'none';
    keytipElement.style.userSelect = 'none';
    keytipElement.setAttribute('aria-hidden', 'true');
  }

  /**
   * Positions the keytip element relative to the target element.
   * @param targetElement The target element.
   * @param keytipElement The keytip element to position.
   */
  private positionKeytip(targetElement: HTMLElement, keytipElement: HTMLElement): void {
    const rect = targetElement.getBoundingClientRect();
    const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    const top = rect.bottom + scrollTop - rect.height / 2;
    const left = rect.left + scrollLeft;

    // Adjust position to place the keytip at the top-left corner of the target element
    keytipElement.style.top = `${top}px`;
    keytipElement.style.left = `${left}px`;
  }

  /**
   * Cleans up existing keytip elements from the DOM.
   */
  private cleanupKeytips(): void {
    this.keytipElements.forEach((entry) => {
      if (document.body.contains(entry.keytipElement)) {
        document.body.removeChild(entry.keytipElement);
      }
    });
    this.keytipElements.clear();
  }

  isKeytip(event: KeyboardEvent) {
    return !!this.currentDepth;
  }
}
