import {GeneralConfig} from '../models/settings';
import {ParseResult} from './settings.parser';
import * as JSON5 from 'json5';
import {addErrors} from './parser';

export namespace GeneralParser {
  export function validateGeneralConfig(general: GeneralConfig, enrich = false): GeneralResult {
    try {
      const result = {isValid: true, generalConfig: general, hasWarnings: false};
      validate(result, enrich);
      return result;
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  export function parseGeneralConfig(generalAsString: string): GeneralResult {
    try {
      return validateGeneralConfig(JSON5.parse(generalAsString) as GeneralConfig);
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  function validate(result: GeneralResult, enrich = false) {
    if (enrich && !result.generalConfig) {
      result.generalConfig = {
        enableTelemetry: true,
        openTabInSameDirectory: true,
        enablePasteOnRightClick: false,
        enableCopyOnSelect: false,
        scrollbackLines: 100000
      };
    }
    addErrors(result, !result.generalConfig ? 'The "general" property is missing.' : null);
    if (enrich && result.generalConfig.enableTelemetry === undefined) {
      result.generalConfig.enableTelemetry = true;
    }
    addErrors(result, result.generalConfig.enableTelemetry === undefined ? 'The "enableTelemetry" property in "general" is missing.' : null);
    if (enrich && result.generalConfig.enablePasteOnRightClick === undefined) {
      result.generalConfig.enablePasteOnRightClick = false;
    }
    addErrors(result, result.generalConfig.enablePasteOnRightClick === undefined ? 'The "enablePasteOnRightClick" property in "general" is missing.' : null);
    if (enrich && result.generalConfig.enableCopyOnSelect === undefined) {
      result.generalConfig.enableCopyOnSelect = false;
    }
    addErrors(result, result.generalConfig.enableCopyOnSelect === undefined ? 'The "enableCopyOnSelect" property in "general" is missing.' : null);
    if (enrich && result.generalConfig.scrollbackLines === undefined) {
      result.generalConfig.scrollbackLines = 100000;
    }
    if (enrich && result.generalConfig.openTabInSameDirectory === undefined) {
      result.generalConfig.openTabInSameDirectory = true;
    }
    addErrors(result, result.generalConfig.openTabInSameDirectory === undefined ? 'The "openTabInSameDirectory" property in "general" is missing.' : null);

  }
}


export interface GeneralResult extends ParseResult {
  generalConfig?: GeneralConfig;
}
