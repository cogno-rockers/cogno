import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Shortcut} from '../../../../shared/models/shortcuts';
import {Observable} from 'rxjs';
import {ShortcutsService} from './+state/shortcuts.service';
import {SettingsTabService} from '../+state/settings-tab.service';
import {CommonModule} from '@angular/common';
import {ShortcutFilterPipe} from '../../+shared/pipes/shortcut-filter/shortcut-filter.pipe';
import {IconComponent} from '../../+shared/components/icon/icon.component';
import {ShortcutPipe} from '../../+shared/pipes/shortcut/shortcut.pipe';
import {EditShortcutComponent} from './edit-shortcut/edit-shortcut.component';
import {FormsModule} from '@angular/forms';
import {CopyEditDeleteComponent} from '../../+shared/components/copy-edit-delete/copy-edit-delete.component';

@Component({
    selector: 'app-shortcuts',
    templateUrl: './shortcuts.component.html',
    styleUrls: ['./shortcuts.component.scss'],
    providers: [ShortcutsService],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        FormsModule,
        ShortcutFilterPipe,
        IconComponent,
        ShortcutPipe,
        EditShortcutComponent,
        CopyEditDeleteComponent
    ]
})
export class ShortcutsComponent implements OnInit, AfterViewInit, OnDestroy {

  public shortcuts: Observable<Shortcut[]>;
  public duplicatedShortcuts: Observable<Shortcut[]>;
  public activeShortcut: Observable<Shortcut>;
  public showEditShortcut: Observable<boolean>;
  public search = '';
  @ViewChild('searchInput')
  private searchInput: ElementRef;

  constructor(private settingsService: SettingsTabService, private service: ShortcutsService) {

  }

  ngOnInit(): void {
    this.service.init(this.settingsService.getTabId() + 'shortcuts');
    this.shortcuts = this.service.selectShortcuts();
    this.duplicatedShortcuts = this.service.selectDuplicatedShortcuts();
    this.activeShortcut = this.service.selectActiveShortcut();
    this.showEditShortcut = this.service.selectShowEditShortcut();
  }

  ngOnDestroy(): void {
    this.service.destroy();
  }

  addAlias() {
    this.service.showEditShortcut({shortcut: '', shortcutKeys: [], command: '', label: '', key: undefined});
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.searchInput.nativeElement.focus());
  }

  handleEvent(shortcut: Shortcut, $event: 'edit' | 'delete') {
    switch ($event) {
      case 'edit':
        this.service.showEditShortcut(shortcut);
        break;
        case 'delete':
        this.service.deleteShortcut(shortcut);
          this.service.saveShortcuts();
        break;
    }
  }

  searchForLabel(label: string) {
    this.search = label;
  }
}
