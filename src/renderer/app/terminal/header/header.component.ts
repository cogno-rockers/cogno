import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TerminalService} from '../+state/terminal.service';
import {Observable} from 'rxjs';
import {AsyncPipe, NgIf} from '@angular/common';
import {SettingsService} from '../../+shared/services/settings/settings.service';
import {Theme} from '../../../../shared/models/settings';
import {isSuccessfulReturn} from '../../common/return-codes';
import {filter, map} from 'rxjs/operators';
import {CommandContextMenuService} from '../command-context-menu/+state/command-context-menu.service';
import {CommandDecorationEntry} from '../+state/handler/decoration.handler';
import {IconComponent} from '../../+shared/components/icon/icon.component';

@Component({
    selector: 'app-header',
    imports: [
        AsyncPipe,
        IconComponent,
        NgIf
    ],
    templateUrl: './header.component.html',
    styleUrl: './header.component.scss',
    encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit{

  public data: Observable<{ entry: CommandDecorationEntry; className: string;}>;

  constructor(private service: TerminalService, private contextMenuService: CommandContextMenuService) {}

  ngOnInit(): void {
    this.data = this.service.outsideViewDecoration().pipe(map(c => {
      return {entry: c, className: this.getReturnCodeClassName(c?.command.status.returnCode)};
    }));
  }

  private getReturnCodeClassName(returnCode?: number): string {
    if(returnCode === undefined || returnCode === null) {return 'running';}
    return isSuccessfulReturn(returnCode) ? 'success' : 'error';
  }

  commandClicked(entry: CommandDecorationEntry, event: MouseEvent) {
    const element = event.target as HTMLElement;
    // Get the bounding rectangle of the element
    const rect = element.getBoundingClientRect();
    // Coordinates relative to the viewport
    const x = rect.right;
    const y = rect.bottom;
    event.stopPropagation();
    event.preventDefault();
    this.service.updateActiveTab();
    this.contextMenuService.openMenuOnPosition({position: {x: x, y: y}, command: entry.command.command, hasBookmark: !!entry.bookmarkDecoration, id: entry.id, returnCode: entry.command.status.returnCode, startTime: entry.command.status.startTime, endTime: entry.command.status.endTime});
  }
}
