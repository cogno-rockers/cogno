import {ElectronService} from '../electron/electron.service';
import {ShellsLoaderService} from './shells-loader.service';
import {TestHelper} from '../../../../../test/helper';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {of} from 'rxjs';
import {debounceTime} from 'rxjs/operators';


describe('ShellsLoaderService', () => {

  let electronService: ElectronService;
  let shellsLoaderService: ShellsLoaderService;

  beforeEach(() => {
    // @ts-ignore
    electronService = new ElectronService(null);
    shellsLoaderService = new ShellsLoaderService(electronService);
  });

  it('should work', (done) => {
    jest.spyOn(electronService, 'invoke').mockImplementation((channel, listener) => {
      return of([TestHelper.createShellConfig()]).pipe(debounceTime(500));
    });
    shellsLoaderService.loadShells().subscribe(r => {
      expect(electronService.invoke).toBeCalledWith(IpcChannel.GetAvailableShells);
      expect(r).toEqual([TestHelper.createShellConfig()]);
      done();
    });
  });
});
