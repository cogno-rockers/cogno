export const ReturnCodes: number[] = [0, 128, 130, 141];

export function isSuccessfulReturn(code: number){
  return ReturnCodes.indexOf(code) !== -1;
}
