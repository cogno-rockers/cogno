import {Injectable, OnDestroy} from '@angular/core';
import {Position, TabType} from '../../../../../shared/models/models';
import {GlobalMenuService} from '../../../+shared/abstract-components/menu/+state/global-menu.service';
import {TerminalService} from '../../+state/terminal.service';
import {createStore, Store} from '../../../common/store/store';
import {MenuService} from '../../../+shared/abstract-components/menu/menu.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GridService} from '../../../global/grid/+state/grid.service';
import {Nav} from '../../../settings/+state/model';

export interface PromptContextMenuState {
  id: string;
  clickPosition: Position;
  command: string;
  returnCode?: number;
  startTime?: number;
  endTime?: number;
  hasBookmark: boolean;
  isCdCommand: boolean;
}

@Injectable()
export class CommandContextMenuService extends MenuService implements OnDestroy{

  private store: Store<PromptContextMenuState>;
  constructor(
    private gridService: GridService,
    menuService: GlobalMenuService,
    private terminalService: TerminalService) {
    super('PromptContext', menuService);
  }

  init(): void {
    this.store = createStore(this.terminalService.getTabId() + 'promptContextMenu', {id: null, clickPosition: null, line: null, hasBookmark: false, command: '', isCdCommand: false});
  }

  selectIsActive(): Observable<boolean> {
    return super.selectIsActive().pipe(map(globalActive => globalActive && this.gridService.getActiveTabId() === this.terminalService.getTabId()));
  }

  ngOnDestroy(): void {
    this.store.destroy();
  }

  openMenuOnPosition(data: {position: Position; id: string; hasBookmark: boolean; command: string; returnCode: number; startTime: number; endTime?: number}): void {
    this.store.update({
      clickPosition: data.position,
      id: data.id,
      hasBookmark: data.hasBookmark,
      command: data.command,
      returnCode: data.returnCode,
      startTime: data.startTime,
      endTime: data.endTime,
      isCdCommand: data.command?.trim().startsWith('cd ') || false
    });
    super.openMenu();
  }

  getClickPosition() {
    return this.store.get(s => s.clickPosition);
  }

  addBookmark() {
    this.terminalService.addBookmark(this.store.get(s => s.id));
    super.closeMenu();
  }

  removeBookmark() {
    this.terminalService.removeBookmark(this.store.get(s => s.id));
    super.closeMenu();
  }

  copyCommand() {
    this.terminalService.copyCommand(this.store.get(s => s.id));
    super.closeMenu();
  }

  selectHasBookmark() {
    return this.store.select(s => s.hasBookmark);
  }

  copyOutput() {
    super.closeMenu();
    return this.terminalService.copyOutput(this.store.get(s => s.id));
  }

  scrollToCommandTop() {
    super.closeMenu();
    return this.terminalService.scrollToCommandTop(this.store.get(s => s.id));
  }

  scrollToCommandBottom() {
    super.closeMenu();
    return this.terminalService.scrollToCommandBottom(this.store.get(s => s.id));
  }

  selectCommand() {
    return this.store.select(s => s.command);
  }

  selectReturnCode() {
    return this.store.select(s => s.returnCode);
  }

  openCommandSettings() {
    super.closeMenu();
    this.gridService.addNewTabOnActivePane(TabType.Settings, {selectedNav: Nav.Commands,
      filter: this.store.get(s => s.command)});
  }

  selectIsCdCommand() {
    return this.store.select(s => s.isCdCommand);
  }

  selectProcessTime(): Observable<number | undefined> {
    return this.store.select(s => s.startTime && s.endTime ? s.endTime - s.startTime : undefined);
  }
}
