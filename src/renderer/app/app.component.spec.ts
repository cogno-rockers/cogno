
import {
  clear,
  getInitService,
  getSettingsService
} from '../../test/factory';
import {AppComponent} from './app.component';
import {InitService} from './+shared/services/init/init.service';
import {TestHelper} from '../../test/helper';
import {DEFAULT_SETTINGS} from '../../shared/default-settings';
import {ParseResult} from '../../shared/parser/parser';
import {Theme} from '../../shared/models/settings';


function testCssVariables(theme: Theme) {
  expect(document.documentElement.style.getPropertyValue('--background-color')).toEqual(theme.colors.background);
  expect(document.documentElement.style.getPropertyValue('--background-color-10l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--background-color-20l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--background-color-30l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--background-color-40l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--background-color-10d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--background-color-20d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--background-color-30d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--background-color-40d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--color-shadow1')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--color-shadow2')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--color-shadow3')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color')).toEqual(theme.colors.foreground);
  expect(document.documentElement.style.getPropertyValue('--foreground-color-10l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color-20l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color-30l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color-40l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color-10d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color-20d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color-30d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--foreground-color-40d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color')).toEqual(theme.colors.highlight);
  expect(document.documentElement.style.getPropertyValue('--highlight-color-10l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color-20l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color-30l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color-40l')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color-10d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color-20d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color-30d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--highlight-color-40d')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--color-green')).toEqual(theme.colors.green);
  expect(document.documentElement.style.getPropertyValue('--color-red')).toEqual(theme.colors.red);
  expect(document.documentElement.style.getPropertyValue('--color-blue')).toEqual(theme.colors.blue);
  expect(document.documentElement.style.getPropertyValue('--color-yellow')).toEqual(theme.colors.yellow);
  expect(document.documentElement.style.getPropertyValue('--color-white')).toEqual(theme.colors.white);
  expect(document.documentElement.style.getPropertyValue('--color-black')).toEqual(theme.colors.black);
  expect(document.documentElement.style.getPropertyValue('--cursor-color')).toEqual(theme.colors.cursor);
  expect(document.documentElement.style.getPropertyValue('--shadow1')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--shadow2')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--shadow3')).toBeTruthy();
  expect(document.documentElement.style.getPropertyValue('--appFontSize')).toEqual(`${theme.appFontsize}px`);
  expect(document.documentElement.style.getPropertyValue('--fontSize')).toEqual(`${theme.fontsize}px`);
  expect(document.documentElement.style.getPropertyValue('--fontWeight')).toEqual(theme.fontWeight);
  expect(document.documentElement.style.getPropertyValue('--fontFamily')).toEqual(`'${theme.fontFamily}'`);
  expect(document.documentElement.style.getPropertyValue('--appFontFamily')).toEqual(`'${theme.appFontFamily}'`);
  expect(document.documentElement.style.getPropertyValue('--padding')).toEqual(`${theme.paddingAsArray[3]}rem`);
  expect(document.documentElement.style.getPropertyValue('--padding-xterm')).toEqual(`${theme.paddingAsArray[0]}rem ${theme.paddingAsArray[1]}rem ${theme.paddingAsArray[2]}rem ${theme.paddingAsArray[3]}rem`);
  expect(document.documentElement.style.getPropertyValue('--color-command-running')).toEqual(`${theme.colors.commandRunning}`);
  expect(document.documentElement.style.getPropertyValue('--color-command-success')).toEqual(`${theme.colors.commandSuccess}`);
  expect(document.documentElement.style.getPropertyValue('--color-command-error')).toEqual(`${theme.colors.commandError}`);
}

describe('AppComponent', () => {
  let component: AppComponent;
  let initService: InitService;

  beforeEach(() => {
    clear();
    initService = getInitService();
    component = new AppComponent(initService, getSettingsService());
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set correct css variables', (done) => {
    const settings = {...TestHelper.createSettingsResult()};
    initService.settingsLoaded(settings);
    const theme = settings.settings.themes.find(s => s.isDefault);
    component.initSettings().subscribe(() => {
      testCssVariables(theme);
      done();
    });
  });

  it('should set show error to false if settings are valid', (done) => {
    const settings = {...TestHelper.createSettingsResult()};
    initService.settingsLoaded(settings);
    component.showError.subscribe(showError => {
      expect(showError).toBeFalsy();
      done();
    });
    component.initSettings().subscribe();
  });

  it('should use default settings if loaded settings are invalid error', (done) => {
    const settings: ParseResult = {isValid: false, hasWarnings: false, warnings: undefined, errors: ['error']};
    initService.settingsLoaded(settings);
    const theme = DEFAULT_SETTINGS.themes[0];
    component.initSettings().subscribe(() => {
      testCssVariables(theme);
      done();
    });
  });

  it('should set show error to true if settings are invalid', (done) => {
    const settings: ParseResult = {isValid: false, hasWarnings: false, warnings: undefined, errors: ['error']};
    initService.settingsLoaded(settings);
    component.showError.subscribe(showError => {
      expect(showError).toBeTruthy();
      done();
    });
    component.initSettings().subscribe();
  });
});
