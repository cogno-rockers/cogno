import {PluginConfig} from '../../../../+shared/models/plugin-config';
import {HighlightType, Suggester, Suggestion, SuggestionType, SuggestRequest} from '../suggester';
import {RegexpHelper} from '../../../../common/regexp';
import {AnsiEscSequence} from '../../../../common/ansi-escape';

export class PluginSuggester implements Suggester {
  constructor(private worker: Worker, private config: PluginConfig) {
  }

  public get regexp(): string {
    return this.config.regexp;
  }

  public get placeholder(): string {
    return this.config.placeholder;
  }

  public get name(): string {
    return this.config.name;
  }

  public get label(): string {
    return this.config.label;
  }

  public suggest(info: SuggestRequest): Promise<Suggestion[]> {
    const timeoutPromise = new Promise<Suggestion[]>((_resolve, reject) => {
      setTimeout(() => {
        reject(new Error(`Plugin ${this.config.name} is to slow. Execution timed out after ${1000} ms.`))
      }, 1000);
    });
    const promise = new Promise<Suggestion[]>((resolve, reject) => {
      this.worker.onmessage = (ev: MessageEvent<Suggestion[] | {error: any}>) => {
        if(Array.isArray(ev.data)){
          resolve(ev.data.map(s => {
            // TODO: #### Return of plugin should contain 'value' instead of 'command', because we now have suggesters which return just values and not commands.
            const command = info.placeholderInfo ? info.placeholderInfo.sourceCommand.replace(info.placeholderInfo.currentPlaceholder, s['command']) : s['command'];
            const c = this.buildExecCommand(command);
            const suggestion: Suggestion = {
              label: s.label,
              score: s.score ?? 1,
              icon: s.icon ?? this.config.icon,
              labelHighlight: [{text: s.label, type: HighlightType.None}],
              shellCommand: c.command,
              nextPlaceholder: c.placeholder,
              suggestionType: SuggestionType.PluginCommand,
              pluginLabel: this.config.label,
              terminalCommand: command
            };
            return suggestion;
          }).filter(s => s.label))
        } else {
          reject(`Plugin ${this.config.name} throw an error: ` + ev.data.error)
        }
      };

      this.worker.onmessageerror = (e) => reject(e);
      this.worker.onerror = (e) => reject(e.error || e.message);
    });
    this.worker.postMessage({methode: 'suggest', data: info});
    return Promise.race([promise, timeoutPromise]);
  }

  private buildExecCommand(command: string): {command: string, placeholder?: string} {
    const commandPlaceholder = new RegExp(RegexpHelper.RegexCommandPlaceholder, 'g').exec(command);
    if(!commandPlaceholder) {return {command};}
    const indexFirstMatch = commandPlaceholder.index;
    const firstMatch = commandPlaceholder[0];
    const moveCursor = AnsiEscSequence.moveCursorLeft(command.length - firstMatch.length - indexFirstMatch);
    return {command: command.replace(commandPlaceholder[0], "") + moveCursor, placeholder: firstMatch};
  }

  destroy() {
    this.worker.terminate();
  }
}
