export interface SearchOptions {
  searchText: string;
  regex: boolean;
  wholeWord: boolean;
  caseSensitive: boolean;
}

export interface SearchResult {
  count: number;
  position: number;
  hasMatch: boolean;
}
