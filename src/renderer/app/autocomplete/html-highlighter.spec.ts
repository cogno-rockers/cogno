import {HighlightType, Suggestion} from './+state/suggester/suggester';
import {calcHtml} from './html-highlighter';

describe('HtmlHighlighter', () => {

  it('should work with undefined highlightlabel', () => {
    const suggestion: Suggestion = {labelHighlight: undefined};
    const result = calcHtml(suggestion);
    expect(result).toEqual('');
  });

  it('should work with empty highlightlabel', () => {
    const suggestion: Suggestion = {labelHighlight: []};
    const result = calcHtml(suggestion);
    expect(result).toEqual('');
  });

  it('should calc correct highlight html', () => {
    const suggestion: Suggestion = {labelHighlight: [{text: 'cd', type: HighlightType.Search}, {text: ' ', type: HighlightType.None}, {text: 'v', type: HighlightType.Search}, {text: '/src/Test.V', type: HighlightType.None}]};
    const result = calcHtml(suggestion);
    expect(result).toEqual('<span class="highlight">cd</span>&nbsp;<span class="highlight">v</span>/src/Test.V');
  });

  it('should calc correct highlight html with variable arguments', () => {
    const suggestion: Suggestion = {labelHighlight: [{text: 'cd', type: HighlightType.Search}, {text: ' ', type: HighlightType.None}, {text: '?', type: HighlightType.Argument}]};
    const result = calcHtml(suggestion);
    expect(result).toEqual('<span class="highlight">cd</span>&nbsp;<span class="lowlight">?</span>');
  });

  it('should calc correct highlight html with connected', () => {
    const suggestion: Suggestion = {labelHighlight: [{text: null, type: HighlightType.OpenSpecial}, {text: 'cd&nbsp;test', type: HighlightType.None}, {text: null, type: HighlightType.CloseSpecial}]};
    const result = calcHtml(suggestion);
    expect(result).toEqual('<span class="highlight-special">cd&nbsp;test</span>');
  });
});
