import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TabComponent} from '../+shared/abstract-components/tab/tab.component';
import {AboutService, AboutState} from './+state/about.service';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../+shared/components/icon/icon.component';
import {LogoComponent} from '../+shared/components/logo/logo.component';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    providers: [AboutService],
    imports: [
        CommonModule,
        IconComponent,
        LogoComponent
    ]
})
export class AboutComponent extends TabComponent<AboutState> implements OnInit{

  public version: Observable<string>;
  public isNightly: Observable<boolean>;
  public userId: Observable<string>;
  public isProduction: Observable<boolean>;

  constructor(protected service: AboutService) {
    super(service);
  }

  ngOnInit(): void {
    this.service.init(this.tabId);
    this.version = this.service.selectVersion();
    this.isNightly = this.service.selectIsNightly();
    this.userId = this.service.selectUserId();
    this.isProduction = this.service.selectIsProduction();
  }

  openThirdPartyLicenses() {
    this.service.openThirdPartyLicenses();
  }

  openLogs() {
    this.service.openLogs();
  }

  openDevtools() {
    this.service.openDevtools();
  }
}
