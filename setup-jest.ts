import {setupZoneTestEnv} from 'jest-preset-angular/setup-env/zone';

setupZoneTestEnv();

const crypto = require('crypto');
Object.defineProperty(global.self, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
});
Object.defineProperty(window.HTMLCanvasElement.prototype, 'getContext', {
  value: jest.fn(() => {
    return {
      // Mock any necessary WebGL methods here
      fillRect: jest.fn(),
      clearRect: jest.fn(),
      // Add more mocks as needed
    };
  }),
});
jest.mock('electron-log/renderer', () => ({
  info: jest.fn(),
  error: jest.fn(),
  warn: jest.fn(),
  debug: jest.fn(),
  silly: jest.fn(),
  variables: {
    processType: 'test'
  },
  transports: {
    file: {
      level: 'info',
    },
  },
}));
global['__unittest__'] = true;
window.require = require;
(window as any)['setImmediate'] = (callback: (...args: any) => void, ...args: any) => setTimeout(() => callback(args));
