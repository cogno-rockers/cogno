export type PluginConfig = {
  name: string;
  type: string;
  regexp?: string;
  placeholder?: string;

  description: string;

  installedVersion: string;
  availableVersion: string;

  username: string;
  email: string;
  repository: string;
  homepage: string;

  tarballUrl: string;

  installDir: string;
  workerPath: string;

  isInstalled: boolean;
  isUpdateAvailable: boolean;

  icon: string;
  label: string;
};
