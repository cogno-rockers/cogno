import {ParseResult} from './settings.parser';
import * as JSON5 from 'json5';
import {addErrors, addWarnings} from './parser';
import {Shortcuts} from '../models/shortcuts';
import {DEFAULT_SETTINGS} from '../default-settings';

export namespace ShortcutsParser {
  export function validateShortcuts(shortcuts: Shortcuts): ShortcutsResult {
    try {
      const result = {isValid: true, shortcuts: shortcuts, hasWarnings: false};
      validate(result);
      return result;
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  export function parseShortcuts(shortcutsAsJson: string): ShortcutsResult {
    try {
      return validateShortcuts(JSON5.parse(shortcutsAsJson) as Shortcuts);
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  function validateNullOrEmpty(result: ShortcutsResult) {
    const error = !result.shortcuts || Object.keys(result.shortcuts).length === 0 ? 'The "shortcuts" property is missing.' : null;
    addErrors(result, error);
  }

  function validateNoDuplicates(result: ShortcutsResult) {
    const shortcuts = {...result.shortcuts};
    shortcuts.copy = undefined; // copy is a special shortcut and can be duplicated
    const shortcutValues = Object.values(shortcuts).filter(s => typeof s === 'string') as string[];
    const aliasValues = Object.values(shortcuts.aliases).map(a => a.shortcut);
    const allShortcutValues = shortcutValues.concat(aliasValues);
    const distinctShortcuts = new Set<string>(allShortcutValues);
    let duplicatedShortcuts = null;
    if (distinctShortcuts.size !== allShortcutValues.length) {
      duplicatedShortcuts = [];
      distinctShortcuts.forEach(s => {
        if (allShortcutValues.filter(a => a === s).length > 1) {
          duplicatedShortcuts.push(s);
        }
      });
    }
    const warning = duplicatedShortcuts !== null ? `You have an duplicated shortcut defined. Please change or delete ${duplicatedShortcuts.toString()}` : null;
    addWarnings(result, warning);
  }

  function validate(result: ShortcutsResult): void {
    validateNullOrEmpty(result);
    validateAccelerators(result);
    validateAliases(result);
    validateNoDuplicates(result);
    removeNotExistingShortcuts(result);
  }

  function validateAccelerators(result: ShortcutsResult): void {
    if (!result.isValid) {
      return;
    }
    for (const key in result.shortcuts) {
      if (typeof result.shortcuts[key] === 'string') {
        if (!!result.shortcuts[key]) {
          const error = !isAccelerator(result.shortcuts[key]) ? `Shortcut '${key}' is invalid. Please use electron accelerators.` : '';
          addErrors(result, error);
        }
      }
    }
  }

  function removeNotExistingShortcuts(result: ShortcutsResult): void {
    if (!result.isValid) {
      return;
    }
    for (const key in result.shortcuts) {
      if (typeof result.shortcuts[key] === 'string' && !DEFAULT_SETTINGS.shortcuts[key]) {
        delete result.shortcuts[key];
      }
    }
  }

  function validateAliases(result: ShortcutsResult): void {
    if (result.shortcuts.aliases) {
      for (const alias of result.shortcuts.aliases) {
        let error = !alias.shortcut ? 'Shortcut for alias is not defined.' : '';
        addErrors(result, error);
        error = !alias.command ? 'Command for alias is not defined.' : '';
        addErrors(result, error);
        error = !isAccelerator(alias.shortcut) ? `Shortcut '${alias.shortcut}' is invalid. Please use electron accelerators.` : '';
        addErrors(result, error);
      }
    }
  }

  function isAccelerator(str: string): boolean {
    const modifiers = /^(Command|Cmd|Control|Ctrl|CommandOrControl|CmdOrCtrl|Alt|Option|AltGr|Shift|Super)$/;
    const keyCodes = /^([0-9A-Z)ÄÖÜµ€!@#$%^&*(:+<_>?~{|}";=,\-.\/`[\\\]']|F1*[1-9]|F10|F2[0-4]|Plus|Space|Tab|Backspace|Delete|Insert|Return|Enter|Up|Down|Left|Right|Home|End|PageUp|PageDown|Escape|Esc|VolumeUp|VolumeDown|VolumeMute|MediaNextTrack|MediaPreviousTrack|MediaStop|MediaPlayPause|PrintScreen)$/;
    const parts = str.split('+');
    let keyFound = false;
    return parts.every((val, index) => {
      const isKey = keyCodes.test(val);
      const isModifier = modifiers.test(val);
      if (isKey) {
        if (keyFound) {
          return false;
        }
        keyFound = true;
      }
      if (index === parts.length - 1 && !keyFound) {
        return false;
      }
      return isKey || isModifier;
    });
  }
}

export interface ShortcutsResult extends ParseResult {
  shortcuts?: Shortcuts;
}
