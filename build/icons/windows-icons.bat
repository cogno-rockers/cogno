inkscape -w 16 -h 16 .\normal\favicon.svg -o .\normal\favicon.16x16.png
inkscape -w 32 -h 32 .\normal\favicon.svg -o .\normal\favicon.32x32.png
inkscape -w 48 -h 48 .\normal\favicon.svg -o .\normal\favicon.48x48.png
inkscape -w 64 -h 64 .\normal\favicon.svg -o .\normal\favicon.64x64.png
inkscape -w 128 -h 128 .\normal\favicon.svg -o .\normal\favicon.128x128.png
inkscape -w 256 -h 256 .\normal\favicon.svg -o .\normal\favicon.256x256.png
inkscape -w 1048 -h 1048 .\normal\favicon.svg -o .\normal\favicon.png
magick convert .\normal\favicon.16x16.png .\normal\favicon.32x32.png .\normal\favicon.48x48.png .\normal\favicon.64x64.png .\normal\favicon.128x128.png .\normal\favicon.256x256.png .\normal\favicon.png .\normal\favicon.ico
inkscape -w 16 -h 16 .\nightly\favicon.svg -o .\nightly\favicon.16x16.png
inkscape -w 32 -h 32 .\nightly\favicon.svg -o .\nightly\favicon.32x32.png
inkscape -w 48 -h 48 .\nightly\favicon.svg -o .\nightly\favicon.48x48.png
inkscape -w 64 -h 64 .\nightly\favicon.svg -o .\nightly\favicon.64x64.png
inkscape -w 128 -h 128 .\nightly\favicon.svg -o .\nightly\favicon.128x128.png
inkscape -w 256 -h 256 .\nightly\favicon.svg -o .\nightly\favicon.256x256.png
inkscape -w 1048 -h 1048 .\nightly\favicon.svg -o .\nightly\favicon.png
magick convert .\nightly\favicon.16x16.png .\nightly\favicon.32x32.png .\nightly\favicon.48x48.png .\nightly\favicon.64x64.png .\nightly\favicon.128x128.png .\nightly\favicon.256x256.png .\nightly\favicon.png .\nightly\favicon.ico
copy ".\normal\favicon.ico" "..\win32\icon\normal\icon.ico"
copy ".\nightly\favicon.ico" "..\win32\icon\nightly\icon.ico"
copy ".\normal\favicon.png" "..\darwin\icon\normal\icon.png"
copy ".\nightly\favicon.png" "..\darwin\icon\nightly\icon.png"
copy ".\normal\favicon.svg" "..\linux\icon\normal\icon.svg"
copy ".\nightly\favicon.svg" "..\linux\icon\nightly\icon.svg"
