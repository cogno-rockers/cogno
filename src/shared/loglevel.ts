export enum LogLevel {
  DEBUG = 'debug', INFO = 'info', ERROR = 'error'
}
