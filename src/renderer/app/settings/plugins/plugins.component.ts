import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Observable} from 'rxjs';
import {PluginConfig} from '../../+shared/models/plugin-config';
import {SettingsTabService} from '../+state/settings-tab.service';
import {FilterNamePipe} from '../pipe/filter-name.pipe';
import {PluginsLoader} from './+state/plugins.loader';
import {PluginsSettingsService} from './+state/plugins.settings.service';
import {LoaderComponent} from '../../+shared/components/loader/loader.component';

@Component({
    selector: 'app-plugins',
    templateUrl: './plugins.component.html',
    styleUrls: ['./plugins.component.scss'],
    providers: [PluginsSettingsService, PluginsLoader],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        FormsModule,
        FilterNamePipe,
        LoaderComponent
    ]
})
export class PluginsComponent implements OnInit, OnDestroy {

  public availablePlugins: Observable<PluginConfig[]>;
  public installedPlugins: Observable<PluginConfig[]>;
  public selectedPlugin: Observable<PluginConfig>;
  public isLoading: Observable<boolean>;
  public isPluginsLoading: Observable<boolean>;
  public search: string;

  constructor(private service: PluginsSettingsService, private tabService: SettingsTabService) {
    this.service.init(this.tabService.getTabId() + 'pluginsettings');
    this.availablePlugins = service.selectAvailablePlugins();
    this.installedPlugins = service.selectInstalledPlugins();
    this.selectedPlugin = service.selectSelectedPlugin();
    this.isLoading = tabService.selectIsLoading();
    this.isPluginsLoading = service.selectIsPluginsLoading();
  }

  ngOnInit(): void {
    this.service.loadPlugins();
  }

  ngOnDestroy(): void {
  }

  selectPlugin(plugin: PluginConfig) {
    this.service.selectPlugin(plugin);
  }

  async installPlugin() {
    await this.service.installPlugin();
  }

  async uninstallPlugin() {
    await this.service.uninstallPlugin();
  }

  async updatePlugin() {
    await this.service.updatePlugin();
  }

  openExternal(link: string) {
    this.service.openExternal(link);
  }

  openSourceCode(plugin: PluginConfig) {
    this.service.openExternal('file://' + plugin.installDir);
  }
}
