import {ChangeDetectionStrategy, Component, ElementRef, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Position} from '../../../../shared/models/models';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';
import {MenuComponent} from '../../+shared/abstract-components/menu/menu.component';
import {CommandContextMenuService} from './+state/command-context-menu.service';
import {map} from 'rxjs/operators';
import {CommonModule} from '@angular/common';
import {KeytipService} from '../../+shared/services/keyboard/keytip.service';
import {TimespanPipe} from '../../+shared/pipes/timespan/timespan.pipe';

@Component({
    selector: 'app-command-context-menu',
    templateUrl: './command-context-menu.component.html',
    styleUrls: ['./command-context-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        CommonModule,
        TimespanPipe
    ]
})
export class CommandContextMenuComponent extends MenuComponent implements OnInit{

  isVisible: Observable<boolean>;
  hasBookmark: Observable<boolean>;
  returnCode: Observable<number>;
  processTime: Observable<number>;
  hasReturnCode: Observable<boolean>;
  isCdCommand: Observable<boolean>;
  positionStyle: Observable<{top: string; left: string}>;

  constructor(protected eRef: ElementRef, protected keyboardService: KeyboardService, public keytipService: KeytipService, protected menuService: CommandContextMenuService) {
    super(eRef, menuService, keyboardService, keytipService);
  }

  ngOnInit() {
    this.menuService.init();
    this.returnCode = this.menuService.selectReturnCode();
    this.processTime = this.menuService.selectProcessTime();
    this.hasReturnCode = this.returnCode.pipe(map(code => code !== null && code !== undefined));
    this.hasBookmark = this.menuService.selectHasBookmark();
    this.positionStyle = this.menuService.selectIsActive().pipe(map((active) => this.calculatePositionStyle(this.menuService.getClickPosition(), active)));
    this.isCdCommand = this.menuService.selectIsCdCommand();
  }

  private calculatePositionStyle(position: Position, isActive: boolean): any {
    if (!position || !isActive) {
      return {
        'top': '-1000000px',
        'left': '-1000000px'
      };
    }
    const parentHeight =  this.eRef.nativeElement.parentElement.offsetHeight;
    const height =  this.eRef.nativeElement.firstElementChild.getBoundingClientRect().height;
    const parentX =  this.eRef.nativeElement.parentElement.getBoundingClientRect().x;
    const parentY =  this.eRef.nativeElement.parentElement.getBoundingClientRect().y;
    const parentYBottom =  parentHeight;
    const correctPosition = {x: position.x - parentX - 250, y: position.y - parentY};

    if (correctPosition.y + 40 + height > parentYBottom) {
      correctPosition.y = parentYBottom - height - 40;
    }
    return {
      'top': correctPosition.y + 'px',
      'left': correctPosition.x + 'px'
    };
  }

  addBookmark() {
    this.menuService.addBookmark();
  }

  copyCommand() {
    this.menuService.copyCommand();
  }

  removeBookmark() {
    this.menuService.removeBookmark();
  }

  copyOutput() {
    this.menuService.copyOutput();
  }

  scrollToCommandTop() {
    this.menuService.scrollToCommandTop();
  }

  scrollToCommandBottom() {
    this.menuService.scrollToCommandBottom();
  }

  openCommandSettings() {
    this.menuService.openCommandSettings();
  }
}
