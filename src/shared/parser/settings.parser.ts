import {Settings} from '../models/settings';
import {ThemesParser} from './themes.parser';
import * as JSON5 from 'json5';
import {ShellConfigParser} from './shells.parser';
import {addErrors, addWarnings} from './parser';
import {ShortcutsParser} from './shortcuts.parser';
import {GeneralParser} from './general.parser';
import {AutocompleteParser} from './autocomplete.parser';
import {DEFAULT_SETTINGS} from '../default-settings';


export namespace SettingsParser {

  export function parseSettings(rawSettingsAsJson: string, enrich = false): ParseResult {
    try {
      if (enrich) {
        rawSettingsAsJson = mergeWithDefaultSettings(rawSettingsAsJson);
      }
      const settings = JSON5.parse(rawSettingsAsJson) as Settings;

      return validateSettings(settings, enrich);
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  export function validateSettings(settings: Settings, enrich = false): ParseResult {
    try {
      const result = {isValid: true, settings: settings, hasWarnings: false};
      validate(result, enrich);
      return result;
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  function mergeWithDefaultSettings(settings: string): string {
    return JSON5.stringify(deepMerge(DEFAULT_SETTINGS, JSON5.parse(settings)));
  }

  function deepMerge(...args: any): any {
      // Variables
      const target = {};
      let i = 0;
      // Merge the object into the target object
      const merger = (obj) => {
         for (const prop in obj) {
            if (obj.hasOwnProperty(prop)) {
              if (Object.prototype.toString.call(obj[prop]) === '[object Object]') {
              // If we're doing a deep merge
              // and the property is an object
                 target[prop] = deepMerge(target[prop], obj[prop]);
             } else {
              // Otherwise, do a regular merge
                target[prop] = obj[prop];
             }
          }
        }
      };
       // Loop through each object and conduct a merge
       for (; i < args.length; i++) {
          merger(args[i]);
       }
       return target;
  }

  function validate(result: ParseResult, enrich = false) {
    const error = !result.settings ? 'No settings are defined.' : null;
    addErrors(result, error);
    validateThemes(result, enrich);
    validateShellConfigs(result, enrich);
    validateShortcuts(result);
    validateGeneralConfig(result, enrich);
    validateAutocompleteConfig(result, enrich);
  }

  function validateThemes(result: ParseResult, enrich = false) {
    if (!result.isValid) { return; }
    const themesResult = ThemesParser.validateThemes(result.settings.themes, enrich);
    if (!themesResult.isValid) {
      addErrors(result, ...themesResult.errors );
    }
    if (themesResult.hasWarnings) {
      addWarnings(result, ...themesResult.warnings );
    }
  }

  function validateShellConfigs(result: ParseResult, enrich = false) {
    if (!result.isValid) { return; }
    const shellsResult = ShellConfigParser.validateShellConfig(result.settings.shells, enrich);
    if (!shellsResult.isValid) {
      addErrors(result, ...shellsResult.errors );
    }
    if (shellsResult.hasWarnings) {
      addWarnings(result, ...shellsResult.warnings );
    }
  }

  function validateShortcuts(result: ParseResult) {
    if (!result.isValid) { return; }
    const shortcutResult = ShortcutsParser.validateShortcuts(result.settings.shortcuts);
    if (!shortcutResult.isValid) {
      addErrors(result, ...shortcutResult.errors );
    }
    if (shortcutResult.hasWarnings) {
      addWarnings(result, ...shortcutResult.warnings );
    }
  }

  function validateGeneralConfig(result: ParseResult, enrich = false) {
    if (!result.isValid) { return; }
    const generalResult = GeneralParser.validateGeneralConfig(result.settings.general, enrich);
    if (!generalResult.isValid) {
      addErrors(result, ...generalResult.errors );
    }
    if (generalResult.hasWarnings) {
      addWarnings(result, ...generalResult.warnings );
    }
  }

  function validateAutocompleteConfig(result: ParseResult, enrich = false) {
    if (!result.isValid) { return; }
    const autocompleteResult = AutocompleteParser.validateAutocompleteConfig(result.settings.autocomplete, enrich);
    if (!autocompleteResult.isValid) {
      addErrors(result, ...autocompleteResult.errors );
    }
    if (autocompleteResult.hasWarnings) {
      addWarnings(result, ...autocompleteResult.warnings );
    }
  }
}

export interface ParseResult {
  showOnboarding?: boolean;
  isValid: boolean;
  hasWarnings: boolean;
  errors?: string[];
  warnings?: string[];
  settings?: Settings;
}
