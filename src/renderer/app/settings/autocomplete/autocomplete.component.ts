import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {AutocompleteSettingsService} from './+state/autocomplete.settings.service';
import {SettingsTabService} from '../+state/settings-tab.service';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../../+shared/components/icon/icon.component';
import {FormsModule} from '@angular/forms';
import {Shortcuts} from '../../../../shared/models/shortcuts';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';
import {SettingsService} from '../../+shared/services/settings/settings.service';
import {map} from 'rxjs/operators';
import {ShortcutPipe} from '../../+shared/pipes/shortcut/shortcut.pipe';

@Component({
    selector: 'app-autocomplete',
    templateUrl: './autocomplete.component.html',
    styleUrls: ['./autocomplete.component.scss'],
    providers: [AutocompleteSettingsService],
    imports: [
        CommonModule,
        FormsModule,
        ShortcutPipe
    ]
})
export class AutocompleteComponent implements OnInit, OnDestroy {

  public position: Observable<'cursor' | 'line'>;
  public mode: Observable<'always' | 'shortcut'>;
  public shortcuts: Observable<string>;

  constructor(private settingsService: SettingsTabService, private service: AutocompleteSettingsService) {
  }

  ngOnInit(): void {
    this.service.init(this.settingsService.getTabId() + 'autocompleteSettings');
    this.position = this.service.selectAutocompletePosition();
    this.mode = this.service.selectAutocompleteMode();
    this.shortcuts = this.settingsService.selectShortcuts().pipe(map(s => s.showAutocompletion));
  }

  ngOnDestroy(): void {
    this.service.destroy();
  }

  setAutocompletePosition(position: 'cursor' | 'line') {
    this.service.setAutocompletePosition(position);
  }

  setAutocompleteMode(mode: 'always' | 'shortcut') {
    this.service.setAutocompleteMode(mode);
  }
}
