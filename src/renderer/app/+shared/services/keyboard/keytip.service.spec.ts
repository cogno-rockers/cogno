import { fakeAsync, tick } from '@angular/core/testing';
import { KeytipService } from './keytip.service';
import { KeyboardService } from './keyboard.service';
import {getKeyboardService, getKeytipService} from '../../../../../test/factory';
import {TestHelper} from '../../../../../test/helper';
import createKeyboardEvent = TestHelper.createKeyboardEvent;

describe('KeytipService', () => {
  let service: KeytipService;
  let keyboardService: KeyboardService;

  const keytipShortcut = createKeyboardEvent({ctrlKey: true, key: 'k', code: 'KeyK', type: 'keydown'});
  const keytipContextShortcut = createKeyboardEvent({ctrlKey: true, shiftKey: true, key: 'k', code: 'KeyK', type: 'keydown'});

  beforeEach(() => {

    keyboardService = getKeyboardService();
    service = getKeytipService()
  });

  afterEach(() => {
    // Clean up any keytip elements added to the DOM
    const keytips = document.querySelectorAll('.keytip');
    keytips.forEach((keytip) => keytip.remove());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('scanAndDecorate', () => {
    beforeEach(() => {
      // Add elements to the DOM for testing
      const element1 = document.createElement('button');
      element1.innerText = 'ButtonA'
      element1.setAttribute(KeytipService.keytipAttributeName, 'A');
      element1.style.width = '100px';
      element1.style.height = '100px';
      document.body.appendChild(element1);

      const element2 = document.createElement('button');
      element2.setAttribute(KeytipService.keytipAttributeName, 'B');
      element2.innerText = 'ButtonB'
      element2.style.display = 'none';
      document.body.appendChild(element2);
    });

    afterEach(() => {
      // Remove test elements from the DOM
      document.body.innerHTML = '';
    });

    it('should decorate visible elements with keytips in global mode', fakeAsync(() => {
      keyboardService.handleShortcut(keytipShortcut);
      tick(100);
      const keytips = document.querySelectorAll('.keytip');
      expect(keytips.length).toBe(1);
      const element = keytips[0] as HTMLElement;
      expect(element.innerText).toBe('A');
    }));

    it('should decorate elements in context mode', fakeAsync(() => {
      const container = document.createElement('div');
      container.setAttribute('data-keytip-container', '');
      document.body.appendChild(container);

      const element3 = document.createElement('button');
      element3.setAttribute('data-keytip', 'C');
      container.appendChild(element3);

      // Focus on an element inside the container
      element3.focus();

      keyboardService.handleShortcut(keytipContextShortcut, true);
      tick(100);

      const keytips = document.querySelectorAll('.keytip');
      expect(keytips.length).toBe(1);
      const element = keytips[0] as HTMLElement;
      expect(element.innerText).toBe('C');
    }));
  });

  describe('handleKeytip', () => {
    let actionElement: HTMLElement;

    beforeEach(fakeAsync(() => {
      actionElement = document.createElement('button');
      actionElement.setAttribute('data-keytip', 'A');
      document.body.appendChild(actionElement);
      jest.spyOn(actionElement, 'click');
      keyboardService.handleShortcut(keytipShortcut, true);
      tick(100)
    }));

    afterEach(() => {
      document.body.innerHTML = '';
    });

    it('should handle keytip activation', () => {
      const event = new KeyboardEvent('keydown', { code: 'KeyA' });
      service.handleKeytip(event);
      expect(actionElement.click).toHaveBeenCalled();
    });

    it('should clear keytips on Escape key', fakeAsync(() => {
      const event = new KeyboardEvent('keydown', { key: 'Escape' });
      service.handleKeytip(event);
      tick(100);
      expect(service['currentDepth']).toBeUndefined();
      expect(service['mode']).toBeUndefined();
      const keytips = document.querySelectorAll('.keytip');
      expect(keytips.length).toBe(0);
    }));

    it('should not handle modifier keys', fakeAsync(() => {
      const event = new KeyboardEvent('keydown', { code: 'KeyA', ctrlKey: true });
      const result = service.handleKeytip(event);
      tick(100);
      expect(result).toBeFalsy();
      expect(actionElement.click).not.toHaveBeenCalled();
    }));
  });

  describe('cleanupKeytips', () => {
    beforeEach(fakeAsync(() => {
      const element = document.createElement('button');
      element.setAttribute('data-keytip', 'A');
      document.body.appendChild(element);

      keyboardService.handleShortcut(keytipShortcut, true);
      tick(100);
    }));

    afterEach(() => {
      document.body.innerHTML = '';
    });

    it('should remove all keytip elements from the DOM', () => {
      service['cleanupKeytips']();

      const keytips = document.querySelectorAll('.keytip');
      expect(keytips.length).toBe(0);
    });
  });

  describe('disableOnMouseMove', () => {
    beforeEach(fakeAsync(() => {
      const element = document.createElement('button');
      element.setAttribute('data-keytip', 'A');
      document.body.appendChild(element);

      keyboardService.handleShortcut(keytipShortcut, true);
      tick(100);
    }));

    afterEach(() => {
      document.body.innerHTML = '';
    });

    it('should clear keytips on mouse move', fakeAsync(() => {
      const mouseMoveEvent = new MouseEvent('mousemove');
      window.dispatchEvent(mouseMoveEvent);
      tick();

      expect(service['currentDepth']).toBeUndefined();
      expect(service['mode']).toBeUndefined();
      const keytips = document.querySelectorAll('.keytip');
      expect(keytips.length).toBe(0);
    }));
  });

  describe('getNextChar', () => {
    it('should return the wished character if available', () => {
      const char = service['getNextChar']('A');
      expect(char).toBe('A');
    });

    it('should return the next available character if wished character is used', () => {
      service['keytipElements'].set('A', null);
      const char = service['getNextChar']('A');
      expect(char).toBe('B');
    });

    it('should return null if all characters are used', () => {
      KeytipService.allowedChars.forEach((c) => service['keytipElements'].set(c, null));
      const char = service['getNextChar']('Z');
      expect(char).toBeNull();
    });
  });

  describe('isElementVisible', () => {
    let element: HTMLElement;

    beforeEach(() => {
      element = document.createElement('div');
      document.body.appendChild(element);
    });

    afterEach(() => {
      document.body.innerHTML = '';
    });

    it('should return true for visible elements', () => {
      element.style.display = 'block';
      element.style.visibility = 'visible';
      element.style.width = '100px';
      element.style.height = '100px';
      element.setAttribute('data-keytip-depth', '1');
      service['currentDepth'] = 1;

      const isVisible = service['shouldDisplayKeytipForElement'](element);
      expect(isVisible).toBeTruthy();
    });

    it('should return false for elements with display:none', () => {
      element.style.display = 'none';
      const isVisible = service['shouldDisplayKeytipForElement'](element);
      expect(isVisible).toBeFalsy();
    });

    it('should return false for elements with visibility:hidden', () => {
      element.style.visibility = 'hidden';
      const isVisible = service['shouldDisplayKeytipForElement'](element);
      expect(isVisible).toBeFalsy();
    });

    it('should return false if depth does not match currentDepth', () => {
      element.setAttribute('data-keytip-depth', '2');
      service['currentDepth'] = 1;

      const isVisible = service['shouldDisplayKeytipForElement'](element);
      expect(isVisible).toBeFalsy();
    });
  });
});
