import { Pipe, PipeTransform } from '@angular/core';
import {HasName} from '../../../../shared/models/settings';

@Pipe({
  name: 'filterName',
  standalone: true
})
export class FilterNamePipe implements PipeTransform {

  transform(values: HasName[], searchValue: string): any[] {
    if (!searchValue || !values) {
      return values;
    }
    return values.filter((value) =>
      value.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1);
  }
}
