import {Injectable} from '@angular/core';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {createStore, Store} from '../../../common/store/store';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {Observable, Subscription} from 'rxjs';
import {SettingsService} from '../../../+shared/services/settings/settings.service';
import {SettingsTabService} from '../../+state/settings-tab.service';

export interface AutocompleteSettingsState {
  position: 'cursor' | 'line';
  mode: 'always' | 'shortcut';
}

export const initialState: AutocompleteSettingsState = {
  position: 'cursor',
  mode: 'always'
};

@Injectable()
export class AutocompleteSettingsService {

  private store: Store<AutocompleteSettingsState>;
  private subscriptions: Subscription[] = [];

  constructor(
    private electron: ElectronService,
    private settingsService: SettingsService,
    private tabService: SettingsTabService
    ) {
  }

  public init(id: string) {
    this.store = createStore(id,  initialState);
    this.subscriptions.push(
      this.settingsService.selectSettings().subscribe(settings => {
        this.tabService.updateIsLoading(false);
        this.store.update({
          position: settings.autocomplete?.position || 'cursor',
          mode: settings.autocomplete?.mode || 'always'
        });
      })
    );
  }

  destroy() {
    this.store.destroy();
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  private saveConfig() {
    this.electron.send(IpcChannel.SaveAutocompleteConfig, this.store.get(s => s));
    this.tabService.updateIsLoading(true);
  }

  setAutocompletePosition(position: 'cursor' | 'line') {
    this.store.update(s => ({position: position}));
    this.saveConfig();
  }

  setAutocompleteMode(mode: 'always' | 'shortcut') {
    this.store.update(s => ({mode: mode}));
    this.saveConfig();
  }

  selectAutocompletePosition(): Observable<'cursor' | 'line'> {
    return this.store.select(s => s.position);
  }

  selectAutocompleteMode(): Observable<'always' | 'shortcut'> {
    return this.store.select(s => s.mode);
  }

}
