import * as path from 'path';
import * as fs from 'fs';
import {Application} from '../shared/application';
import {BrowserWindow, ipcMain, screen} from 'electron';
import * as chokidar from 'chokidar';
import {
  AutocompleteConfig,
  GeneralConfig,
  Settings,
  ShellConfig,
  Theme,
  WindowSettings
} from '../shared/models/settings';
import {ParseResult, SettingsParser} from '../shared/parser/settings.parser';
import {IpcChannel} from '../shared/ipc.chanels';
import {Shortcuts} from '../shared/models/shortcuts';
import {DEFAULT_SETTINGS} from '../shared/default-settings';
import { LogLevel } from '../shared/loglevel';
import {Logger} from './logger';


const Datastore = require('@seald-io/nedb');



export namespace SettingsHandler {

  let datastore;
  const SETTINGS_FILE_NAME = 'settings.json';
  let logLevel: LogLevel;

  export function init(level: LogLevel): void {
    Logger.debug('Init settings handler');
    logLevel = level;
    datastore = new Datastore({filename: path.join(Application.getAppConfigDirPath(), 'db', 'temp.json'), autoload: true});
    const settingsPath = path.join(Application.getAppConfigDirPath(), SETTINGS_FILE_NAME);
    if (!isAvailable(Application.getAppConfigDirPath())) {
      Logger.info('Create app home dir in', Application.getAppConfigDirPath());
      fs?.mkdirSync(Application.getAppConfigDirPath());
    }
    if (!isAvailable(settingsPath)) {
      Logger.info('Start init settings');
      saveSettings({...DEFAULT_SETTINGS});
    }
    ipcMain.on(IpcChannel.InstallTheme, function (evte, theme: Theme) {
      loadSettings((settings) => {
        theme.isDefault = true;
        settings.settings.themes.forEach(t => t.isDefault = false);
        settings.settings.themes.push(theme);
        if (settings.isValid) {
          saveSettings(settings.settings);
        }
      });
    });
    ipcMain.on(IpcChannel.SaveShellsConfig, function (event, shells: ShellConfig[]) {
      loadSettings((settings) => {
        shells.forEach((shell) => {
          delete shell.isSelected;
          delete shell.isSubshell;
        });
        settings.settings.shells = shells;
        if (settings.isValid) {
          saveSettings(settings.settings);
        }
      });
    });
    ipcMain.on(IpcChannel.SaveSettings, function (event, settings: Settings) {
      saveSettings(settings);
    });
    ipcMain.on(IpcChannel.SaveThemeConfig, function (event, theme: Theme) {
      loadSettings((settings) => {
        if (theme.isDefault) {
          settings.settings.themes.forEach(t => t.isDefault = false);
        }
        if (theme.isDarkModeTheme) {
          settings.settings.themes.forEach(t => t.isDarkModeTheme = false);
        }
        const indexOfTheme = settings.settings.themes.findIndex(t => t.name === theme.name);
        settings.settings.themes.splice(indexOfTheme, 1, theme);
        if (settings.isValid) {
          saveSettings(settings.settings);
        }
      });
    });
    ipcMain.on(IpcChannel.SaveGeneralConfig, function (evte, config: GeneralConfig) {
      loadSettings((settings) => {
        settings.settings.general = config;
        if (settings.isValid) {
          saveSettings(settings.settings);
        }
      });
    });
    ipcMain.on(IpcChannel.SaveAutocompleteConfig, function (evte, config: AutocompleteConfig) {
      loadSettings((settings) => {
        settings.settings.autocomplete.position = config.position;
        settings.settings.autocomplete.mode = config.mode;
        if (settings.isValid) {
          saveSettings(settings.settings);
        }
      });
    });
    ipcMain.on(IpcChannel.SaveShortcutConfig, function (evte, shortcuts: Shortcuts) {
      loadSettings((settings) => {
        settings.settings.shortcuts = shortcuts;
        if (settings.isValid) {
          saveSettings(settings.settings);
        }
      });
    });
    ipcMain.on(IpcChannel.UninstallTheme, function (event, theme: Theme) {
      loadSettings((settings) => {
        if (settings.settings.themes.length > 1 && !!theme) {
          const themeInSettings = settings.settings.themes.find(t => t.name === theme.name);
          const indexOfTheme = settings.settings.themes.indexOf(themeInSettings);
          settings.settings.themes.splice(indexOfTheme, 1);
          if (!settings.settings.themes.find(t => t.isDefault)) {
            settings.settings.themes[0].isDefault = true;
          }
        }
        if (settings.isValid) {
          saveSettings(settings.settings);
        }
      });
    });
  }

  export function isInitialized(): boolean {
    return !!datastore;
  }

  export function getWindowSettings(callback: (w: WindowSettings) => void): void {
    datastore.findOne({_id: 'windowBounds'}, (err, data) => {
      const size = screen.getPrimaryDisplay().workAreaSize;
      if (err || !data || !data.bounds) {
        callback({
          isMaximized: false,
          isInBounds: false,
          bounds: {
            width: size.height / 1.5 * 1.61,
            height: size.height / 1.5
          }
        });
        return;
      }
      let inBounds = false;
      if (data.bounds.x && data.bounds.y) {
        screen.getAllDisplays().forEach((display) => {
          if (data.bounds.x >= display.workArea.x &&
            data.bounds.x <= display.workArea.x + display.workArea.width &&
            data.bounds.y >= display.workArea.y &&
            data.bounds.y <= display.workArea.y + display.workArea.height) {
            inBounds = true;
          }
        });
      }
      callback({...data, isInBounds: inBounds});
    });
  }

  export function saveWindowSettings(window: BrowserWindow): void {
    const t = {
      _id: 'windowBounds',
      isMaximized: window.isMaximized(),
      bounds: window.getBounds()
    };
    datastore.findOne({_id: 'windowBounds'}, (err, winBounds) => {
      if (!winBounds) {
        datastore.insert(t, () => {
          Logger.debug('window bounds inserted', t);
        });
      } else {
        datastore.update({_id: 'windowBounds'}, t, {}, () => {
          Logger.debug('window bounds updated', t);
        });
      }
    });
  }

  export function loadSettings(callback: (result: ParseResult) => void): void {
    loadSettingsAsString((err, settingsAsString) => {
      if (err) {
        Logger.error('Could not load settings.', err);
        callback({isValid: false, errors: [err.message], hasWarnings: false});
        return;
      }
      const settingsResult = SettingsParser.parseSettings(settingsAsString, true);
      settingsResult.showOnboarding = settingsResult.settings.shells.length === 0;
      if (!settingsResult.isValid) {
        Logger.error('Could not load settings.', settingsResult.errors);
        callback(settingsResult);
        return;
      }
      if (settingsResult.hasWarnings) {
        Logger.warn('Settings warning.', settingsResult.warnings);
      }
      const settings: Settings = settingsResult.settings;
      settings.logLevel = logLevel;
      settings.appConfigDirPath = Application.getAppConfigDirPath();

      callback(settingsResult);
    });
  }

  function isAvailable(p: string): boolean {
    if (!p) {
      return false;
    }
    return fs && fs.existsSync(p);
  }

  function loadSettingsAsString(callback: (err: any, settingsAsString: string) => void) {
    const settingsPath = path.join(Application.getAppConfigDirPath(), SETTINGS_FILE_NAME);
    fs.readFile(settingsPath, {encoding: 'utf8'}, (err, data) => {
      callback(err, data);
    });
  }

  export function saveSettings(settings: Settings) {
    const settingsPath = path.join(Application.getAppConfigDirPath(), SETTINGS_FILE_NAME);
    if (settings) {
      settings.themes.forEach(s => {
        s.isImageAvailable = undefined;
        s.isInstalled = undefined;
        s.terminalSexyId = undefined;
        s.paddingAsArray = undefined;
      });
      settings.logLevel = undefined;
      settings.appConfigDirPath = undefined;
      fs.writeFileSync(settingsPath, JSON.stringify(settings, null, 4));
    } else {
      fs.writeFileSync(settingsPath, '');
    }
  }

  export function watchSettings(): chokidar.FSWatcher {
    const settingsPath = path.join(Application.getAppConfigDirPath(), SETTINGS_FILE_NAME);
    Logger.info('Watch', settingsPath);
    return chokidar.watch(settingsPath);
  }
}
