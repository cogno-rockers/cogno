import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ElectronService} from '../electron/electron.service';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FontsLoaderService {

  constructor(private electron: ElectronService) {
  }

  loadFonts(): Observable<string[]> {
    return this.electron.invoke<string[]>(IpcChannel.GetAvailableFonts);
  }

  loadAppFonts(): Observable<string[]> {
    return this.electron.invoke<string[]>(IpcChannel.GetAvailableAppFonts);
  }
}
