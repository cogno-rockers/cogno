import {ElectronService} from '../../+shared/services/electron/electron.service';
import {
  clear,
  getElectronService, getFontsLoaderService,
  getGridService,
  getSettingsTabService, getThemesLoaderService, getThemesSettingsService
} from '../../../../test/factory';
import {SettingsTabService} from '../+state/settings-tab.service';
import {of} from 'rxjs';
import {GridService} from '../../global/grid/+state/grid.service';
import {BinaryTree} from '../../common/tree/binary-tree';
import {TabType} from '../../../../shared/models/models';
import {ThemesComponent} from './themes.component';
import {ThemesSettingsService} from './+state/themes.settings.service';
import {ThemesLoaderService} from './+state/themes.loader.service';
import {FontsLoaderService} from '../../+shared/services/fonts/fonts-loader.service';
import {TestHelper} from '../../../../test/helper';


describe('ThemesComponent', () => {
  let component: ThemesComponent;
  let service: ThemesSettingsService;
  let electronService: ElectronService;
  let themesLoaderService: ThemesLoaderService;
  let fontsLoadService: FontsLoaderService;
  let settingsTabService: SettingsTabService;
  let gridService: GridService;

  beforeEach(() => {
    clear();
    electronService = getElectronService();
    gridService = getGridService();
    gridService.addNewTab(BinaryTree.ROOT_KEY, TabType.Settings);
    fontsLoadService = getFontsLoaderService();
    themesLoaderService = getThemesLoaderService();
    service = getThemesSettingsService();
    settingsTabService = getSettingsTabService();
    settingsTabService.init(gridService.getActiveTabId());
    component = new ThemesComponent(settingsTabService, service);
  });

  it('should handle selectTheme correct', () => {
    jest.spyOn(themesLoaderService, 'loadThemes').mockReturnValue(of([]));
    jest.spyOn(fontsLoadService, 'loadFonts').mockReturnValue(of([]));
    jest.spyOn(fontsLoadService, 'loadAppFonts').mockReturnValue(of([]));
    component.ngOnInit();
    component.selectTheme(service.getState().installedThemes[0]);
    expect(service.getState().selectedTheme).toEqual(service.getState().installedThemes[0]);
    expect(service.getState().selectedThemeOriginal).toEqual(service.getState().installedThemes[0]);
  });

  it('should handle isLight correct', () => {
    expect(component.isLight('#000000')).toEqual(false);
    expect(component.isLight('#FFFFFF')).toEqual(true);
  });

  it('should handle installTheme correct', () => {
    const themeToInstall = {...TestHelper.createTheme(), name: 'test'};
    const expected = {...themeToInstall,  isInstalled: true, isDefault: true};
    jest.spyOn(themesLoaderService, 'loadThemes').mockReturnValue(of([themeToInstall]));
    jest.spyOn(fontsLoadService, 'loadFonts').mockReturnValue(of([]));
    jest.spyOn(fontsLoadService, 'loadAppFonts').mockReturnValue(of([]));
    jest.spyOn(electronService, 'send').mockReturnValue(void(0));
    component.ngOnInit();
    component.selectTheme(themeToInstall);
    component.installTheme();
    expect(service.getState().selectedTheme).toEqual(expected);
    expect(settingsTabService.getState().isLoading).toBeTruthy();
    expect(electronService.send).toHaveBeenCalledWith('installTheme', expected);
  });
});
