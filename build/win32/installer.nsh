!macro customInstall
  WriteRegStr HKCU "Software\Classes\Directory\Background\shell\Cogno" "" "Open Cogno here"
  WriteRegStr HKCU "Software\Classes\Directory\Background\shell\Cogno" "Icon" "$appExe"
  WriteRegStr HKCU "Software\Classes\Directory\Background\shell\Cogno\command" "" `$appExe "%V"`

  WriteRegStr HKCU "Software\Classes\Directory\shell\Cogno" "" "Open Cogno here"
  WriteRegStr HKCU "Software\Classes\Directory\shell\Cogno" "Icon" "$appExe"
  WriteRegStr HKCU "Software\Classes\Directory\shell\Cogno\command" "" `$appExe "%V"`

  WriteRegStr HKCU "Software\Classes\Drive\shell\Cogno" "" "Open Cogno here"
  WriteRegStr HKCU "Software\Classes\Drive\shell\Cogno" "Icon" "$appExe"
  WriteRegStr HKCU "Software\Classes\Drive\shell\Cogno\command" "" `$appExe "%V"`
!macroend

!macro customUnInstall
  DeleteRegKey HKCU "Software\Classes\Directory\Background\shell\Cogno"
  DeleteRegKey HKCU "Software\Classes\Directory\shell\Cogno"
  DeleteRegKey HKCU "Software\Classes\Drive\shell\Cogno"
!macroend
