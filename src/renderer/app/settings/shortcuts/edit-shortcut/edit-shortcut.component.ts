import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Shortcut} from '../../../../../shared/models/shortcuts';
import {ShortcutsService} from '../+state/shortcuts.service';
import {CommonModule} from '@angular/common';
import {ShortcutKeysPipe} from '../../../+shared/pipes/shortcut-keys/keys.pipe';
import {FormsModule} from '@angular/forms';
import {Key} from '../../../common/key';

@Component({
    selector: 'app-edit-shortcut',
    templateUrl: './edit-shortcut.component.html',
    styleUrls: ['./edit-shortcut.component.scss'],
    imports: [
        CommonModule,
        FormsModule,
        ShortcutKeysPipe
    ]
})
export class EditShortcutComponent implements AfterViewInit {

  @ViewChild('commandInput') commandInput: ElementRef;
  @ViewChild('keyInput') keyInput: ElementRef;
  keys: Observable<string[]>;
  activeShortcut: Observable<Shortcut>;
  duplicatedShortcut: Observable<boolean>;
  hasShortcutInputFocus = new BehaviorSubject<boolean>(false);

  constructor(private shortcutService: ShortcutsService) {
    this.keys = shortcutService.selectActiveShortcutKeys();
    this.activeShortcut = shortcutService.selectActiveShortcut();
    this.duplicatedShortcut = shortcutService.selectSameShortcutExists();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.commandInput) {
        this.commandInputFocus(null);
      } else {
        this.keyInputFocus(null);
      }
    });
  }

  commandChanged(event: KeyboardEvent): void {
    event?.preventDefault();
    event?.stopPropagation();
    if (event.key === Key.Enter) {
      this.save();
    } else if (event.key === Key.Escape) {
      this.close();
    } else {
      this.shortcutService.updateCommand((event.target as HTMLInputElement).value);
    }
  }

  onFocusShortcutInput(event: Event) {
    event?.preventDefault();
    event?.stopPropagation();
    this.hasShortcutInputFocus.next(true);
  }

  shortcutChanged(event: KeyboardEvent): void {
    event?.preventDefault();
    event?.stopPropagation();
    if (event.key === Key.Enter) {
      this.save();
    } else if (event.key === Key.Escape) {
      this.close();
    } else {
      this.shortcutService.updateShortcut(event);
    }
  }

  commandInputFocus(event: Event): void {
    event?.preventDefault();
    event?.stopPropagation();
    this.commandInput?.nativeElement.focus();
    this.hasShortcutInputFocus.next(false);
  }

  keyInputFocus(event: Event): void {
    event?.preventDefault();
    event?.stopPropagation();
    this.keyInput?.nativeElement.focus();
    this.hasShortcutInputFocus.next(true);
  }

  besideClick(): void {
    this.shortcutService.closeEditShortcutDialog();
  }

  close($event: Event = null): void {
    $event?.stopPropagation();
    $event?.preventDefault();
    this.shortcutService.closeEditShortcutDialog();
  }

  save($event: Event = null): void {
    this.close($event);
    this.shortcutService.saveShortcuts();
  }
}
