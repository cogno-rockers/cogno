import {IpcChannel} from '../shared/ipc.chanels';
import {ipcMain} from 'electron';
import * as path from 'path';
import {ShellType} from '../shared/models/models';
import {ShellConfig} from '../shared/models/settings';
import * as fs from 'fs';
import {Logger} from './logger';
import {v4 as uuidv4} from 'uuid';
import {Application} from '../shared/application';

let Registry: any;
if (process.platform === 'win32') {
  Registry = require('windows-native-registry');
}

export namespace ShellHandler {

  export function init() {
    Logger.debug('Init shell handler');
    ipcMain.handle(IpcChannel.GetAvailableShells, async function (evt) {
      return new Promise<ShellConfig[]>((resolve, reject) => {
        try {
          let availableShells = [];
          switch (process.platform) {
            case 'win32':
              availableShells = [getGitBash(), getPowershell(), getBashWsl(), getZSHWsl(), getPowershellCore()].filter(s => s !== null);
              break;
            case 'darwin':
              availableShells = [getZSH(), getBashLinux()];
              break;
            case 'linux':
              availableShells = [getBashLinux(), getZSH()];
              break;
          }
          Logger.debug('Found following shells', availableShells);
          resolve(availableShells);
        } catch (e) {
          Logger.error('Error load fonts', e);
          reject(e);
        }
      });
    });
    ipcMain.handle(IpcChannel.LoadScriptContent, async function (evt, shellType: ShellType) {
      Logger.debug('Request shell script', shellType);
      let p = '';
      switch (shellType) {
        case ShellType.Bash:
          p = path.join(Application.getAppPath(), 'src', 'scripts', 'bash.sh');
          break;
        case ShellType.GitBash:
          p = path.join(Application.getAppPath(), 'src', 'scripts', 'bash.sh');
          break;
        case ShellType.Powershell:
          p = path.join(Application.getAppPath(), 'src', 'scripts', 'powershell.ps1');
          break;
        case ShellType.ZSH:
          p = path.join(Application.getAppPath(), 'src', 'scripts', 'zsh.sh');
          break;
      }
      return new Promise<string>((resolve, reject) => {
        fs.readFile(p, 'utf8', (err, data) => {
          if (err) {
            Logger.error('Error load fonts', err);
            reject(err);
          }
          Logger.debug(`File content:\n${data}`);
          resolve(data);
        });
      });
    });
  }

  function getBashLinux(): ShellConfig {
    const paths = ['/bin/bash'];
    const p = searchPaths(paths);
    return p ? {
      ...getDefaultShellConfigForLinux(),
      name: 'Bash',
      path: p,
      args: ['--login', '-i'],
      type: ShellType.Bash
    } : null;
  }

  function getZSH(): ShellConfig {
    const paths = ['/bin/zsh'];
    const p = searchPaths(paths);
    return p ? {
      ...getDefaultShellConfigForDarwin(),
      name: 'ZSH',
      path: p,
      args: ['--login', '-i'],
      type: ShellType.ZSH
    } : null;
  }

  function getBashWsl(): ShellConfig {
    const paths = [`${process.env.windir}\\system32\\wsl.exe`];
    const p = searchPaths(paths);
    return p ? {
      ...getDefaultShellConfigForWindows(),
      name: 'Bash (WSL)',
      path: p,
      args: [],
      type: ShellType.Bash
    } : null;
  }

  function getZSHWsl(): ShellConfig {
    const paths = [`${process.env.windir}\\system32\\wsl.exe`];
    const p = searchPaths(paths);
    return p ? {
      ...getDefaultShellConfigForWindows(),
      name: 'ZSH (WSL)',
      path: p,
      args: [],
      type: ShellType.ZSH
    } : null;
  }

  function getPowershell(): ShellConfig {
    const paths = [
      `${process.env.windir}\\System32\\WindowsPowerShell\\v1.0\\powershell.exe`,
      `${process.env.windir}\\System32\\powershell.exe`
    ];
    const p = searchPaths(paths);
    return p ? {
      ...getDefaultShellConfigForWindows(),
      name: 'Powershell',
      path: p,
      args: ['-nologo'],
      type: ShellType.Powershell
    } : null;
  }


  function getPowershellCore(): ShellConfig {

    let powershellSevenPath = Registry?.getRegistryValue(Registry.HK.LM, 'SOFTWARE\\Microsoft\\PowerShell\\7', 'ExecutablePath');
    if(!powershellSevenPath) {
      powershellSevenPath = Registry?.getRegistryValue(Registry.HK.LM, 'SOFTWARE\\WOW6432Node\\Microsoft\\PowerShell\\7', 'ExecutablePath');
    }
    if (!powershellSevenPath) {
      powershellSevenPath = Registry?.getRegistryValue(Registry.HK.LM, 'SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\pwsh.exe', '');
    }
    return {
      ...getDefaultShellConfigForWindows(),
      name: 'Powershell 7',
      path: powershellSevenPath,
      args: ['-nologo'],
      type: ShellType.Powershell
    };
  }

  function getGitBash(): ShellConfig {
    let gitBashPath = searchPaths(['C:\\Program Files\\Git']);
    if (!gitBashPath) {
      gitBashPath = Registry?.getRegistryValue(Registry.HK.LM, 'SOFTWARE\\GitForWindows', 'InstallPath');
    }
    if (!gitBashPath) {
      gitBashPath = Registry?.getRegistryValue(Registry.HK.CU, 'SOFTWARE\\GitForWindows', 'InstallPath');
    }

    if (!gitBashPath) {
      Logger.debug('Could not find gitBash');
      return null;
    }
    return {
      ...getDefaultShellConfigForWindows(),
      name: 'Git Bash',
      path: path.join(gitBashPath, 'bin', 'sh.exe'),
      args: ['--login', '-i'],
      type: ShellType.GitBash
    };
  }

  function searchPaths(paths: string[]): string {
    for (const searchPath of paths) {
      try {
        fs.statSync(searchPath);
        return searchPath;
      } catch (ex) {
        Logger.debug(`Could not find ${path}`, ex);
      }
    }
  }

  function getDefaultShellConfigForWindows(): ShellConfig {
    return {
      id: uuidv4(),
      name: '',
      path: '',
      args: [],
      default: false,
      isSelected: true,
      type: ShellType.Powershell,
      workingDir: Application.getUserHomeDirPath(),
      injectionType: 'Auto'
    };
  }

  function getDefaultShellConfigForDarwin(): ShellConfig {
    return {
      id: uuidv4(),
      name: '',
      path: '',
      args: [],
      default: false,
      isSelected: true,
      type: ShellType.ZSH,
      workingDir: Application.getUserHomeDirPath(),
      injectionType: 'Auto'
    };
  }

  function getDefaultShellConfigForLinux(): ShellConfig {
    return {
      id: uuidv4(),
      name: '',
      path: '',
      args: [],
      default: false,
      isSelected: true,
      type: ShellType.Bash,
      workingDir: Application.getUserHomeDirPath(),
      injectionType: 'Auto'
    };
  }
}
