import { ShellType } from '../../../../../shared/models/models';
import { Normalizer } from '../../../../../shared/normalizer';
import { ShellLocation } from '../../../common/shellLocation';
import {Highlighter} from './highlight';
import {SuggestionCalculation} from './history/models';
import { HighlightType } from './suggester';

describe('Highlighter', () => {

  let highlighter: Highlighter;

  beforeEach(() => {
    highlighter = new Highlighter(new Normalizer(new ShellLocation('', ShellType.Bash)));
  });

  it('should work with empty tokens', () => {
    const suggestion: SuggestionCalculation = {label: 'cd test'};
    highlighter.apply([suggestion], []);
    expect(suggestion.labelHighlight).toEqual([{text: 'cd test', type: 0}]);
  });

  it('should work with non empty input', () => {
    const suggestion: SuggestionCalculation = {label: 'cd test'};
    highlighter.apply([suggestion], ['cd', 'test']);
    expect(suggestion.labelHighlight).toEqual([{text: 'cd', type: 1}, {text: ' ', type: 0}, {text: 'test', type: 1}]);
  });

  it('should ignore case in search', () => {
    const suggestion: SuggestionCalculation = {label: 'cd test'};
    highlighter.apply([suggestion], ['cd', 'Test']);
    expect(suggestion.labelHighlight).toEqual([{text: 'cd', type: 1}, {text: ' ', type: 0}, {text: 'test', type: 1}]);
  });

   it('should ignore case in label', () => {
     const suggestion: SuggestionCalculation = {label: 'cd Test'};
     highlighter.apply([suggestion], ['cd', 'test']);
     expect(suggestion.labelHighlight).toEqual([{text: 'cd', type: 1}, {text: ' ', type: 0}, {text: 'Test', type: 1}]);
   });

   it('should work with many input parts', () => {
     const suggestion: SuggestionCalculation = {label: 'cd cd tete st   est'};
     highlighter.apply([suggestion], ['te', 'st']);
     expect(suggestion.labelHighlight).toEqual([{text: 'cd cd ', type: HighlightType.None}, {text: 'tete', type: HighlightType.Search}, {text: ' ', type: HighlightType.None}, {text: 'st', type: HighlightType.Search}, {text: '   e', type: HighlightType.None}, {text: 'st', type: HighlightType.Search}]);
   });

   it('should sort the result', () => {
     const suggestion: SuggestionCalculation = {label: 'coverage/'};
     highlighter.apply([suggestion], ['v', 'c']);
     expect(suggestion.labelHighlight).toEqual([{text: 'c', type: HighlightType.Search}, {text: 'o', type: HighlightType.None}, {text: 'v', type: HighlightType.Search}, {text: 'erage/', type: HighlightType.None}]);
   });

   it('should only highlight the longest', () => {
     const suggestion: SuggestionCalculation = {label: 'coverage/'};
     highlighter.apply([suggestion], ['o', 'co']);
     expect(suggestion.labelHighlight).toEqual([{text: 'co', type: HighlightType.Search}, {text: 'verage/', type: HighlightType.None}]);
   });

   it('should work with argument variable last position', () => {
     const suggestion: SuggestionCalculation = {label: 'git commit -a -m {{?}}'};
     highlighter.apply([suggestion], ['git']);
     expect(suggestion.labelHighlight).toEqual([{text: 'git', type: HighlightType.Search}, {text: ' commit -a -m ', type: HighlightType.None}, {text: '{{?}}', type: HighlightType.Argument}]);
   });

  it('should work with argument variable with name last position', () => {
    const suggestion: SuggestionCalculation = {label: 'git commit -a -m {{comment}}'};
    highlighter.apply([suggestion], ['git']);
    expect(suggestion.labelHighlight).toEqual([{text: 'git', type: HighlightType.Search}, {text: ' commit -a -m ', type: HighlightType.None}, {text: '{{comment}}', type: HighlightType.Argument}]);
  });

  it('should work with argument variable with minus', () => {
    const suggestion: SuggestionCalculation = {label: 'git checkout {{git-branch-name}}'};
    highlighter.apply([suggestion], ['git']);
    expect(suggestion.labelHighlight).toEqual([{text: 'git', type: HighlightType.Search}, {text: ' checkout ', type: HighlightType.None}, {text: '{{git-branch-name}}', type: HighlightType.Argument}]);
  });

  it('should work with argument variable middle position', () => {
    const suggestion: SuggestionCalculation = {label: 'git commit --date={{?}} -a'};
    highlighter.apply([suggestion], ['git']);
    expect(suggestion.labelHighlight).toEqual([{text: 'git', type: HighlightType.Search}, {text: ' commit --date=', type: HighlightType.None}, {text: '{{?}}', type: HighlightType.Argument}, {text: ' -a', type: HighlightType.None}]);
  });

  it('should work with argument variable middle and end position', () => {
    const suggestion: SuggestionCalculation = {label: 'git commit -a --date={{?}} -m {{?}}'};
    highlighter.apply([suggestion], ['git']);
    expect(suggestion.labelHighlight).toEqual([{text: 'git', type: HighlightType.Search}, {text: ' commit -a --date=', type: HighlightType.None}, {text: '{{?}}', type: HighlightType.Argument}, {text: ' -m ', type: HighlightType.None}, {text: '{{?}}', type: HighlightType.Argument}]);
  });

   it('should work with connected commands - linux', () => {
     const suggestion: SuggestionCalculation = {label: 'cd test && git init', shellCommand: 'git init'};
     highlighter.apply([suggestion], ['git']);
     expect(suggestion.labelHighlight).toEqual([{text: '', type: HighlightType.OpenSpecial}, {text: 'cd test &&', type: HighlightType.None}, {text: '', type: HighlightType.CloseSpecial}, {text: ' ', type: HighlightType.None}, {text: 'git', type: HighlightType.Search}, {text: ' init', type: HighlightType.None}]);
   });

  it('should work with connected commands - windows', () => {
    const suggestion: SuggestionCalculation = {label: 'cd test ; git init', shellCommand: 'git init'};
    highlighter = new Highlighter(new Normalizer(new ShellLocation('', ShellType.Powershell)));
    highlighter.apply([suggestion], ['git']);
    expect(suggestion.labelHighlight).toEqual([{text: '', type: HighlightType.OpenSpecial}, {text: 'cd test ;', type: HighlightType.None}, {text: '', type: HighlightType.CloseSpecial}, {text: ' ', type: HighlightType.None}, {text: 'git', type: HighlightType.Search}, {text: ' init', type: HighlightType.None}]);
  });
});
