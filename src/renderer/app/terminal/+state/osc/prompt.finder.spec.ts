import {PromptFinder} from './prompt.finder';
import {Chars} from '../../../../../shared/chars';

describe('PromptFinder.tryToFindPrompt', () => {

  test('should return undefined if lineText is empty', () => {
    expect(new PromptFinder(null, null).tryToFindPrompt('')).toBeUndefined();
  });

  test('should return undefined if lineText is null', () => {
    expect(new PromptFinder(null, null).tryToFindPrompt(null)).toBeUndefined();
  });

  test('should return undefined if lineText is undefined', () => {
    expect(new PromptFinder(null, null).tryToFindPrompt(undefined)).toBeUndefined();
  });

  test('should match custom prompt with promptString provided', () => {
    const lineText = 'custom prompt ❯';
    const promptEnding = '❯';
    expect(new PromptFinder(promptEnding, null).tryToFindPrompt(lineText)).toBe('custom prompt ❯');
  });

  test('should match PowerShell prompt', () => {
    const lineText = 'PS C:\\Users> ';
    expect(new PromptFinder(null, null).tryToFindPrompt(lineText)).toBe('PS C:\\Users> ');
  });

  test('should match ZSH prompt with #', () => {
    const lineText = 'zsh-prompt #';
    expect(new PromptFinder(null, null).tryToFindPrompt(lineText)).toBe('zsh-prompt #');
  });

  test('should match ZSH prompt with %', () => {
    const lineText = 'mac-zsh-prompt %';
    expect(new PromptFinder(null, null).tryToFindPrompt(lineText)).toBe('mac-zsh-prompt %');
  });

  test('should match Bash prompt with $', () => {
    const lineText = 'bash-prompt $';
    expect(new PromptFinder(null, null).tryToFindPrompt(lineText)).toBe('bash-prompt $');
  });

  test('should match custom prompt with ❯', () => {
    const lineText = 'custom-prompt ❯';
    expect(new PromptFinder(null, null).tryToFindPrompt(lineText)).toBe('custom-prompt ❯');
  });

  test('should match Cogno prompt with narrow no-break space', () => {
    const lineText = 'cogno-prompt\u200A';
    expect(new PromptFinder(null, null).tryToFindPrompt(lineText)).toBe('cogno-prompt');
  });

  test('should match Cogno prompt with narrow no-break space as last prompt char', () => {
    const lineText = 'cogno-prompt\u200A';
    const lastPromptChar = '\u200A';
    expect(new PromptFinder(lastPromptChar, null).tryToFindPrompt(lineText)).toBe('cogno-prompt\u200A');
  });


  test('should return undefined if no match is found', () => {
    const lineText = 'no-prompt-here';
    expect(new PromptFinder(null, null).tryToFindPrompt(lineText)).toBeUndefined();
  });

  test('should escape promptString correctly', () => {
    const lineText = 'escaped prompt (test) ❯ ';
    const promptString = '❯';
    expect(new PromptFinder(promptString, null).tryToFindPrompt(lineText)).toBe('escaped prompt (test) ❯');
  });

  test('should escape promptString correctly with whitespace', () => {
    const lineText = 'escaped prompt (test) ❯ ';
    const promptString = '❯';
    expect(new PromptFinder(promptString, true).tryToFindPrompt(lineText)).toBe('escaped prompt (test) ❯ ');
  });

  test('should escape promptString correctly with %', () => {
    const lineText = 'larswolfram@Air-von-Lars ~ % ';
    const promptString = '%';
    expect(new PromptFinder(promptString, false).tryToFindPrompt(lineText)).toBe('larswolfram@Air-von-Lars ~ %');
  });

  it('should find prompt $', () => {
    expect(new PromptFinder(undefined, false).tryToFindPrompt(undefined)).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf ')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf $')).toBe('saf $');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf $ ')).toBe('saf $ ');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf $ ssdf')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt(' $')).toBe(' $');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('$ ')).toBe('$ ');
  });

  it('should find prompt %', () => {
    expect(new PromptFinder(undefined, false).tryToFindPrompt(undefined)).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf ')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf %')).toBe('saf %');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf % ')).toBe('saf % ');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf % ssdf')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt(' %')).toBe(' %');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('%    ')).toBe('% ');
  });

  it('should find prompt ❯', () => {
    expect(new PromptFinder(undefined, false).tryToFindPrompt(undefined)).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf ')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf ❯')).toBe('saf ❯');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf ❯ ')).toBe('saf ❯ ');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf ❯ ssdf')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt(' ❯')).toBe(' ❯');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('❯')).toBe('❯');
  });

  it('should find prompt PS >', () => {
    expect(new PromptFinder(undefined, false).tryToFindPrompt(undefined)).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('saf ')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('PS >')).toBe('PS >');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('(Beispiel) PS1234 >')).toBe('(Beispiel) PS1234 >');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('PS0012 > ')).toBe('PS0012 > ');
    expect(new PromptFinder(undefined, false).tryToFindPrompt('PS')).toBe(undefined);
    expect(new PromptFinder(undefined, false).tryToFindPrompt('PS > Zusatz')).toBe(undefined);
  });
});
