import {Component, Input, OnInit} from '@angular/core';
import {Observable, tap} from 'rxjs';
import {TerminalService} from '../+state/terminal.service';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../../+shared/components/icon/icon.component';

@Component({
    selector: 'app-scroll-to-bottom',
    templateUrl: './scroll-to-bottom.component.html',
    styleUrls: ['./scroll-to-bottom.component.scss'],
    imports: [
        CommonModule,
        IconComponent
    ]
})
export class ScrollToBottomComponent implements OnInit {

  @Input()
  public id: string;
  public showScrollToBottom: Observable<boolean>;
  constructor(private terminalService: TerminalService) { }

  ngOnInit(): void {
    this.showScrollToBottom = this.terminalService.select(s => s?.isScrollButtonVisible);
  }

  scrollToBottom() {
    this.terminalService.scrollToBottom();
    this.terminalService.focus();
  }
}
