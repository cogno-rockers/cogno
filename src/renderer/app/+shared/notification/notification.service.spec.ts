import {NotificationService, NotificationType} from './notification.service';
import {skip, take} from 'rxjs/operators';

describe('NotificationService', () => {
  let service: NotificationService;

  beforeEach(() => {
    service = new NotificationService();
  });

  it('should add notification as error', (done) => {
    service.notifications.pipe(skip(1)).subscribe(n => {
      expect(n[0].type).toEqual(NotificationType.Error);
      done();
    });
    service.create().title('test').showAsError().subscribe();
  });

  it('should add notification as warning', (done) => {
    service.notifications.pipe(skip(1)).subscribe(n => {
      expect(n[0].type).toEqual(NotificationType.Warning);
      done();
    });
    service.create().title('test').showAsWarning().subscribe();
  });

  it('should add notification as success', (done) => {
    service.notifications.pipe(skip(1)).subscribe(n => {
      expect(n[0].type).toEqual(NotificationType.Success);
      done();
    });
    service.create().title('test').showAsSuccess().subscribe();
  });

  it('should not add notification if has same single key', (done) => {
    service.notifications.pipe(skip(2)).subscribe(n => {
      expect(n.length).toEqual(2);
      done();
    });
    service.create().title('test').single('test').showAsSuccess().subscribe();
    service.create().title('test').single('test').showAsSuccess().subscribe();
    service.create().title('test').single('test2').showAsSuccess().subscribe();
  });

  it('should add notification title', (done) => {
    service.notifications.pipe(skip(1)).subscribe(n => {
      expect(n[0].title).toEqual('Test');
      done();
    });
    service.create().title('Test').showAsSuccess().subscribe();
  });

  it('should add notification body', (done) => {
    service.notifications.pipe(skip(1)).subscribe(n => {
      expect(n[0].body).toEqual(['Test1', 'Test2']);
      done();
    });
    service.create().title('test').body('Test1', 'Test2').showAsSuccess().subscribe();
  });

  it('should remove notification after timeout', (done) => {
    service.notifications.pipe(skip(1), take(1)).subscribe(n => {
      expect(n.length).toEqual(1);
    });
    service.notifications.pipe(skip(2), take(1)).subscribe(n => {
      expect(n.length).toEqual(0);
      done();
    });
    service.create().title('test').duration(500).showAsSuccess().subscribe();
  });

  it('should show footer', (done) => {
    service.notifications.pipe(skip(1)).subscribe(n => {
      expect(n[0].footer).toEqual(['test']);
      done();
    });
    service.create().title('test').footer('test').showAsSuccess().subscribe();
  });

  it('should toggle visibility if mouse is moved', (done) => {
    service.isVisible.pipe(skip(1), take(1)).subscribe(isVisible => {
     expect(isVisible).toBeTruthy();
    });
    service.isVisible.pipe(skip(2), take(1)).subscribe(isVisible => {
      expect(isVisible).toBeFalsy();
      done();
    });
    service.create().title('test').footer('test').duration(500).showAsSuccess().subscribe();
  });

  it('should add many notifications', (done) => {
    service.notifications.pipe(skip(1), take(1)).subscribe(n => {
      expect(n.length).toEqual(1);
    });
    service.notifications.pipe(skip(2), take(1)).subscribe(n => {
      expect(n.length).toEqual(2);
      done();
    });
    service.create().title('test').showAsError().subscribe();
    service.create().title('test').showAsError().subscribe();
  });
});
