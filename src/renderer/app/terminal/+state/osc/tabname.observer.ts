import {Terminal} from '@xterm/xterm';
import {IDisposable} from 'node-pty';
import {Observable, Subject} from 'rxjs';

export class TabnameObserver {

  public readonly OSC_CODE = 0;

  private terminal: Terminal;
  private disposables: IDisposable[] = [];
  private _tabname: Subject<string> = new Subject<string>();
  public get tabname$(): Observable<string> {
    return this._tabname.asObservable();
  }

  bind(terminal: Terminal) {
    this.terminal = terminal;
    this.disposables.push(this.terminal.parser.registerOscHandler(this.OSC_CODE, (data) => this.handleOsc(data)));
  }

  destroy(){
    for (const disposable of this.disposables) {
      disposable.dispose();
    }
  }

  private handleOsc(data: string): boolean {
    this._tabname.next(data);
    return true;
  }
}
