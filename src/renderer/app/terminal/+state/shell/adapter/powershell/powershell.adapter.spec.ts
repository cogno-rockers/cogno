import {PowershellAdapter} from './powershell.adapter';
import {ColorName} from '../../../../../../../shared/models/settings';
import {TestHelper} from '../../../../../../../test/helper';
import {ZshAdapter} from '../zsh/zsh.adapter';

describe('Powershell Adapter', () => {

  it('should calc terminal colors - empty', () => {
    expect(PowershellAdapter.toTerminalColor(null)).toBeTruthy();
    expect(PowershellAdapter.toTerminalColor(undefined)).toBeTruthy();
  });

  it('should work calc terminal colors', () => {
    let defaultColorCount = 0;
    for (const [key, value] of Object.entries(ColorName)) {
      const color = PowershellAdapter.toTerminalColor(value);
      expect(color).toBeTruthy();
      if (color === 'Cyan') {
        defaultColorCount++;
      }
    }
    expect(defaultColorCount).toBe(8);
  });
});
