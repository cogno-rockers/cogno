import { Component } from '@angular/core';
import {WindowButtonsService} from '../../../global/window-buttons/+state/window-buttons.service';
import {IconComponent} from '../icon/icon.component';

@Component({
    selector: 'app-close-window-button',
    templateUrl: './close-window-button.component.html',
    styleUrls: ['./close-window-button.component.scss'],
    imports: [
        IconComponent
    ]
})
export class CloseWindowButtonComponent {

  constructor(private windowService: WindowButtonsService) { }

  close() {
    this.windowService.closeWindow();
  }

}
