import {WorkspacesMenuComponent} from './workspaces-menu.component';
import {WorkspacesService} from './+state/workspaces.service';
import {
  clear,
  getColorService,
  getKeyboardService,
  getKeytipService,
  getWorkspaceService
} from '../../../test/factory';
import {TestHelper} from '../../../test/helper';
import {Key} from '../common/key';
import {Workspace, WorkspaceNode} from '../+shared/models/workspace';


describe('WorkspacesMenuComponent', () => {
  let component: WorkspacesMenuComponent;
  let service: WorkspacesService;

  beforeEach(() => {
    clear();
    service = getWorkspaceService();
    service.loadWorkspaces();
    component = new WorkspacesMenuComponent(
      {nativeElement: {blur: () => {}, focus: () => {}}},
      getKeyboardService(),
      getKeytipService(),
      service,
      getColorService()
      );
  });

  it('should add new Workspace', (done) => {
    component.addWorkspace(TestHelper.createEvent());
    component.workspaces$.subscribe(workspaces => {
      expect(workspaces.length).toEqual(1);
      expect(workspaces[0].id).toEqual('');
      done();
    });
  });

  it('should set name of new Workspace', (done) => {
    component.addWorkspace(TestHelper.createEvent());
    component.nameChanged(TestHelper.createKeyboardEvent({target: {value: 'test1'} as any as EventTarget}));
    component.selectedWorkspace$.subscribe(workspace => {
      expect(workspace.name).toEqual('test1');
      expect(workspace.id).toEqual('');
      done();
    });
  });

  it('should set selectedWorkspace$ to undefined on close', (done) => {
    component.addWorkspace(TestHelper.createEvent());
    component.closeInputField(TestHelper.createEvent());
    component.selectedWorkspace$.subscribe(workspace => {
      expect(workspace).toBeFalsy();
      done();
    });
  });

  it('should remove empty workspace from list on close', (done) => {
    component.addWorkspace(TestHelper.createEvent());
    component.closeInputField(TestHelper.createEvent());
    component.workspaces$.subscribe(workspaces => {
      expect(workspaces.length).toBe(0);
      done();
    });
  });

  it('should save new Workspace on enter', (done) => {
    component.addWorkspace(TestHelper.createEvent());
    component.nameChanged(TestHelper.createKeyboardEvent({target: {value: 'test1'} as any as EventTarget}));
    component.nameChanged(TestHelper.createKeyboardEvent({key: Key.Enter}));
    component.workspaces$.subscribe(workspaces => {
      expect(workspaces.length).toEqual(1);
      expect(workspaces[0].id.length).toBeGreaterThan(0);
      done();
    });
  });

  it('should select Workspace on edit', (done) => {
    component.addWorkspace(TestHelper.createEvent());
    component.nameChanged(TestHelper.createKeyboardEvent({target: {value: 'test1'} as any as EventTarget}));
    component.nameChanged(TestHelper.createKeyboardEvent({key: Key.Enter}));
    const workspaces = service.getWorkspaces();
    component.handleEvent(workspaces[0], 'edit');
    component.selectedWorkspace$.subscribe(w => {
      expect(w).toEqual(workspaces[0]);
      expect(w).not.toBe(workspaces[0]);
      done();
    });
  });

  it('should not override panes of selected workspace if it is not active workspace', (done) => {
    const node: WorkspaceNode = {
      _data: {
        isTopRightPane: false,
        isFirstPane: false
      },
      _left: undefined,
      _right: undefined
    };
    const workspace: Workspace = {id: '123', name: 'test', panes: {_root: node}};
    service.getStore().update({workspaces: [workspace], selectedWorkspace: {...workspace}});
    const workspaces = service.getWorkspaces();
    component.handleEvent(workspaces[0], 'edit');
    component.saveWorkspace(TestHelper.createEvent());
    component.selectedWorkspace$.subscribe(w => {
      expect(w).toBeFalsy();
      expect(service.getWorkspaces().length).toBe(1);
      const workspace = service.getWorkspaces()[0];
      expect(workspace.panes._root).toBe(node);
      done();
    });
  });

  it('should override panes of selected workspace if it is active workspace', (done) => {
    const node: WorkspaceNode = {
      _data: {
        isTopRightPane: false,
        isFirstPane: false
      },
      _left: undefined,
      _right: undefined
    };
    const workspace: Workspace = {id: '123', name: 'test', panes: {_root: node}};
    service.getStore().update({workspaces: [workspace], selectedWorkspace: {...workspace}, activeWorkspace: {...workspace}});
    component.saveWorkspace(TestHelper.createEvent());
    component.selectedWorkspace$.subscribe(w => {
      expect(w).toBeFalsy();
      expect(service.getWorkspaces().length).toBe(1);
      const workspace = service.getWorkspaces()[0];
      expect(workspace.panes._root).not.toBe(node);
      done();
    });
  });

  it('should delete Workspace', (done) => {
    const node: WorkspaceNode = {
      _data: {
        isTopRightPane: false,
        isFirstPane: false
      },
      _left: undefined,
      _right: undefined
    };
    const workspace: Workspace = {id: '123', name: 'test', panes: {_root: node}};
    service.getStore().update({workspaces: [workspace], selectedWorkspace: {...workspace}, activeWorkspace: {...workspace}});
    const workspaces = service.getWorkspaces();
    component.handleEvent(workspaces[0], 'delete');
    component.selectedWorkspace$.subscribe(w => {
      expect(w).toBeFalsy();
      expect(service.getWorkspaces().length).toBe(0);
      done();
    });
  });
});
