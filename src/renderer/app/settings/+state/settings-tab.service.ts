import {Injectable, OnDestroy} from '@angular/core';
import {GridService} from '../../global/grid/+state/grid.service';
import {OnReady, TabService} from '../../+shared/abstract-components/tab/+state/tab.service';
import {Nav} from './model';
import {ElectronService} from '../../+shared/services/electron/electron.service';
import {Observable} from 'rxjs';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';
import {TabType} from '../../../../shared/models/models';
import {Shortcut, Shortcuts} from '../../../../shared/models/shortcuts';

export interface SettingsState {
  selectedNav: Nav;
  isClosed: boolean;
  isActive: boolean;
  isLoading: boolean;
}

export const initialState: SettingsState = {
  selectedNav: Nav.General,
  isClosed: false,
  isActive: false,
  isLoading: false,
};

@Injectable()
export class SettingsTabService extends TabService<SettingsState> implements OnDestroy, OnReady {

  constructor(
    protected gridService: GridService,
    private electron: ElectronService,
    private keyboardService: KeyboardService
    ) {
    super(gridService, initialState);
    setTimeout(() => this.keyboardService.activateGlobalShortcuts());
  }

  onReady(): void | Promise<void> {
    const tab = this.gridService.getTab(this.getTabId());
    if(tab && tab.tabType === TabType.Settings && tab.config) {
      this.updateNavigation(tab.config.selectedNav);
    }
  }

  updateNavigation(nav: Nav): void {
    this.store.update({selectedNav: nav});
  }

  openSettingsExternal(): void {
    this.electron.openPath(this.electron.getSettingsPath());
  }

  selectSelectedNav(): Observable<Nav> {
    return this.store.select((s) => s.selectedNav);
  }

  selectIsLoading(): Observable<boolean> {
    return this.store.select((s) => s.isLoading);
  }

  updateIsLoading(isLoading: boolean): void {
    this.store.update({isLoading});
  }

  ngOnDestroy(): void {
    this.unsubscribe();
    this.keyboardService.deactivateGlobalShortcuts();
  }

  selectShortcuts(): Observable<Shortcuts> {
    return this.keyboardService.selectShortcuts()
  }
}
